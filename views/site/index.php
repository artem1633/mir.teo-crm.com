<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Главная';
?>
<div class="site-index index-page">
    <div class="index-page__header">
        <div class="container-fluid">
            <div class="header-blocks">
                <div class="header-blocks__left">
                    <div class="header-blocks__logo">
                        <a href="/" class="header-blocks__link">
                            <img src="/img/index-logo.png" alt="logo">
                        </a>
                    </div>
                    <div class="header-blocks__message">
                        <a href="https://miruwir.com/chat/index" class="header-blocks__link">
                            <span class="header-message__icon"></span>
                        </a>
                    </div>
                    <div class="header-blocks__favorite">
                        <a href="https://miruwir.com/chat/index" class="header-blocks__link">
                            <span class="header-favorite__icon"></span>
                        </a>
                    </div>
                    <div class="header-blocks__notification">
                        <a href="https://miruwir.com/chat/index" class="header-blocks__link">
                            <span class="header-notification__icon"></span>
                        </a>
                    </div>
                    <div class="header-blocks__info">
                        <a href="https://miruwir.com/chat/index" class="header-blocks__link">
                            <span class="header-info__icon"></span>
                        </a>
                    </div>
                </div>
                <div class="header-blocks__right">
                    <div class="header-blocks__plus">
                        <a href="/construction" class="header-blocks__link">
                            <span class="header-plus__icon"></span>
                        </a>
                    </div>
                    <div class="header-blocks__profile">
                        <a href="/construction" class="header-blocks__link">
                            <span class="header-profile__icon">
                                <img src="/img/index-profile.png" alt="" class="profile-avatar">
                            </span>
                        </a>
                    </div>
                    <div class="header-blocks__balance">
                        <a href="<?= Url::to(['profile/balance']) ?>" class="header-blocks__link header-balance__info">
                            <span class="header-balance__icon"></span>
                            <?php if(!Yii::$app->user->isGuest): ?>
                                <span class="header-balance__summa"><?= round(Yii::$app->user->identity->balance_usd) ?></span>
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="header-blocks__exit">
                        <a href="/site/logout" class="header-blocks__link">
                            <span class="header-exit__icon"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="scroll-block mCustomScrollbar" data-mcs-theme="minimal-dark">
        <div class="index-page__body">
            <div class="container-fluid">
                <div class="index-page__blocks">
                    <div class="index-page__block">
                        <a href="/info" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/1.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Инфогид
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/2.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Конкурсы
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/blog/news" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/3.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Блог МИРа
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block news-block__parent">
                        <div class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/4.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Новости
                            </div>

                            <div class="news-block__item">
                                <div class="news-block__title">Новости</div>
                                <div class="news-block__links">
                                    <a href="/news/az"><img src="/img/country/1.png" alt="">Азербайджан</a>
                                    <a href="/news/am"><img src="/img/country/2.png" alt="">Армения</a>
                                    <a href="/news/by"><img src="/img/country/3.png" alt="">Беларусь</a>
                                    <a href="/news/ge"><img src="/img/country/4.png" alt="">Грузия</a>
                                    <a href="/news/kaz"><img src="/img/country/5.png" alt="">Казахстан</a>
                                    <a href="/news/kg"><img src="/img/country/6.png" alt="">Киргизия</a>
                                    <a href="/news/lv"><img src="/img/country/7.png" alt="">Латвия</a>
                                    <a href="/news/lt"><img src="/img/country/8.png" alt="">Литва</a>
                                    <a href="/news/md"><img src="/img/country/9.png" alt="">Молдова</a>
                                    <a href="/news/"><img src="/img/country/10.png" alt="">Россия</a>
                                    <a href="/news/tj"><img src="/img/country/11.png" alt="">Таджикистан</a>
                                    <a href="/news/tm"><img src="/img/country/12.png" alt="">Туркменистан</a>
                                    <a href="/news/uz"><img src="/img/country/13.png" alt="">Узбекистан</a>
                                    <a href="/news/ua"><img src="/img/country/14.png" alt="">Украина</a>
                                    <a href="/news/ee"><img src="/img/country/15.png" alt="">Эстония</a>
                                    <a href="/news/cn"><img src="/img/country/16.png" alt="">China</a>
                                    <a href="/news/fr"><img src="/img/country/17.png" alt="">France</a>
                                    <a href="/news/de"><img src="/img/country/18.png" alt="">Germany</a>
                                    <a href="/news/in"><img src="/img/country/19.png" alt="">India</a>
                                    <a href="/news/it"><img src="/img/country/20.png" alt="">Italy</a>
                                    <a href="/news/es"><img src="/img/country/21.png" alt="">Spain</a>
                                    <a href="/news/usa"><img src="/img/country/22.png" alt="">USA</a>
                                    <a href="/news/uk"><img src="/img/country/23.png" alt="">Inited Kingdom</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="index-page__block">
                        <a href="/weather" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/5.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Погода
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/tv" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/6.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Телевидение
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/radio" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/7.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Радио
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/movies" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/8.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Кино и сериалы
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/games" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/9.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Игры
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/10.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Викторины
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/academy" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/11.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Академия
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/12.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Знакомства
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/13.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Работа
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/14.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Объявления
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/15.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Шопинг
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/16.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Услуги
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/17.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Автомото
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/18.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Детский мир
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/19.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Питание
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/20.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Путешествия
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/21.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Подарки
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/22.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Сувениры
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/23.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Такси
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/24.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Рейтинги
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/25.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Ленты
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/rod" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/26.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Родословная
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/site/profile" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/27.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Профиль
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/28.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Счет
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/wiki" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/29.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Викистраница
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/30.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Фото
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/31.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Видео
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/32.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Блог
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/33.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Связи
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/34.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Клубы
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/35.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Анкета
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/36.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Резюме
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/37.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Магазин
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/38.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Книга отзывов
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/39.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Статистика
                            </div>
                        </a>
                    </div>
                    <div class="index-page__block">
                        <a href="/construction" class="index-page__block--link">
                            <div class="index-page__icon">
                                <img src="/img/index/40.svg" alt="" title="">
                            </div>
                            <div class="index-page__name">
                                Офис
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
         newsShow();
        
        $(window).resize(function () {
            newsShow();
        });
        
        $('.news-block__parent').hover(function() {
            let height = $(this).find('.news-block__item').height();
            
//            console.log(height);
        })
    })
        function newsShow() {
         if ($(window).width() < 1025) {
            $('.news-block__parent').on('click', function() {
                $(this).toggleClass('opened');
            });
         }
     }
</script>