<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\forms\LoginForm */

$this->title = 'Авторизация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];
?>

<style>
    body {
        /*background: url("/img/login.jpg") !important;
        background-size: 100% !important;
        background-repeat-x: no-repeat !important;*/
        background: #5f9abb;
    }
</style>

<div class="login animated fadeInDown">
    <div class="wrapper">
        <!-- begin logo -->
        <img src="/img/reg-logo.png" alt="logo" data-wow-offset="150">
        <!-- end logo -->
        <h1 class="title titlelogin">Вход в МИР</h1>
        <div style="margin-top: -10px;" data-wow-offset="150">
            <?php if(Yii::$app->session->hasFlash('success')): ?>
                <b class="text-success"><?= Yii::$app->session->getFlash('success') ?></b>
            <?php endif; ?>
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
            <p class="email">Логин (email)</p>
            <?= $form
                ->field($model, 'username', $fieldOptions1)
                ->label(false)
                ->textInput(['value'=>'admin','placeholder' => $model->getAttributeLabel(''), 'class' => 'form-control input-lg no-border nameandsurname logpassword']) ?>
            <div class="login-text">
		<p class="passwordtext">Пароль</p>
		<?= Html::a('<i class="yellow yellowlogin">Забыли пароль?</i>', ['site/forget-password']) ?>
            </div>
            <?= $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['value'=>'admin','placeholder' => $model->getAttributeLabel(''), 'class' => 'form-control input-lg no-border nameandsurname logpassword']) ?>
            <div class="login-buttons" style="margin-top: 50px; text-align: center;">
                <?= Html::submitButton('Войти', ['class' => 'btn btnlogin', 'name' => 'login-button', 'style' => 'width: 150px;']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

