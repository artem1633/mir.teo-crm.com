<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\helpers\DataHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Регистрация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

if(isset($_GET['ref_id'])){
    $refId = $_GET['ref_id'];
} else {
    $refId = null;
}



?>

<style>
    body {
            background: #5f9abb;
        /*background: url("/img/login.jpg") !important;
        background-size: 100% !important;
        background-repeat-x: no-repeat !important; */
    }
</style>

<div class="animated fadeInDown">
    <div class="wrapper">
        <!-- begin brand -->
        <img src="/img/reg-logo.png" alt="" class="bg" data-wow-offset="150">
        <h1 class="title">Регистрация</h1>
        <a href="https://miruwir.com/ "><p class="yellow ">Уже есть регистрация? </p></a>
        <div class="text">
            <p>Регистрация в МИРе возможна только по приглашению.</p>
            <p>Ваше приглашение от:</p>
	</div>
        <div class="icon" data-wow-offset="150">
			
        </div>
        <!-- end brand -->
        <div class="login-content registration">
            <?php $form = ActiveForm::begin(['id' => 'register-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
            <p class="">Имя и фамилия</p>
            <?= $form
                ->field($model, 'name', $fieldOptions1)
                ->label(false)
                ->textInput(['class' => 'form-control nameandsurname']) ?>
            <p class="">Страна и город</p>
            <?= $form
                ->field($model, 'living_place', $fieldOptions2)
                ->label(false)
                ->textInput(['class' => 'form-control nameandsurname']) ?>
            <p class="">Email (логин)</p>
            <?= $form
                ->field($model, 'email', $fieldOptions2)
                ->label(false)
                ->textInput(['class' => 'form-control nameandsurname']) ?>
            <p class="">Телефон</p>
            <?= $form
                ->field($model, 'phone', $fieldOptions2)
                ->label(false)
                ->textInput(['class' => 'form-control nameandsurname']) ?>
            <p class="textphone">Месенджеры на телефоне</p>
            <div class="gg">
                <div class="main">
                    <input type="checkbox" id="" required="">
                    <label class="" for="">WhatsApp</label><br>
                    <input type="checkbox" id="" required="">
                    <label class="" for="">Viber</label><br>
                </div>
                <div class="nemain nemain2">
                    <input type="checkbox" id="">
                    <label class="" for="">Telegram</label><br>
                    <input type="checkbox" id="" required="">
                    <label class="" for="">Отсутсвуют</label><br>
                </div>
            </div>
            <div class="me">
                <p class="textupper">Меня заинтересовало</p>
                <div action="" class="bb" data-wow-offset="150">
                    <div class="main">
                        <input type="checkbox" id="">
                        <label class="" for="none">Аристократические титулы</label><br>
                        <input type="checkbox" id="gg">
                        <label class="" for="gg">Поставки товаров и услуг </label><br>
                        <input type="checkbox" id="">
                        <label class="" for="">Членство в профсоюзе</label>
                    </div>
                    <div class="nemain">
                        <input type="checkbox" id="">
                        <label class="" for="">Партийная работа</label><br>
                        <input type="checkbox" id="">
                        <label class="" for="">Помогите определиться </label><br>
                        <input type="checkbox" id="">
                        <label class="" for="">Ничего из перечисленного </label>
                    </div>
                </div>
            </div>
            <p class="">Пароль</p>
            <?= $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg no-border']) ?>
            <p class="">Пароль ещё раз</p>
            <input type="password" class="nameandsurname" required="">
            
            <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn fadeInUpBig', 'name' => 'login-button']) ?>

            <p class="footer-text">Вам на почту будет отправлено письмо с подтверждением регистрации.</p>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

