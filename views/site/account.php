<?php
if($profile){
$this->title = "Профиль «{$profile->name}»";
}else{
   $this->title = "Профиль не найден"; 
}
if($profile){
?>

<div class="account-page" style="height: 100%;">
<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>
    <?= $this->render('/layouts/header-new-menu.php'); ?>

    <?php if ( ($dig != null) && ($profile->code_vip == '33655') ){?>
        <style>
            .account {
                background-image: url("/img/VIP.png");
                background-color: #F9D88F;
            }
            .img_vip
            {
                width: 75px !important;
                height: 50px !important;
                position: absolute;
                top: -10px;
                right: -10px;
                border-radius: 0 !important;
                object-fit: contain !important;
                display: block;
            }
        </style>

    <?php } ?>
<div class="account color_bg">

    <div class="scroll-block mCustomScrollbar" data-mcs-theme="minimal-dark">
        <div class="account__inner">
            <div class="account__body">
                <div class="account__body-top">
                    <div class="container-fluid">
                        <div class="account__user-info">
                            <div class="account__user-photo">                                   <?php if ($profile->avatar != null):?>
                                    <img src="<?=$profile->avatar?>" alt="" class="img_prev1">
                                <?php
                                else:
                                    ?>
                                    <img src="/img/noimg.jpg" alt="" class="img_prev1">
                                <?php
                                endif;
                                ?></div>
                            <div class="account__user-content">
                                <div class="account__user-title">Профиль</div>
                                <div class="account__user-name"><?php if ($profile->play_name != null) echo $profile->play_name; else echo $profile->name;?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="account__total">
                        <div class="account__total-left">
                            <div class="profile__images">
                                <div class="profile__block-photo">
                                    <div class="profile__user-photo">
                                        <?php if ($profile->avatar != null):?>
                                            <img src="<?=$profile->avatar?>" alt="" class="img_prev1">
                                            <img src="/img/vipicon.png" alt="" class="img_vip">
                                        <?php
                                        else:
                                            ?>
                                            <img src="/img/noimg.jpg" alt="" class="img_prev1">

                                        <?php
                                        endif;

                                        ?>

                                    </div>
                                </div>

                                <?php if ( ($dig != null) && ($profile->code_vip == '33655') ){?>
                                <div class="profile__block-photo">
                                    <div class="profile__user-emblem">
                                        <img src="<?=$profile->gerb?>">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <div class="account__total-lines">
                                <?php if ($profile->play_name != '0' && $profile->play_name != '' && $profile->play_name != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Игровой псевдоним</div>
                                    <div class="account__total-desc <?php if ($profile->play_name != null) echo "account__total-desc--full"?>"><?php if ($profile->play_name == null) echo "(не указано)"; else echo $profile->play_name ?></div>
                                </div>
                                <?php } ?>
                                <?php if ($profile->name != '0' && $profile->name != '' && $profile->name != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Имя и фамилия</div>
                                    <div class="account__total-desc <?php if ($profile->name != null) echo "account__total-desc--full"?>"><?php if ($profile->name == null) echo "(не указано)"; else echo $profile->name ?></div>
                                </div>
                                <?php } ?>
                                <?php if ($profile->country != '0' && $profile->country != '' && $profile->country != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Страна и город</div>
                                    <div class="account__total-desc <?php if ($profile->country != null) echo "account__total-desc--full"?>"><?php if ($profile->country == null) echo "(не указано)"; else echo ($profile->country.', '.$profile->living_place) ?></div>
                                </div>
                                <?php } ?>
                                <?php if ($profile->birth_date != '0' && $profile->birth_date != '' && $profile->birth_date != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Дата рождения</div>


                                    <div class="account__total-desc <?php if ($profile->birth_date != null) echo "account__total-desc--full"?>"><?php if ($profile->birth_date == null) echo "(не указано)"; else echo $thisdate ?></div>
                                </div>
                                <?php } ?>
                                <?php if ($profile->education != '0'  && $profile->education != '' && $profile->education != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Образование</div>
                                    <div class="account__total-desc <?php if ($profile->education != null) echo "account__total-desc--full"?>"><?php if ($profile->education == null) echo "(не указано)"; else echo $profile->education ?></div>
                                </div>
                                <?php } ?>
                                <?php if ($profile->profession != '0' && $profile->profession != '' && $profile->profession != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Профессия</div>
                                    <div class="account__total-desc <?php if ($profile->profession != null) echo "account__total-desc--full"?>"><?php if ($profile->profession == null) echo "(не указано)"; else echo $profile->profession ?></div>
                                </div>
                                <?php } ?>
                                <?php if ($profile->marital_status != '0' && $profile->marital_status != '' && $profile->marital_status != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Семейное положение</div>
                                    <div class="account__total-desc <?php if ($profile->marital_status!= null) echo "account__total-desc--full"?>"><?php if ($profile->marital_status == null) echo "(не указано)"; else echo $profile->marital_status ?></div>
                                </div>
                                <?php } ?>
                                <?php if ($profile->interests != '0' && $profile->interests != '' && $profile->interests != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Интересы</div>
                                    <div class="account__total-desc <?php if ($profile->interests!= null) echo "account__total-desc--full"?>"><?php if ($profile->interests == null) echo "(не указано)"; else echo $profile->interests ?></div>
                                </div>
                                <?php } ?>
                                <?php if ($profile->hobby != '0' && $profile->hobby != '' && $profile->hobby != null){ ?>
                                <div class="account__total-line">
                                    <div class="account__total-title">Хобби</div>
                                    <div class="account__total-desc <?php if ($profile->hobby!= null) echo "account__total-desc--full"?>"><?php if ($profile->hobby == null) echo "(не указано)"; else echo $profile->hobby ?></div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="account__total-right">
                            <div class="account__total-line">
                                <div class="account__total-title">Статус в государстве МИР</div>
                                <div class="account__total-desc account__total-desc--full"><?php if ($profile->code_state == null) echo "Гость"; elseif($profile->code_state == '11585') echo "Почетный гражданин"; elseif($profile->code_state == '11615') echo "Со-основатель, Гражданин"; elseif($profile->code_state == '11747') echo "Со-основатель, Почетный гражданин"; elseif($profile->code_state == '11222') echo "Основатель, Почетный гражданин"; elseif($profile->code_state == '11585') echo "Резидент"; else echo "Гражданин";  ?></div>
                            </div>
                            <?php if ($profile->position_state != '0' && $profile->position_state != null && $profile->position_state != '' ){ ?>
                            <?php if ($profile->code_service != '0' && $profile->code_service != null && $profile->code_service != '' ){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Должность (должности) в государстве МИР </div>
                                <div class="account__total-desc <?php if ($profile->position_state!= null && $profile->code_service!= null) echo "account__total-desc--full"?>"><?php if ($profile->position_state == null || $profile->code_service == null) echo "(не указано)"; else echo $profile->position_state ?></div>
                            </div>
                             <?php } ?>
                              <?php } ?>
                             <?php if ($profile->code_vip != '0' && $profile->code_vip != null && $profile->code_vip != ''){ ?>
                                <?php if ($dig != '0' && $dig != null && $dig != ''){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Аристократический титул (титулы) МИРа </div>

                                <div class="account__total-desc <?php if ($dig!= null && $profile->code_vip!= null) echo "account__total-desc--full"?>"><?php if ($dig == null || $profile->code_vip== null) echo "(не указано)"; else echo $dig ?></div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                            <?php if ($profile->totalistic_dignity != '0' && $profile->totalistic_dignity != null && $profile->totalistic_dignity != ''){ ?>
                            <?php if ($profile->code_tpm != '0' && $profile->code_tpm != null && $profile->code_tpm != ''){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Должность (должности) в Фонде МИРа</div>
                                <div class="account__total-desc <?php if ($profile->totalistic_dignity!= null && $profile->code_tpm!= null) echo "account__total-desc--full"?>"><?php if ($profile->totalistic_dignity == null || $profile->code_tpm == null) echo "(не указано)"; else echo $profile->totalistic_dignity ?></div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                            <?php if ($profile->code_dsm != '0' && $profile->code_dsm != null && $profile->code_dsm != ''){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Членство в Деловом Союзе МИРа </div>
                                <div class="account__total-desc <?php if ($profile->code_dsm!= null) echo "account__total-desc--full"?>"><?php if ($profile->code_dsm == '55200') echo "Член Делового союза, Арендатор магазина"; if ($profile->code_dsm == '55321') echo "Член Делового союза, Владелец магазина"; if ($profile->code_dsm == '55890') echo "Член Делового союза"; ?></div>
                            </div>
                             <?php } ?>
                             <?php if ($profile->code_epm != '0' && $profile->code_epm != null && $profile->code_epm != ''){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Членство в Едином Профсоюзе МИРа   </div>
                                <div class="account__total-desc <?php if ($profile->code_epm!= null) echo "account__total-desc--full"?>"><?php if ($profile->code_epm == '66100') echo "Соучредитель Профсоюза"; if ($profile->code_epm == '66909') echo "Член Профсоюза"; ?></div>
                            </div>
                            <?php } ?>
                             <?php if ($profile->code_ppm != '0' && $profile->code_ppm != null && $profile->code_ppm != ''){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Участие в Партнерской Программе МИРа </div>
                                <div class="account__total-desc <?php if ($profile->code_ppm!= null) echo "account__total-desc--full"?>"><?php if ($profile->code_ppm == null) echo "(не указано)"; else echo "Участник программы" ?></div>
                            </div>
                             <?php } ?>
                             <?php if ($profile->is_simple != '0' && $profile->is_simple != null && $profile->is_simple != ''){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Членство в клубе Simple </div>
                                <div class="account__total-desc <?php if ($profile->is_simple!= null) echo "account__total-desc--full"?>"><?php if ($profile->is_simple == '88120') echo "ВИП-член клуба"; else echo "Член клуба Simple" ?></div>
                            </div>
                            <?php } ?>
                            <?php if ($profile->franchise_city != '0' && $profile->franchise_city != null && $profile->franchise_city != ''){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Город франшизы MIR-City </div>
                                <div class="account__total-desc <?php if ($profile->franchise_city!= null) echo "account__total-desc--full"?>">
                                    <?= $profile->franchise_city?>
                                    <?php if ($profile->code_sity != '0' && $profile->code_sity != null && $profile->code_sity != '') echo ",Франчайзи" ?>
                                        
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($profile->code_matrix != '0' && $profile->code_matrix != null && $profile->code_matrix != ''){ ?>
                            <div class="account__total-line">
                                <div class="account__total-title">Владение лицензией MIR-Matrix </div>
                                <div class="account__total-desc <?php if ($profile->code_matrix!= null) echo "account__total-desc--full"?>"><?php if ($profile->code_matrix == '10353') echo "Лицензиат, Пайщик"; else echo "Лицензиат"; if ($profile->totem != null && $profile->code_matrix != null ) echo ", ".$profile->totem; ?> </div>
                            </div>
                            <?php } ?>
                            <div class="account__total-line">
                                <div class="user-social__links">


                                    <?php if ($profile->link_vk!= null) echo '<a target="_blank" href="'.$profile->link_vk.'" class="user-social__link user-social__link--vk"></a>';?>

                                    <?php if ($profile->link_odnok!= null) echo '<a target="_blank" href="'.$profile->link_odnok.'" class="user-social__link user-social__link--ok"></a>';?>
                                    <?php if ($profile->link_facebook!= null) echo '<a target="_blank" href="'.$profile->link_facebook.'" class="user-social__link user-social__link--fb"></a>';?>
                                    <?php if ($profile->link_instagram!= null) echo '<a target="_blank" href="'.$profile->link_instagram.'" class="user-social__link user-social__link--insta"></a>';?>
                                    <?php if ($profile->link_twitter!= null) echo '<a target="_blank" href="'.$profile->link_twitter.'" class="user-social__link user-social__link--tw"></a>';?>
                                    <?php if ($profile->link_youtube!= null) echo '<a target="_blank" href="'.$profile->link_youtube.'" class="user-social__link user-social__link--yb"></a>';?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="account__body-bottom">
                    <div class="profile__bottom">
                        <span class="blog-last__see"><?php if($visi == null){ echo "1";}else{ echo $visi->counter;}?></span>

                        <span class="blog-last__like <?php if ($islike1 ==null) echo 'linklike active'; ?>"><?=$countlike?></span>
                        <span class="profile__balance active"><?=$profile->balance_usd ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php }else{

 ?>Профиль не найден<?php
}?>


<script>
    $.ajaxSetup({
        data: <?= \yii\helpers\Json::encode([
            \yii::$app->request->csrfParam => \yii::$app->request->csrfToken,
        ]) ?>
    });

    $('.linklike').click(function(){
        var iscls = $('.linklike').hasClass('linklike');

        if (iscls==false)
            return;

        var param = $('meta[name="csrf-param"]').attr("content");
        var token = $('meta[name="csrf-token"]').attr("content");

        var user_to = "<?php echo $id?>";

        var postData = {user_to: user_to};
        postData[param] = token ;

        $.ajax({
            url: "/site/userlike",
            type: 'POST',
            data: postData,
            success: function(res){

             var countprev = $('.linklike').html();
                countprev++;
                $('.linklike').html(countprev);
                $('.linklike').removeClass('active');
                $('.linklike').removeClass('linklike');

            },
        });

    })



</script>


<style>

    .active{

        cursor: pointer;
    }
</style>