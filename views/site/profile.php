<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\helpers\DataHelper;
use kartik\file\FileInput;

$this->title = "Профиль"; 
//var_dump($user);
?>
<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>

<?php

if( Yii::$app->session->hasFlash('success') ):

   // echo Yii::$app->session->getFlash('success');

        echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-success',
            ],
            'body' => "Данные сохранены",
        ]);


endif;

if( Yii::$app->session->hasFlash('errorval') ):

    // echo Yii::$app->session->getFlash('success');

    echo \yii\bootstrap\Alert::widget([
        'options' => [
            'class' => 'alert-danger',
        ],
        'body' => "Данные введены неверно",
    ]);


endif;

?>

<?php if ( ($user->aristocrat_dignity1 != null) && ($user->code_vip == '33655') ){?>
<style>
    .main_main {
        background-image: url("/img/VIP.png");
        background-color: #F9D88F;
    }
    .img_vip
    {
        width: 75px !important;
        height: 50px !important;
        position: absolute;
        top: -10px;
        right: -10px;
        border-radius: 0 !important;
        object-fit: contain !important;
        display: block;
    }
</style>

<?php } ?>

<div class="profile main color_bg">
    <div class="main_main mCustomScrollbar" data-mcs-theme="minimal-dark">
        <div class="main__inner">


            <?php $form = ActiveForm::begin(['id' => 'editmain-form','fieldConfig' =>['options' => ['tag' => false,],] , 'action' =>'profile', 'enableClientValidation' => true], $options = ['class' => 'margin-bottom-0']); ?>

            <div class="profile__header">
                <div class="page-name">Мой профиль и настройки</div>

                <div class="profile__header-right">
                    <div class="profile__header-inputs">
                        <div class="profile__input-block">
                            <label class="profile__input-label">Логин (e-mail)</label>


                            <?= $form
                                ->field($model, 'email')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value' => $user->login]) ?>

                        </div>
                        <div class="profile__input-block">
                            <label class="profile__input-label">Пароль</label>

                            <?= $form
                                ->field($model, 'password')
                                ->label(false)
                                ->passwordInput(['class' => 'profile__input']) ?>
                        </div>
                        <div class="profile__input-block">
                            <label class="profile__input-label">Телефон</label>

                            <?php if ($user->phone==false) $place = ''; else $place='';
                            ?>
                            <?=
                            $form
                                ->field($model, 'phone')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$user->phone,'placeholder' => $place]) ?>
                        </div>
                    </div>
                    <div class="profile__messengers">
                        <div class="profile__input-label">Мессенджеры</div>
                        <div class="profile__messengers-blocks">
                            <div class="profile__messengers-block">
                                <label class="profile__checkbox-block">
                                    <input type="checkbox" class="profile__messengers-input" name="EditMainSetting[messager_whats_app]" <?php if ($user->messager_whats_app == 1) echo "checked"?>>


                                    <span class="profile__messengers-name">WhatsApp</span>
                                </label>
                            </div>
                            <div class="profile__messengers-block">
                                <label class="profile__checkbox-block">
                                    <input type="checkbox" class="profile__messengers-input" name="EditMainSetting[messager_telegram]" <?php if ($user->messager_telegram == 1) echo "checked"?>>
                                    <span class="profile__messengers-name">Telegram</span>
                                </label>
                            </div>
                            <div class="profile__messengers-block">
                                <label class="profile__checkbox-block">
                                    <input type="checkbox" class="profile__messengers-input" name="EditMainSetting[messager_viber]" <?php if ($user->messager_viber == 1) echo "checked"?>>
                                    <span class="profile__messengers-name">Viber</span>
                                </label>
                            </div>
                            <div class="profile__messengers-block">
                                <label class="profile__checkbox-block">
                                    <input type="checkbox" class="profile__messengers-input" name="EditMainSetting[messager_none]" <?php if ($user->messager_none == 1) echo "checked"?>>
                                    <span class="profile__messengers-name">Отсутствуют</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn--green profile__save-setting" value="Сохранить настройки">
<!--                    <button type="button" class="btn btn--green profile__save-setting">Сохранить настройки</button>-->
                </div>

            </div>

            <?php ActiveForm::end(); ?>

            <?php $form = ActiveForm::begin(['id' => 'editdop-form','fieldConfig' =>['options' => ['tag' => false, 'enctype' => 'multipart/form-data'],] , 'action' =>'profile', 'enableClientValidation' => true], $options = ['class' => 'margin-bottom-0']); ?>

            <div class="profile__body">
                <div class="profile__body-left">
                    <div class="profile__images">
                        <div class="profile__block-photo">
                            <div class="profile__user-photo">
                                <img src="<?=$user->avatar?>" alt="" class="img_prev">
                                <img src="/img/vipicon.png" alt="" class="img_vip">
                            </div>

                            <?= $form->field($model2, 'avatar')->widget(FileInput::class,['options' => ['accept' => 'image/*','class' => 'file-upload','id' =>'file-upload' ],])->label(false)
                            //->fileInput(['class' => 'file-upload', 'id' => 'file-upload',$options])->label(false) ?>
                            <button type="button" class="btn btn--green profile__emblem-download">Загрузить фото</button>


                        </div>
                        <div class="profile__block-photo">
                            <div class="profile__user-emblem">
                                <img src="<?=$user->gerb?>" alt="" class="img_prev1">
                            </div>

                            <?= $form->field($model2, 'gerb')->widget(FileInput::class,['options' => ['accept' => 'image/*','class' => 'file-upload1','id' =>'file-upload1' ],])->label(false) ?>
                            <button type="button" class="btn btn--green profile__emblem-download">Загрузить герб</button>
                            <div class="profile__user-text">Показ гербов работает только для аристократов МИРа.  
                            Если Вы являетесь аристократом, то укажите свой титул (титулы) и введите код ВИП-клуба 
                            в соответствующих полях на этой странице. </div>
                        </div>
                    </div>
                    <div class="profile__user-blocks">
                        <div class="profile__user-block">
                            <label class="profile__input-label">Игровой псевдоним</label>

                            <?php if ($user->play_name==false) $place = ''; else $place='';
                            ?>
                            <?=
                            $form
                                ->field($model2, 'play_name')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$user->play_name,'placeholder' => $place])
                                ->error(['class' => 'form-error-message text-danger']);
                            ?>

                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Имя и фамилия</label>
                            <?php if ($user->name==false) $place = ''; else $place='';
                            ?>
                            <?=
                            $form
                                ->field($model2, 'name')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$user->name,'placeholder' => $place]) ?>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Страна и город</label>

                            <?php if ($user->country==false && $user->living_place==false) $place = ''; else $place='';
                            ?>
                            <?=
                            $form
                                ->field($model2, 'country')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$user->country.', '.$user->living_place,'placeholder' => $place]) ?>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Дата рождения</label>
                            <?php if ($user->birth_date==false) $place = ''; else $place='';?>
                            <input type="date" class="profile__input" name="EditAdditionalSetting[birth_date]" placeholder="<?= $place?>" value="<?=$user->birth_date?>">
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Образование</label>
                            <?php if ($user->education==false) $place = ''; else $place='';
                            ?>
                            <?=
                            $form
                                ->field($model2, 'education')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$user->education,'placeholder' => $place]) ?>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Профессия</label>
                            <?php if ($user->profession==false) $place = ''; else $place='';
                            ?>
                            <?=
                            $form
                                ->field($model2, 'profession')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$user->profession,'placeholder' => $place]) ?>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Семейное положение</label>
                            <?php if ($user->marital_status==false) $place = ''; else $place='';
                            $value=$user->marital_status;
                                        if ($value == '0'){$value = '';}
                            ?>
                            <?=
                            $form
                                ->field($model2, 'marital_status')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Интересы</label>
                            <?php if ($user->interests==false) $place = ''; else $place='';
                            ?>
                            <?=
                            $form
                                ->field($model2, 'interests')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$user->interests,'placeholder' => $place]) ?>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Хобби</label>
                            <?php if ($user->hobby==false) $place = ''; else $place='';
                            ?>
                            <?=
                            $form
                                ->field($model2, 'hobby')
                                ->label(false)
                                ->textInput(['class' => 'profile__input','value'=>$user->hobby,'placeholder' => $place]) ?>
                        </div>
                    </div>
                </div>
                <div class="profile__body-right">
                    <div class="profile__user-blocks">
                        <div class="profile__user-block">
                            <label class="profile__input-label">Статус в государстве МИР</label>
                            <div class="profile__user-block--small">

                                <?php if ($user->code_state == 11585) $value = '11585'; elseif ($user->code_state == 11222) $value = '11222'; elseif ($user->code_state == 11747) $value = '11747'; elseif ($user->code_state == 11615) $value = '11615'; else $value = '';

                                ?>
                                <?=
                                $form
                                    ->field($model2, 'code_state')
                                    ->label(false)
                                    ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код статуса']) ?>
                            </div>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Должность (должности) в государстве МИР   </label>
                            <div class="profile__user-block--inputs">
                                <div class="profile__user-block--meddium">
                                    <?php if ($user->position_state == false) $place = ''; else $place='';
                                        $value=$user->position_state;
                                        if ($value == '0'){$value = '';}
                                    ?>
                                    <?=
                                    $form
                                        ->field($model2, 'position_state')
                                        ->label(false)
                                        ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                </div>
                                <div class="profile__user-block--small">
                                    <?php if ($user->code_service == 22847 ) $value = '22847';  else $value = '';

                                    ?>
                                    <?=
                                    $form
                                        ->field($model2, 'code_service')
                                        ->label(false)
                                        ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код госслужбы']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Аристократический титул (титулы) МИРа</label>
                            <div class="profile__user-block--inputs">
                                <div class="profile__user-block--meddium">
                                    <?php if ($user->aristocrat_dignity1 == false) $place = ''; else $place='';
                                        $value=$user->aristocrat_dignity1;
                                        if ($value == '0'){$value = '';}
                                    ?>
                                    <?=
                                    $form
                                        ->field($model2, 'aristocrat_dignity1')
                                        ->label(false)
                                        ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                </div>
                                <div class="profile__user-block--small">
                                    <?php if ($user->code_vip != null ) $value = $user->code_vip;  else $value = '';
                                        if ($user->code_vip != 0 ) $value = $user->code_vip;  else $value = '';
                                    ?>
                                    <?=
                                    $form
                                        ->field($model2, 'code_vip')
                                        ->label(false)
                                        ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код ВИП-клуба']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label"> Должность (должности) в Фонде МИРа</label>
                            <div class="profile__user-block--inputs">
                                <div class="profile__user-block--meddium">
                                    <?php if ($user->totalistic_dignity == false) $place = ''; else $place='';
                                        $value=$user->totalistic_dignity;
                                        if ($value == '0'){$value = '';}
                                        
                                    ?>
                                    <?=
                                    $form
                                        ->field($model2, 'totalistic_dignity')
                                        ->label(false)
                                        ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                </div>
                                <div class="profile__user-block--small">
                                    <?php if ($user->code_tpm != null ) $value = $user->code_tpm;  else $value = '';
                                        if ($user->code_tpm != 0 ) $value = $user->code_tpm;  else $value = '';
                                    ?>
                                    <?=
                                    $form
                                        ->field($model2, 'code_tpm')
                                        ->label(false)
                                        ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код ФМ']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Членство в Деловом Союзе МИРа</label>
                            <div class="profile__user-block--small">
                                <?php if ($user->code_dsm != null ) $value = $user->code_dsm;  else $value = '';
                                    if ($user->code_dsm != 0 ) $value = $user->code_dsm;  else $value = '';
                                ?>
                                <?=
                                $form
                                    ->field($model2, 'code_dsm')
                                    ->label(false)
                                    ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код ДСМ']) ?>
                            </div>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Членство в Едином Профсоюзе МИРа</label>
                            <div class="profile__user-block--small">
                                <?php if ($user->code_epm != null ) $value = $user->code_epm;  else $value = '';
                                    if ($user->code_epm != 0 ) $value = $user->code_epm;  else $value = '';
                                ?>
                                <?=
                                $form
                                    ->field($model2, 'code_epm')
                                    ->label(false)
                                    ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код ЕПМ']) ?>
                            </div>
                        </div>
                        <div class="profile__user-block">
                            <label class="profile__input-label">Участие в Партнерской Программе МИРа </label>
                            <div class="profile__user-block--small">
                                <?php if ($user->code_ppm != null ) $value = $user->code_ppm;  else $value = '';
                                    if ($user->code_ppm != 0 ) $value = $user->code_ppm;  else $value = '';
                                ?>
                                <?=
                                $form
                                    ->field($model2, 'code_ppm')
                                    ->label(false)
                                    ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код ППМ']) ?>
                            </div>
                        </div>

                        <div class="profile__user-block">
                            <label class="profile__input-label">Членство в клубе Simple </label>
                            <div class="profile__user-block--small">
                                <?php if ($user->is_simple != null ) $value = $user->is_simple;  else $value = '';
                                    if ($user->is_simple != 0 ) $value = $user->is_simple;  else $value = '';
                                ?>  
                                <?=
                                $form
                                    ->field($model2, 'is_simple')
                                    ->label(false)
                                    ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код Simple']) ?>
                            </div>
                        </div>

                        <div class="profile__user-block">
                            <label class="profile__input-label">Город франшизы MIR-City </label>
                            <div class="profile__user-block--inputs">
                            <div class="profile__user-block--meddium">
                                    <?php if ($user->franchise_city == false) $place = ''; else $place='';
                                         $value=$user->franchise_city;
                                        if ($value == '0'){$value = '';}
                                    ?>
                                    <?=
                                    $form
                                        ->field($model2, 'franchise_city')
                                        ->label(false)
                                        ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                </div>                                
                            <div class="profile__user-block--small">
                                <?php if ($user->code_sity != null ) $value = $user->code_sity;  else $value = '';
                                        if ($user->code_sity != 0 ) $value = $user->code_sity;  else $value = '';
                                ?>
                                <?=
                                $form
                                    ->field($model2, 'code_sity')
                                    ->label(false)
                                    ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код City','autocomplete' => 'nope']) ?>
                            </div>
                        </div>
                        </div>

                        <div class="profile__user-block">
                            <label class="profile__input-label">Владение лицензией MIR-Matrix </label>
                            <div class="profile__user-block--small">
                                <?php if ($user->code_matrix != null ) $value = $user->code_matrix;  else $value = '';
                                if ($user->code_matrix != 0 ) $value = $user->code_matrix;  else $value = '';
                                ?>
                                <?=
                                $form
                                    ->field($model2, 'code_matrix')
                                    ->label(false)
                                    ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код Matrix']) ?>
                            </div>
                        </div>


                        <?php

                        if ($user->code_matrix != null && $user->code_matrix != 0) {

                            echo $form->field($model2, 'totem')->label(false)->dropDownList([
                                'Маршал' => 'Маршал',
                                'Генерал' => 'Генерал',
                                'Подполковник' => 'Подполковник',
                                'Полковник' => 'Полковник',
                                'Майор' => 'Майор',
                                'Капитан' => 'Капитан',
                                'Лейтенант' => 'Лейтенант',
                                'Прапорщик' => 'Прапорщик',
                                'Сержант' => 'Сержант',
                                'Рядовой' => 'Рядовой',                                
                            ]);

                        ?>
                    <div style="margin-top: 10px;" class="profile__user-block">
                        <label  class="profile__input-label"> </label>
                            <div class="profile__user-block--small">
                                <?php if ($user->totem_code == 10110) $value = '10110'; elseif ($user->totem_code == 10122) $value = '10122'; elseif ($user->totem_code == 10965) $value = '10965'; elseif ($user->totem_code == 10827) $value = '10827'; elseif ($user->totem_code == 10755) $value = '10755'; elseif ($user->totem_code == 10687) $value = '10687'; elseif ($user->totem_code == 10578) $value = '10578'; elseif ($user->totem_code == 10228) $value = '10228'; elseif ($user->totem_code == 10337) $value = '10337'; elseif ($user->totem_code == 10424) $value = '10424'; else $value = '';

                                ?>
                                <?= 
                                $form
                                    ->field($model2, 'totem_code')
                                    ->label(false)
                                    ->textInput(['aria-invalid' => true,'class' => 'profile__input','value'=>$value,'placeholder' => 'Код звания']) ?>
                            </div>
                        </div>

                <?php

                        }                       


                        ?>
                    
                    </div>

                    <div class="profile__social">
                        <div class="profile__social-blocks">
                            <div class="profile__social-block">
                                <div class="profile__social-line">
                                    <label class="profile__input-label">Ссылка на мою страницу в ВКонтакте    </label>
                                    <div class="profile__social-input profile__social--vk profile__social-icon">
                                        <?php if ($user->link_vk==false) $place = ''; else $place='';                                    
                                         $value=$user->link_vk;
                                        if ($value == '0'){$value = '';}
                                        ?>
                                        <?=
                                        $form
                                            ->field($model2, 'link_vk')
                                            ->label(false)
                                            ->textInput(['class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                    </div>
                                </div>
                                <div class="profile__social-line">
                                    <label class="profile__input-label">Ссылка на мою страницу в Facebook</label>
                                    <div class="profile__social-input profile__social--fb profile__social-icon">
                                        <?php if ($user->link_facebook==false) $place = ''; else $place='';
                                        $value=$user->link_facebook;
                                        if ($value == '0'){$value = '';}
                                        ?>
                                        <?=
                                        $form
                                            ->field($model2, 'link_facebook')
                                            ->label(false)
                                            ->textInput(['class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                    </div>
                                </div>
                                <div class="profile__social-line">
                                    <label class="profile__input-label">Ссылка на мою страницу в Twitter</label>
                                    <div class="profile__social-input profile__social--tw profile__social-icon">
                                        <?php if ($user->link_twitter	==false) $place = ''; else $place='';
                                        $value=$user->link_twitter ;
                                        if ($value == '0'){$value = '';}

                                        ?>
                                        <?=
                                        $form
                                            ->field($model2, 'link_twitter')
                                            ->label(false)
                                            ->textInput(['class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="profile__social-block">
                                <div class="profile__social-line">
                                    <label class="profile__input-label">Ссылка на мою страницу в Одноклассниках </label>
                                    <div class="profile__social-input profile__social--ok profile__social-icon">
                                        <?php if ($user->link_odnok	==false) $place = ''; else $place='';
                                        $value=$user->link_odnok ;
                                        if ($value == '0'){$value = '';}
                                        ?>
                                        <?=
                                        $form
                                            ->field($model2, 'link_odnok')
                                            ->label(false)
                                            ->textInput(['class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                    </div>
                                </div>
                                <div class="profile__social-line">
                                    <label class="profile__input-label">Ссылка на мою страницу в Instagram</label>
                                    <div class="profile__social-input profile__social--insta profile__social-icon">
                                        <?php if ($user->link_instagram	==false) $place = ''; else $place='';
                                        $value=$user->link_instagram;
                                        if ($value == '0'){$value = '';}
                                        ?>
                                        <?=
                                        $form
                                            ->field($model2, 'link_instagram')
                                            ->label(false)
                                            ->textInput(['class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                    </div>
                                </div>
                                <div class="profile__social-line">
                                    <label class="profile__input-label">Ссылка на мою страницу на YouTube </label>
                                    <div class="profile__social-input profile__social--yt profile__social-icon">
                                        <?php if ($user->link_youtube==false) $place = ''; else $place='';
                                         $value=$user->link_youtube;
                                        if ($value == '0'){$value = '';}
                                        ?>
                                        <?=
                                        $form
                                            ->field($model2, 'link_youtube')
                                            ->label(false)
                                            ->textInput(['class' => 'profile__input','value'=>$value,'placeholder' => $place]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="profile__buttons">
                        <input type="submit" class="btn btn--green profile__save-setting" value="Сохранить профиль">
                        <a target="_blank" href="<?=\yii\helpers\Url::to(['site/account','id'=>$user->id])?>"><button type="button" class="btn btn--blue profile__preview-btn" >Смотреть превью</button></a>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

            <div class="profile__bottom">
                <span class="blog-last__see"><?=$view1?></span>
                <span class="blog-last__like"><?=$countlike?></span>
                <span class="profile__balance"><?=$user->balance_usd ?></span>
            </div>
        </div>
    </div>
</div>

<script>

    document.getElementById("file-upload").addEventListener("change", function() {
        var fullPath = document.getElementById("file-upload").value
        var filename = fullPath.replace(/^.*[\\\/]/, '')
        document.getElementById("status").innerHTML = filename;
    });


    function readURL(input,class_img) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(class_img).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('.close').click(function () {

        $('.alert-success').hide(400);
        $('.alert-danger').hide(400);
    })
    $("#file-upload").change(function() {
        readURL(this,'.img_prev');
    });
    $("#file-upload1").change(function() {
        readURL(this,'.img_prev1');
    });

    var arr =$('.invalid-feedback');
    $.each(arr,function () {
        if ($(this).css('display') != 'none') {
            $(this).siblings('input').css('border', '1px solid red');
        }
    })


    $('#editadditionalsetting-code_matrix').keyup(function () {
        var matrixval = $(this).val();
        if (matrixval == '10223')
        $('.dropsc').css('display','block');
    })

    var activef = "<?php echo $user->totem ?>";
    if (activef != null) {
        var arre = $('#editadditionalsetting-totem option');

        $.each(arre,function () {
            if ($(this).val() == activef)
                $(this).css('display','none');
        })
        $('#editadditionalsetting-totem').prepend('<option value=' + activef + '>' + activef + '</option>');
        $('#editadditionalsetting-totem option[value='+activef+']').attr('selected','selected');
    }

</script>