<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\forms\ForgetPassword */

$this->title = 'Востановить пароль';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];
?>
<style>
    body {
        /*background: url("/img/login.jpg") !important;
        background-size: 100% !important;
        background-repeat-x: no-repeat !important;*/
        background: #5f9abb;
    }
</style>
<div class="password-sct animated fadeInDown">
    <div class="wrapper">
        <!-- begin logo -->
        <img src="/img/reg-logo.png" alt="logo" data-wow-offset="150">
        <!-- end logo -->
        <h1 class="title titlelogin passwordtitle">Восстановление пароля</h1>
        <div class="login-content registration" style="margin-top: -10px;">
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
            <p class="email">Логин (email)</p>
            <?= $form
                ->field($model, 'email', $fieldOptions1)
                ->label(false)
                ->textInput(['class' => 'nameandsurname logpassword']) ?>
            <p class="lastinputtext">Вам на почту будет отправлено письмо  для восстановления пароля </p>
            <div class="login-buttons">
                <?= Html::submitButton('Войти', ['class' => 'btn btnlogin btnpassword', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

