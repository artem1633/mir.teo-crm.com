<?php

use app\helpers\AvatarResolver;
use app\models\User;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**@var $chat_partners User[] */
/** @var $search_text string */

if (!isset($chat_partners)) {
    $chat_partners = (new User)->getChatPartners();
}

if (!isset($search_text)) {
    $search_text = null;
}

usort($chat_partners, function($a, $b){
    // var_dump($a->id);
    // var_dump($a->getLastMessageChat($a->id));
    // exit;
    $aDate = strtotime($a->getLastMessageChat($a->id)['date']);
    $bDate = strtotime($b->getLastMessageChat($b->id)['date']);

    // $aUnread = $a->getUnreadMessagesCount();
    // $bUnread = $b->getUnreadMessagesCount();

    return $aDate < $bDate;
});

?>
<!--<div class="chat-left">-->
<div class="chat-search form-group">
    <?= Html::input('text', 'search_recipient', $search_text, [
        'class' => 'search-recipient',
        'placeholder' => 'Поиск...'
    ]) ?>
</div>
<?php Pjax::begin(['id' => 'pjax-chat-list-container']) ?>
<ul class="chat-list">
    <?php /** @var User $partner */
    foreach ($chat_partners as $partner): ?>
        <li class="list-item" data-chat-id="<?= $partner->id ?>">
            <a href="#">
                <div class="list-item-first">
                    <div class="<?= $partner->getStatus(); ?>"></div>
                    <img src="/<?= AvatarResolver::getRealAvatarPath($partner->avatar, true) ?>"
                         alt="<?= $partner->name ?>">
                    <div class="list-item-first-info">
                        <h4 title=""><?= $partner->name ?></h4>
                        <p data-role="chat-last-message-text"><?= $partner->getLastMessage($partner->id)['message']; ?></p>
                    </div>
                </div>
                <div class="list-item-second">
                    <div class="time-last-msg"><?php
                        echo Yii::$app->formatter->asTime($partner->last_active_datetime);
                        // echo $partner->getLastMessageChat($partner->id)['date'];
                    ?></div>
                    <?php
                    $unread_count = $partner->getUnreadMessagesCount();
                    Yii::info('Собеседник: ' . $partner->name . '. Непрочитано: ' . $unread_count, 'test');
                    if ($unread_count): ?>
                        <div class="unread-msg"><?= $unread_count ?></div>
                    <?php endif; ?>
                </div>
            </a>
        </li>
    <?php endforeach; ?>
</ul>
<?php Pjax::end() ?>
<!--</div>-->
