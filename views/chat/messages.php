<?php

/** @var $history \app\models\ChatHistory[] */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\TextHelper;

?>

<div id="chat-center-messages" class="chat-center-messages">
    <?php if (isset($history)): ?>
        <?php Yii::info($history, 'test'); ?>
        <?php /** @var \app\models\ChatHistory $message */
        foreach ($history as $message): ?>
            <?php if ($message->recipient_id == Yii::$app->user->identity->id): ?>
                <div class="chat-message chat-message-client">
                    <div class="message-image-wrapper">
                        <?php Yii::info('Avatar: ' . $message->sender->id, 'test') ?>
                        <img src="<?= $message->sender->avatar ?? 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg' ?>"
                             alt="">
                    </div>
                    <div class="message-wrapper">
                        <div class="message">
                            <p><?= TextHelper::parseLinks($message->text); ?></p>
                            <span class="message-time"><?= $message->getFormattedDate(); ?></span>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <div class="chat-message chat-message-me">
                    <div class="message-image-wrapper">
                        <img src="<?= $message->sender->avatar ?? 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg' ?>"
                             alt="">
                    </div>
                    <div class="message-wrapper">
                        <div class="message">
                            <p><?= TextHelper::parseLinks($message->text); ?></p>
                            <span class="message-time"><?= $message->getFormattedDate(); ?></span>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <p class="no-partner">Выберите собеседника</p>
    <?php endif; ?>
    <input type="file" id="chat-btn-file" multiple="multiple" style="display: none">
    <div class="anchor"></div>
    <div class="chat-form">
        <?= Html::button('<i class="fa fa-plus-circle"></i>', [
            'id' => 'chat-upload-file-btn',
            'class' => 'chat-btn',
            'title' => 'Добавить',
        ]) ?>
        <?= Html::button('<i class="fa fa-smile-o"></i>', [
            'id' => 'chat-smile-btn',
            'class' => 'chat-btn',
            'title' => 'Смайлы',
        ]) ?>
        <?php $form = ActiveForm::begin(['id' => 'chat-form']) ?>
        <?php
        //        echo Html::input('text', 'chat_text', '', [
        //            'id' => 'chat-text',
        //            'autocomplete' => 'off',
        //            'placeholder' => 'Введите текст сообщения...'
        //        ])
        ?>
        <textarea id="chat-text" type="text"></textarea>
<!--        <div id="chat-text" contenteditable="true"></div>-->
        <?= Html::button('<i class="fa fa-send"></i>', [
            'id' => 'chat-send-btn',
            'class' => 'chat-btn',
            'title' => 'Отправить',
        ]) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>
