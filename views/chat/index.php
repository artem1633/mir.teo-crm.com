<?php

use app\helpers\SmileHelper;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$sender = Yii::$app->user->identity->id;
$base_url = Url::base(true);

\johnitvn\ajaxcrud\CrudAsset::register($this);
/**
 * @var $chat_partners User[]
 */
?>
<?php Pjax::begin(['id' => 'pjax-chat-container'], ['style' => 'width: 100%']) ?>

<div class="chat-header">
    <div class="chat-left-header">
        <div class="sidebar-header">
            <?= Html::a('<img src="/img/menu-logo.png" style="height: 25px; margin: 8px 8px;">', ['user/profile']) ?>
            <?= Html::a('Мир24', '#') ?>
            <?= Html::a('Skype', '#') ?>
            <?= Html::a('Дзен', '#') ?>
        </div>
    </div>
    <div class="chat-right-header">
        <div class="chat-right-header-center">
            <div class="partner-info">
                <div class="partner-avatar">
                    <?php echo Html::img('', ['id' => 'chat-head-partner-avatar']) ?>
                </div>
                <div class="partner-description">
                    <div class="partner-name">
                    </div>
                    <div class="partner-work">
                    </div>
                </div>
            </div>

            <div class="info-btn">
                <a href="#" id="profile-link">
                    <?= Html::img('/img/info.png'); ?>
                </a>
            </div>
        </div>

        <div class="profile-block">
            <?php if (Yii::$app->user->isGuest == false): ?>
                <!-- begin header navigation right -->
                <ul class="nav navbar-nav" style="
    height: 41px;">
                    <li class="dropdown navbar-user">
                        <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                           style="margin-top: -7px;">
                            <img src="/<?= Yii::$app->user->identity->getRealAvatarPath() ?>" data-role="avatar-view"
                                 alt="">
                            <?php
                            $login = Yii::$app->user->identity->login;

                            if (strlen($login) > 15) {
                                $login = iconv_substr($login, 0, 13, "UTF-8") . '...';
                            }
                            ?>
                            <span class="hidden-xs"><?= $login ?>
                                &nbsp;-&nbsp;$<?= Yii::$app->user->identity->balance_usd ?></span> <b class="caret"></b>
                        </a>
                        <div style="text-align: right;">
                            <ul class="dropdown-menu animated fadeInLeft">
                                <li> <?= Html::a('Счет', ['accruals/index']) ?> </li>
                                <li> <?= Html::a('Профиль', ['user/profile']) ?> </li>
                                <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="chat-main">
    <div class="chat-left">
        <?= $this->render('partner_list.php'); ?>
    </div>

    <div id="chat-center" class="chat-center">
        <?= $this->render('messages') ?>
    </div>

    <div class="chat-right" style="display: none">
        <?php $smiles = new SmileHelper(); ?>
        <?php foreach ($smiles->getSmiles() as $smile_text => $smile_img): ?>
            <div class="smile">
                <img src="<?= $smile_img ?>" alt="<?= str_replace('|', '', $smile_text) ?>">
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php Pjax::end() ?>


<?php

$url = ($_ENV['PUSH_SERVER_HTTPS'] ? 'https://' : 'ws://')
    . $_ENV['PUSH_SERVER_HOST'] . ':' . $_ENV['PUSH_SERVER_PORT'];

$userId = Yii::$app->user->identity->getId();

$script = <<<JS

var recipient = '';

var socket = io('{$url}?from=server&userId={$userId}');

getUrlVars();

socket.on('message', function(data){
    // var data = JSON.parse(data);
    // console.log(data);
    // alert(123);
    $.pjax.reload('#pjax-chat-list-container');
    updateListMessage();
});
 
$(document).keydown(function(e) {
    if(e.keyCode === 13) {
        e.preventDefault();
        if (e.target.id === 'chat-text'){
            sendMessage();
        } else if (e.target.name === 'search_recipient'){
            console.log('Ищем собеседников');
             var s_text = $('.search-recipient').val();
            $.post(
                '/chat/search-partners',
                {
                    'search_text': s_text
                },
                function(response) {
                    // $('.chat-left').detach();
                    $('.chat-left').html(response);
                    recipient = null;
                    updateListMessage();
                    changePartnerFromHead(0);
                }
            )
        }
        console.log(e.target.name);
    }
  });

$(document).on('click', '#chat-smile-btn', function(){
    var right_block = $('.chat-right');
    right_block.slideToggle(400);
    right_block.css('display', 'flex');
    changeLeftSize();
});

$(document).on('click', '.list-item', function(){
    $('.list-item').removeClass('active-partner');
    $(this).addClass('active-partner');
    recipient = $(this).attr('data-chat-id');
    console.log('selected: ' + recipient);
    updateListMessage();
    changePartnerFromHead(recipient);
    $(this).find($('.unread-msg')).hide();    
});

function updateListMessage(){
    $.get(
        '/chat/get-messages?id=' + recipient,
        function(response) {
            $('#chat-center').html(response);
             var scrollTop = $('.anchor').offset().top;
             $('#chat-center-messages').scrollTop(scrollTop);
             setTimeout(function() {
                $('#chat-text').focus();
            }, 500);
        }
    );
    return true;
}

function changePartnerFromHead(partner_id) {
    var name_block = $('.partner-name');
    var image_block = $('#chat-head-partner-avatar');
    var work_block = $('.partner-work');
    var profile_link = $('#profile-link');
    $.get(
        '/chat/get-partner-info',
        {
            id: partner_id
        },
        function(response) {
            // debugger;
            if (response['success'] === 1){
                image_block.attr('src', response['avatar']);
                name_block.html(response['name']);
                work_block.html(response['work']);
                profile_link.attr('href', '/user/view?id=' + partner_id)
            } else {    
                image_block.attr('src', '');
                name_block.html('');
                work_block.html('');
                profile_link.attr('href', '#')
            }
        }
    )
}

$(document).on('keypress', '#chat-send-btn', function(e) {
    e.preventDefault();
    if(e.which == 13){
        sendMessage();
    }
});

function sendMessage() {
    if (recipient === ''){
        alert('Не выбран получатель сообщения!');
        return false;
    }
    
    var text = $('#chat-text').val();
    
    if (text === ''){
        return false;
    }
    var sender = {$sender};

    $.post({
        url: 'https://miruwir.com:3000/message',
        data: { chatId: {$userId}, text: "test", receiverId: recipient },
        done: function(response){
            console.log(response, 'Response from socket server');
        },
    });

    $.post(
        '/chat/send-message',
        {
            'ChatHistory[sender_id]': sender,
            'ChatHistory[recipient_id]': recipient,
            'ChatHistory[text]': text
        },
        function(response) {
            console.log(response);
          $('#chat-text').val('');
          updateListMessage();
        }
    )
};

$(document).on('click', '.smile', function() {
    var text_input =  $('#chat-text');
    console.log('smile click');
    var img = $(this).find('img');
    console.log(img.attr('src'));
    console.log('Insert to ' + text_input.attr('id'));
    img.clone().appendTo('#' + text_input.attr('id'));
    
});

$(document).on('click', '#chat-upload-file-btn', function() {
    setTimeout(function(){
        $('#chat-btn-file').trigger('click');
    }, 500);
});


$(document).on('change', 'input[type=file]', function(){
    var files = this.files;
    console.log(files);
    if(typeof files === 'undefined') return;
    var data = new FormData();
    $.each(files, function(key, value) {
        data.append(key, value);
    });
    data.append('chat_file_upload', 1);
    data.append('recipient', recipient);
    
    $.ajax({
        url: '/chat/upload-files',
        type: 'POST',
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(response, status, jqXHR){
            if (typeof response.error === 'undefined'){
                updateListMessage();
            } else {
                console.log(status,jqXHR);
            }
            console.log(response);
        }
    })
});

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    
     setTimeout(function(){
        $("[data-chat-id='" + vars['user'] + "']").trigger('click');
    }, 500);
    return vars;
};

 $(document).on('resize', '.chat-left', function(){
    console.log('resized');
});
 
function changeLeftSize() {
    var target_with = $('.chat-left').width();
    $('.chat-left-header').css('width', target_with + 75);
}
JS;

$this->registerJs($script); ?>


