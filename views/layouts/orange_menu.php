<?php

use yii\helpers\Url;

$controller_id = Yii::$app->controller->id;
$action_id = Yii::$app->controller->action->id;

?>

<div class="orange_menu">
    <div class="menu_und">  
        <a class="btn_menu_desc" href="#orange_menu_top"><div class="arrow_top">

            </div></a>
        <a class="btn_menu_desc" href="#orange_menu_bottom"><div class="arrow_bottom">

            </div></a>
        <div class="menu_carousel orange_menu_inner carousel owl-carousel owl-theme owl-loaded">
            <a href="/invite"><div class="menu_img green_menu"><img src="/img/orange_menu/1.png" alt="" title="Пригласить на сайт"></div></a>
            <!--<a href="/construction">  <div class="menu_img"><img src="/img/orange_menu/2.png" alt="" title="Уведомления"></div> </a>-->
            <a href="/construction">  <div class="menu_img"><img src="/img/orange_menu/raiting_yellow.png" alt="" title="Рейтинг"></div> </a>
            <a href="/construction">  <div class="menu_img"><img src="/img/orange_menu/news_yellow.png" alt="" title="Ленты"></div> </a> 
            <a href="/rod"><div id="dashboard_page" class="menu_img"><img src="/img/orange_menu/3.png" alt="" title="Родословная"></div></a>

            <a href="/site/profile">  <div id="user-profile" class="menu_img"><img src="/img/orange_menu/4.png" alt="" title="Профиль"></div> </a>
             <a href="<?= Url::to(['profile/balance']) ?>">  <div id="balance_page" class="menu_img <?= $controller_id == 'profile' && $action_id == 'balance' ? 'active_img_new' : '' ?>"><img src="/img/orange_menu/<?= $controller_id == 'profile' && $action_id == 'balance' ? '5_1.png' : '5.png' ?>" alt="" title="Счет"></div> </a>
            <a href="/wiki"> <div id="wiki_page" class="menu_img"><img src="/img/orange_menu/6.png" alt="" title="Викистраница"></div> </a>
            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/7.png" alt="" title="Фото"></div></a>
            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/8.png" alt="" title="Видео"></div></a>


            <a href="/construction">  <div class="menu_img"><img src="/img/orange_menu/11.png" alt="" title="Блог"></div></a>
            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/9.png" alt="" title="Связи"></div></a>
            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/13.png" alt="" title="Клубы"></div></a>

            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/anceta_yellow.png" alt="" title="Анкета"></div></a>
            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/rezume_yellow.png" alt="" title="Резюме"></div></a>

            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/shop_yellow.png" alt="" title="Магазин"></div></a>
            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/10.png" alt="" title="Книга отзывов"></div></a>
            <a href="/construction">  <div class="menu_img"><img src="/img/orange_menu/14.png" alt="" title="Статистика"></div></a>
            <a href="/construction"> <div class="menu_img"><img src="/img/orange_menu/18.png" alt="" title="Офис"></div></a>
            <a data-method="post" href="/site/logout"> <div class="menu_img"><img src="/img/orange_menu/19.png" alt="" title="Выход"></div></a>

        </div>
    </div>
</div> 