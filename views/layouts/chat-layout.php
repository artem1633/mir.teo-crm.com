<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

if (Yii::$app->controller->action->id === 'login') {
    /**
     * Do not use this code in your template. Remove it.
     * Instead, use the code  $this->layout = '//main-login'; in your controller.
     */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {


    app\assets\AppAsset::register($this);
    app\assets\ColorAdminAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <?= $this->head() ?>
        <link href="/theme/assets/css/theme/default.css" rel="stylesheet">
        <link href="/css/chat.css" rel="stylesheet">
        <!-- ================== END BASE CSS STYLE ================== -->
        <script src="/theme/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>

    <?php $this->beginBody() ?>

    <div id="page-container" class="page-container chat-main"

    <?= $this->render(
        'chat_content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>

    <?= $this->render(
        'settings.php'
    ) ?>

    </div>

    <?php $this->endBody() ?>
<!---->
<!--    <script>-->
<!--        $(document).ready(function () {-->
<!--            App.init();-->
<!--        });-->
<!--    </script>-->

    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>

