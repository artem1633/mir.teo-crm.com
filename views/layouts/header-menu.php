<?php

use app\admintheme\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

$level1Count = 0;
$level2Count = 0;
$level3Count = 0;
$level4Count = 0;
$level5Count = 0;
$level6Count = 0;
$level7Count = 0;

if (Yii::$app->user->isGuest == false) {
    $levels = Yii::$app->user->identity->refer->getLevelsCount();

    $level1Count = $levels['1'];
    $level2Count = $levels['2'];
    $level3Count = $levels['3'];
    $level4Count = $levels['4'];
    $level5Count = $levels['5'];
    $level6Count = $levels['6'];
    $level7Count = $levels['7'];
}

?>
    <div class="invite">
        <?= Html::a('Заработать', '#', [
            'class' => 'btn btn--orange',
            'data-ref-url' => Yii::$app->user->identity->getSelfRefHref(),
            'onclick' => 'copyToClipboard($(this).data("ref-url")); alert("Ссылка скопирована в буфер обмена")'
        ]); ?>

        <p class="invite__text">
            Нажмите, чтобы скопировать реферальную
            ссылку для приглашений на сайт и получения
            реферальных доходов
        </p>
        <a href="#" class="invite__details">
            Подробности здесь
        </a>
    </div>
    <div class="referrals">
        <ul class="referrals__list">
            <li class="active referrals__title">
                Все рефералы (<span>0</span>)
            </li>
            <li>
                <?= Html::a("Уровень 1 ({$level1Count})", ['dashboard/index', 'UserSearch[level]' => 1]) ?>
            </li>
            <li>
                <?= Html::a("Уровень 2 ({$level2Count})", ['dashboard/index', 'UserSearch[level]' => 2]) ?>
            </li>
            <li>
                <?= Html::a("Уровень 3 ({$level3Count})", ['dashboard/index', 'UserSearch[level]' => 3]) ?>
            </li>
            <li>
                <?= Html::a("Уровень 4 ({$level4Count})", ['dashboard/index', 'UserSearch[level]' => 4]) ?>
            </li>
            <li>
                <?= Html::a("Уровень 5 ({$level5Count})", ['dashboard/index', 'UserSearch[level]' => 5]) ?>
            </li>
            <li>
                <?= Html::a("Уровень 6 ({$level6Count})", ['dashboard/index', 'UserSearch[level]' => 6]) ?>
            </li>
            <li>
                <?= Html::a("Уровень 7 ({$level7Count})", ['dashboard/index', 'UserSearch[level]' => 7]) ?>
            </li>
            <li>
                <?= Html::a("Рефереры", ['dashboard/parent']) ?>
            </li>
        </ul>
        <a href="#" class="btn btn--green" style="margin-left: 15px; margin-top: 15px;">
            Банеры
        </a>
    </div>
