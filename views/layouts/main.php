<?php
use app\helpers\AvatarResolver;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\UserSearch;

//$isGuest = Yii::$app->user->isGuest;
//if($isGuest)
//    $theme = 'default';
//else
//    $theme = Yii::$app->user->identity->theme;


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    app\assets\ColorAdminAsset::register($this);

    // dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <?=$this->head()?>
        <link href="/theme/assets/css/theme/default.css" rel="stylesheet">
        <!-- ================== END BASE CSS STYLE ================== -->
        <script src="/theme/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
        <link rel="shortcut icon" href="https://miruwir.com/fav.png" type="image/x-icon"/>
<!--        <script src="/assets/pace/pace.min.js"></script>-->
    </head>
    <body>

        <?php $this->beginBody() ?>



        <?php /* $this->render(
            //'header.php',
            //['directoryAsset' => $directoryAsset]
        ) */ ?>


          <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>          


<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>-->
<!--    <script src="/js/owl.carousel.js"></script>-->


    
<a href="<?= \yii\helpers\Url::toRoute(['chat/index']) ?>">
    <div class="message">
        <div class="message__inner">
            <div class="message__block message__block-blue">
                <span class="message__message-icon"></span>
            </div>
            <div class="message__block message__block-violet">
                <span class="message__favorite-icon"></span>
            </div>
            <div class="message__block message__block-orange">
                <span class="message__info-icon"></span>
            </div>
            <div class="message__block message__block-green">
                <span class="message__notification-icon"></span>
            </div>
        </div>
    </div>
</a>


    <div class="d-none">
        <?php $avatar = new \app\models\forms\AvatarForm(); $avatarForm = ActiveForm::begin(['id' => 'avatar-form', 'action' => ['user/upload-avatar'], 'options' => ['enctype' => 'multipart/form-data']])  ?>

        <?= $avatarForm->field($avatar, 'file')->fileInput() ?>

        <?php ActiveForm::end() ?>
    </div>


    <?php $this->endBody() ?>

    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>




    <?php /* $this->render(
        'main-script.php'
    ) */ ?>
    
<style>
[title='Выберите формат файла для экспорта']{
    padding: 3px !important;
    padding-bottom: 2px !important;
}
</style>
<script>


  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53034621-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Cleversite chat button -->
<script type='text/javascript'>
    (function() {
    var s = document['createElement']('script');
    s.type = 'text/javascript';
    s.async = true;
    s.charset = 'utf-8';
    s.src = '//cleversite.ru/cleversite/widget_new.php?supercode=1&referer_main='+encodeURIComponent(document.referrer)+'&clid=65676nZzNd&siteNew=85887';
    var ss = document['getElementsByTagName']('script')[0];
    if(ss) {
    ss.parentNode.insertBefore(s, ss);
    } else {
    document.documentElement.firstChild.appendChild(s);
    };
    })();
</script>
<!-- / End of Cleversite chat button -->

<script>
    if($('.index-page').length) {
        $('body').addClass('body_homepage');
    } else {
        $('body').removeClass('body_homepage');
    }
</script>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>

