<?php
use app\admintheme\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;


if (Yii::$app->user->isGuest == false) {
    $menu = Yii::$app->user->identity->refer->getMenu();
}


?>


<div class="account__header">
        <div class="container-fluid">
            <div class="account__header-inner">
                <div class="account__header-left">
                    <div class="account__header-block ">
                        <div class="hamburger-for-bluemenu">
                            <div class="hamburger hamburger1 ">
                                <span class="bar bar1"></span>
                                <span class="bar bar2"></span>
                                <span class="bar bar4"></span>
                            </div>
                        </div>
                        <a href="#" class="d-block">
                            <div class="account__photo">
                                <?php if($menu->avatar):
                                            if ( ($menu->aristocrat_dignity1 != null) && ($menu->code_vip == '33655') ) {
                                             ?>
                                             <img src="<?= $menu->gerb ?>" alt="" class="img_prev1">
                                              <?
                                         }
                                         else
                                         {
                                             ?>
                                             <img src="<?= $menu->avatar ?>" alt="" class="img_prev1">
                                                                        <?php
                                         }

                                    ?>

                                <?php endif; ?>
                            </div>                            
                        </a>    
                        <div class="account__name">
                            <?php if ($menu->play_name != null) echo $menu->play_name; else echo $menu->name;?> <br>
                                <span><?php echo $menu->country.' '.$menu->living_place?></span>
                            </div>
                    </div>
                </div>
                <div class="account__header-right">
                    <div class="account__header-block">
                        <div class="dropdown account__dropdown-block">
                            <button type="button" id="profileMenuButton" class="profile__menu-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="account__info-text">
                                    <div class="menu-dots">
                                    <span class="menu-dot dot1"></span>
                                    <span class="menu-dot dot2"></span>
                                    <span class="menu-dot dot3"></span>
                                </div></div>
                            </button>
                            <div class="account__dropdown-menu dropdown-menu" aria-labelledby="profileMenuButton">
                                <a href="#" class="account__dropdown-item account__dropdown-item--chat" title="Чат"><span>Чат</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item-lenta" title="Лента"><span>Лента</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--rod" title="Родословная"><span>Родословная</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--profile " title="Профиль"><span>Профиль</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--wiki active" title="Викистраница"><span>Викистраница</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--photo" title="Фото"><span>Фото</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--video" title="Видео"><span>Видео</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--blog" title="Блог"><span>Блог</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--conect" title="Связи"><span>Связи</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--community" title="Клубы"><span>Клубы</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--anketa" title="Анкета"><span>Анкета</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--rezume" title="Резюме"><span>Резюме</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--stor" title="Магазин"><span>Магазин</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--book" title="Книга отзывов"><span>Книга отзывов</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--office" title="Офис"><span>Офис</span></a>
                            </div>
                        </div>
                        <div class="dropdown dropdown_right">
                            <button type="button" id="profileEctionButton" class="profile__ection-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="menu-dots">
                                    <span class="menu-dot dot1"></span>
                                    <span class="menu-dot dot2"></span>
                                    <span class="menu-dot dot3"></span>
                                </div>
                            </button>
                            <div class="account__edition-dropdown dropdown-menu" aria-labelledby="profileEctionButton">
                                <a href="#" class="account__edition-item">Подружиться</a>
                                <a href="#" class="account__edition-item">Подписаться</a>
                                <a href="#" class="account__edition-item">Пожаловаться</a>
                                <a href="#" class="account__edition-item">Заблокировать</a>
                            </div>
                        </div>
                        <div class="hamburger-for-orangemenu">
                            <div class="hamburger hamburger1">
                                <span class="bar bar1"></span>
                                <span class="bar bar2"></span>
                                <span class="bar bar4"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




