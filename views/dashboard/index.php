<?php

use app\helpers\AvatarResolver;
use yii\bootstrap4\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\admintheme\widgets\Menu;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ArrayDataProvider */
$this->title = 'Родословная';

\app\assets\plugins\EqualsHeightAsset::register($this);
\johnitvn\ajaxcrud\CrudAsset::register($this);


$pages = new \yii\data\Pagination(['totalCount' => $dataProvider->count]);

$level1Count = 0;
$level2Count = 0;
$level3Count = 0;
$level4Count = 0;
$level5Count = 0;
$level6Count = 0;
$level7Count = 0;

if (Yii::$app->user->isGuest == false) {
    $levels = Yii::$app->user->identity->refer->getLevelsCount();

    $level1Count = $levels['1'];
    $level2Count = $levels['2'];
    $level3Count = $levels['3'];
    $level4Count = $levels['4'];
    $level5Count = $levels['5'];
    $level6Count = $levels['6'];
    $level7Count = $levels['7'];
    $level8Count = $levels['8'];

    $levelLiderCount = $levels['lider'];
    $levelLoserCount = $levels['loser'];


    $alllevelCount = $level1Count + $level2Count + $level3Count + $level4Count + $level5Count + $level6Count + $level7Count + $level8Count;
}

//$this->registerCssFile('/css/style_new.css');
//$this->registerCssFile('/css/owl.carousel.css');
//$this->registerCssFile('/css/owl.theme.green.css');
//$this->registerCssFile('/bootstrap/bootstrap.min.css');
//$this->registerCssFile('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap');
?>

<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>

<div class="main color_bg">
    <div class="main_main mCustomScrollbar" data-mcs-theme="minimal-dark">
        <div class="main__inner">
            <div id="header" class="header">
                <div class="row ">
                    <div class="menu_btn">

                        <div class="page_name">
                            <span>Моя родословная</span>
                            <a href="https://mirumir24.ru/refprog" target="_blank" class="">Подробности</a>
                        </div>
                        <div>
                            <?=
                            Html::a('Пригласить и заработать', '#', [
                                'class' => 'btn btn-warning',
                                'data-ref-url' => Yii::$app->user->identity->getSelfRefHref(),
                                'onclick' => 'copyToClipboard($(this).data("ref-url")); alert("Ссылка скопирована в буфер обмена")'
                            ]);
                            ?></div> 


                    </div>

                </div>
                <hr>
                <div class="row">
                    <div class="menu">

                        <div class="row">
                            <?php
                            $url_page = $_SERVER['REQUEST_URI'];
                            $url_page = explode('?', $url_page);
                            $url_page = $url_page[0];
                            ?>
                            <?php if ($_SERVER['QUERY_STRING'] == "" && $url_page != '/dashboard/parent' && $url_page != '/dashboard/lider' && $url_page != '/dashboard/loser') { ?>
                                <a class="Refs_target" href="/">Все рефералы (<span><?= $alllevelCount ?></span>)</a>
                            <?php } else { ?>
                                <a class="" href="/">Все рефералы (<span><?= $alllevelCount ?></span>)</a>
                            <?php } ?>

                                <?php if ($_SERVER['QUERY_STRING'] == "UserSearch%5Blevel%5D=1") { ?>
                                <span class="Refs_target">
                                <?= Html::a("Уровень 1 (<span>{$level1Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 1]) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Уровень 1 (<span>{$level1Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 1]) ?>
                            <?php } ?>
                                <?php if ($_SERVER['QUERY_STRING'] == "UserSearch%5Blevel%5D=2") { ?>
                                <span class="Refs_target">
                                <?= Html::a("Уровень 2 (<span>{$level2Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 2]) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Уровень 2 (<span>{$level2Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 2]) ?>
                            <?php } ?>

                                <?php if ($_SERVER['QUERY_STRING'] == "UserSearch%5Blevel%5D=3") { ?>
                                <span class="Refs_target">
                                <?= Html::a("Уровень 3 (<span>{$level3Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 3]) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Уровень 3 (<span>{$level3Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 3]) ?>
                            <?php } ?>
                                <?php if ($_SERVER['QUERY_STRING'] == "UserSearch%5Blevel%5D=4") { ?>
                                <span class="Refs_target">
                                <?= Html::a("Уровень 4 (<span>{$level4Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 4]) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Уровень 4 (<span>{$level4Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 4]) ?>
                            <?php } ?>
                                <?php if ($_SERVER['QUERY_STRING'] == "UserSearch%5Blevel%5D=5") { ?>
                                <span class="Refs_target">
                                <?= Html::a("Уровень 5 (<span>{$level5Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 5]) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Уровень 5 (<span>{$level5Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 5]) ?>
                            <?php } ?>
                                <?php if ($_SERVER['QUERY_STRING'] == "UserSearch%5Blevel%5D=6") { ?>
                                <span class="Refs_target">
                                <?= Html::a("Уровень 6 (<span>{$level6Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 6]) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Уровень 6 (<span>{$level6Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 6]) ?>
                            <?php } ?>
                                <?php if ($_SERVER['QUERY_STRING'] == "UserSearch%5Blevel%5D=7") { ?>
                                <span class="Refs_target">
                                <?= Html::a("Уровень 7 (<span>{$level7Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 7]) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Уровень 7 (<span>{$level7Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 7]) ?>
                            <?php } ?>
                                <?php if ($_SERVER['QUERY_STRING'] == "UserSearch%5Blevel%5D=8") { ?>
                                <span class="Refs_target">
                                <?= Html::a("Уровень 8+ (<span>{$level7Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 8]) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Уровень 8+ (<span>{$level7Count}</span>)", ['dashboard/index', 'UserSearch[level]' => 8]) ?>
            <?php } ?>


                                <?php if ($url_page == '/dashboard/parent') { ?>
                                <span class="Refs_target">
                                <?= Html::a("Рефереры", ['dashboard/parent']) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Рефереры", ['dashboard/parent']) ?>
                            <?php } ?>

                                <?php if ($url_page == '/dashboard/lider') { ?>
                                <span class="Refs_target">
                                <?= Html::a("Лидеры (<span>{$levelLiderCount}</span>)", ['dashboard/lider']) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Лидеры (<span>{$levelLiderCount}</span>)", ['dashboard/lider']) ?>
                            <?php } ?>     
                                <?php if ($url_page == '/dashboard/loser') { ?>
                                <span class="Refs_target">
                                <?= Html::a("Лузеры (<span>{$levelLoserCount}</span>)", ['dashboard/loser']) ?></span>
                            <?php } else { ?>
                                <?= Html::a("Лузеры (<span>{$levelLoserCount}</span>)", ['dashboard/loser']) ?>
            <?php } ?>   




                        </div>                            
                    </div>
                </div>
            </div>



            <div class="shop rod">    

            <div class="row">

                <?php
                $url_page = $_SERVER['REQUEST_URI'];
                $url_page = explode('?', $url_page);
                $url_page = $url_page[0];
                if ($url_page != '/dashboard/loser' && $url_page != '/dashboard/lider') {
                    ?>
                    <?php foreach ($dataProvider->getModels() as $model): ?>
                        <?php $tmp_count = 0; ?>
                        <?php foreach ($dataProvider2->models as $modeling): ?>
                            <?php
                            if ($model['id'] == $modeling['ref_id']) {
                                $tmp_count++;
                            }
                            ?>
                        <?php endforeach; ?>  
                        <div class="element col-xl-1 col-lg-2 col-md-2 col-sm-4 col-6">
                            <a href="<?= \yii\helpers\Url::toRoute(['user/view', 'id' => $model['id']]) ?>" class="">
                                <div class="block ">
                                    <div class="block_img">
                                        <img src="/<?= AvatarResolver::getRealAvatarPath($model['avatar']) ?>" alt="">
                                    </div>
                                    <div class="block-content">
                                        <h3><?= $model['name'] ?></h3>
                                        <p><?= implode([$model['country'], $model['living_place']]) ?> <br> <?= $model['created_at'] ?> </p>
                                        <p class="bold">У<?= $model['level'] ?> <?= $tmp_count ?> реф. $<?= $model['balance_usd'] ?> </p>
                                    </div>
                                </div></a>
                        </div>            
                    <?php endforeach; ?>
                <?php }
                if ($url_page == '/dashboard/loser') {
                    ?>

                    <?php foreach ($dataProvider->models as $model): ?>
                        <?php $tmp_count = 0; ?>
                        <?php foreach ($dataProvider->models as $modeling): ?>
                            <?php
                            if ($model['id'] == $modeling['ref_id']) {
                                $tmp_count++;
                            }
                            ?>
                        <?php endforeach; ?>  
                        <?php if ($tmp_count == 0) { ?>
                            <div class="element col-xl-1 col-lg-2 col-md-2 col-sm-4 col-6">
                                <a href="<?= \yii\helpers\Url::toRoute(['user/view', 'id' => $model['id']]) ?>" class="">  
                                    <div class="block ">
                                        <div class="block_img">
                                            <img src="/<?= AvatarResolver::getRealAvatarPath($model['avatar']) ?>" alt="">
                                        </div>
                                        <div class="block-content">
                                            <h3><?= $model['name'] ?></h3>
                                            <p><?= implode([$model['country'], $model['living_place']]) ?> <br> <?= $model['created_at'] ?> </p>
                                            <p class="bold">У<?= $model['level'] ?> <?= $tmp_count ?> реф. $<?= $model['balance_usd'] ?> </p>
                                        </div>
                                    </div></a>
                            </div>  

                <?php } ?>         
            <?php endforeach; ?>


                <?php } ?>

                <?php if ($url_page == '/dashboard/lider') { ?>

                    <?php foreach ($dataProvider->models as $model): ?>
                        <?php $tmp_count = 0; ?>
                        <?php foreach ($dataProvider->models as $modeling): ?>
                            <?php
                            if ($model['id'] == $modeling['ref_id']) {
                                $tmp_count++;
                            }
                            ?>
                        <?php endforeach; ?>  
                        <?php if ($model['balance_usd'] >= 1000) { ?>
                            <div class="element col-xl-1 col-lg-2 col-md-2 col-sm-4 col-6">
                                <a href="<?= \yii\helpers\Url::toRoute(['user/view', 'id' => $model['id']]) ?>" class="">  
                                    <div class="block ">
                                        <div class="block_img">
                                            <img src="/<?= AvatarResolver::getRealAvatarPath($model['avatar']) ?>" alt="">
                                        </div>
                                        <div class="block-content">
                                            <h3><?= $model['name'] ?></h3>
                                            <p><?= implode([$model['country'], $model['living_place']]) ?> <br> <?= $model['created_at'] ?> </p>
                                            <p class="bold">У<?= $model['level'] ?> <?= $tmp_count ?> реф. $<?= $model['balance_usd'] ?> </p>
                                        </div>
                                    </div></a>
                            </div>  

                <?php } ?>         
            <?php endforeach; ?>


        <?php } ?>


            </div>
        </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS

$('#user-main-form input').change(function(){
    $('#user-main-form').submit();
});

$('.card-content-text').equalHeights();

JS;

$this->registerJs($script, \yii\web\View::POS_READY);
?>
<?php
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // always need it for jquery plugin
])
?>
<?php Modal::end(); ?>
