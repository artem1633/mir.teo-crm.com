<?php

use yii\bootstrap4\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Главная';

\app\assets\plugins\EqualsHeightAsset::register($this);
\johnitvn\ajaxcrud\CrudAsset::register($this);


$pages = new \yii\data\Pagination(['totalCount' => $dataProvider->count]);

?>

<div class="col-md-12">
    <?php $form = ActiveForm::begin(['id' => 'user-main-form', 'method' => 'GET']); ?>

    <?= $form->field($searchModel, 'sorting')->radioList(
        ['1' => 'По чел.', '2' => 'По $', '3' => 'По дате', '4' => 'По стране', 'По городу'],
        [
            'item' => function($index, $label, $name, $checked, $value){
                return "
                    
                    <label class='checkcontainer ".($checked ? 'checkcontainer-checked' : '')."'>
                        {$label}
                        <input type='radio' name='{$name}' value='{$value}' ".($checked ? "checked='checked'": '').">
                    </label>
                ";
            },
        ]
    )->label('Сортировка:') ?>

    <?php ActiveForm::end() ?>
</div>

<?php foreach ($dataProvider->models as $model): ?>

    <div class="col-md-3 col-sm-4">
        <div class="panel panel-transparent profile-card">
            <div class="panel-body">
                <div class="row">
                    <?= Html::a('<img class="img-content" src="/'.\app\helpers\AvatarResolver::getRealAvatarPath($model['avatar']).'" alt="">', ['user/view', 'id' => $model['id']]) ?>
                    <div class="card-content">
                        <div class="card-content-text">
                            <h5><?= Html::a($model['name'], ['user/view', 'id' => $model['id']], ['style' => 'display: inline-block; width: 100%; word-break: break-word;']) ?></h5>
                            <p style="display: inline-block; height: 49px;"><?= implode(', ', [$model['country'], $model['living_place']]) ?> в 23:15</p>
                            <p>У<?=$model['level']?> чел. $<?= $model['balance_usd'] ?></p>
                        </div>
                        <a class="btn btn-success" href="/chat?user=<?= $model['id'] ?>" style="margin-top: 5px;">Связаться</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endforeach; ?>

<div class="col-md-12">
    <?= \yii\widgets\LinkPager::widget([
        'pagination' => $pages,
    ]) ?>
</div>

<?php

$script = <<< JS

$('#user-main-form input').change(function(){
    $('#user-main-form').submit();
});

$('.card-content-text').equalHeights();

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
