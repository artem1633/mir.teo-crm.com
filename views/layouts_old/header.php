<?php

use yii\helpers\Html;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top" style="min-height: unset; height: 41px;">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header">
                    <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                        <span class="navbar-logo"></span>
                        <?=Yii::$app->name?>
                    </a>
                    <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- end mobile sidebar expand / collapse button -->
                <?php
                if (Yii::$app->controller->id != 'chat'): ?>
                <ul class="header-menu">
                    <li><?= Html::a('Купоны', '#') ?></li>
                    <li><?= Html::a('Карта', '#') ?></li>
                    <li><?= Html::a('Тендеры', '#') ?></li>
                    <li><?= Html::a('Маркетплейс', '#') ?></li>
                    <li><?= Html::a('Доска', '#') ?></li>
                    <li><?= Html::a('Прокат', '#') ?></li>
                    <li><?= Html::a('Билеты', '#') ?></li>
                    <li class="dropdown">
                        <?= Html::a('Новости <i class="fa fa-angle-down"></i>', '#', ['class' => 'dropdown-toggle'.(isset($_GET['header-active']) && $_GET['header-active'] == 'news' ? ' active' : ''), 'data-toggle'=>'dropdown', 'role' => 'button',
                                    'onmouseover' => '
                            $("#news-dropown").toggle();
                        ']) ?>
                        <div id="news-dropown" class="dropdown-menu left-header left-header-gray animated fadeInLeft">
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/160px-Flag_of_Azerbaijan.svg.png" class="image-country"> Азербайджан', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#azer-dropown").show();']) ?>
                                <div id="azer-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('Вестник Кавказа', ['/site/source', 'header-active' => 'news', 's' => 'https://vestikavkaza.ru/azerbaijan']) ?>
                                    <?= Html::a('RT Азербайджан', ['/site/source', 'header-active' => 'news', 's' => 'https://russian.rt.com/tag/azerbaidzhan']) ?>
                                    <?= Html::a('Regnum Азербайджан', ['/site/source', 'header-active' => 'news', 's' => 'https://regnum.ru/foreign/caucasia/azerbaijan.html']) ?>
                                    <?php
//                                        echo Html::a('Новостной сайт Oxu.Az', ['/site/source', 'header-active' => 'news', 's' => 'https://ru.oxu.az/'])
                                    ?>
                                    <?= Html::a('Узнай всё Азербайджан', ['/site/source', 'header-active' => 'news', 's' => 'https://uznayvse.ru/region-news/azerbajdzhan-baku-novosti-segodnja.htm']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Flag_of_Armenia.svg/160px-Flag_of_Armenia.svg.png" class="image-country"> Армения', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#arm-dropown").show();']) ?>
                                <div id="arm-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('NEWS.am', ['/site/source', 'header-active' => 'news', 's' => 'https://news.am/rus/']) ?>
                                    <?= Html::a('Спутник Армения', ['/site/source', 'header-active' => 'news', 's' => 'https://ru.armeniasputnik.am/']) ?>
                                    <?= Html::a('Новости Армении', ['/site/source', 'header-active' => 'news', 's' => 'https://novostink.net/']) ?>
                                    <?php
//                                        echo Html::a('АМИ Армения', ['/site/source', 'header-active' => 'news', 's' => 'https://newsarmenia.am/news/'])
                                    ?>
                                    <?= Html::a('Армения - последние новости', ['/site/source', 'header-active' => 'news', 's' => 'https://plainnews.ru/armeniya']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/160px-Flag_of_Belarus.svg.png" class="image-country" style="object-position: left;"> Беларусь', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#bel-dropown").show();']) ?>
                                <div id="bel-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('Хартия\'97', ['/site/source', 'header-active' => 'news', 's' => 'https://charter97.org/']) ?>
                                    <?= Html::a('Новости Беларуси', ['/site/source', 'header-active' => 'news', 's' => 'https://news.tut.by/']) ?>
                                    <?= Html::a('Белорусский партизан', ['/site/source', 'header-active' => 'news', 's' => 'https://belaruspartisan.by/']) ?>
                                    <?php
//                                        echo Html::a('Белорусские новости', ['/site/source', 'header-active' => 'news', 's' => 'https://naviny.by/'])
                                    ?>
                                    <?= Html::a('БЕЛТА', ['/site/source', 'header-active' => 'news', 's' => 'https://www.belta.by/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Flag_of_Georgia_official.svg/160px-Flag_of_Georgia_official.svg.png" class="image-country" style="object-position: left;"> Грузия', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#geo-dropown").show();']) ?>
                                <div id="geo-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('ГрузияOnline', ['/site/source', 'header-active' => 'news', 's' => 'https://www.apsny.ge/']) ?>
                                    <?= Html::a('Грузия всегда с тобой', ['/site/source', 'header-active' => 'news', 's' => 'https://www.geomigrant.com']) ?>
                                    <?= Html::a('Спутник Грузия', ['/site/source', 'header-active' => 'news', 's' => 'https://sputnik-georgia.ru/']) ?>
                                    <?= Html::a('Грузия онлайн', ['/site/source', 'header-active' => 'news', 's' => 'https://russian.rt.com/inotv/catalog/channel/%D0%93%D1%80%D1%83%D0%B7%D0%B8%D1%8F%20Online']) ?>
                                    <?= Html::a('Новости Грузии', ['/site/source', 'header-active' => 'news', 's' => 'https://sololaki.ru/novosti']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Flag_of_Kazakhstan.svg/160px-Flag_of_Kazakhstan.svg.png" class="image-country"> Казахстан', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#kz-dropown").show();']) ?>
                                <div id="kz-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('Tengrinews', ['/site/source', 'header-active' => 'news', 's' => 'https://tengrinews.kz/']) ?>
                                    <?= Html::a('Новости Казахстан', ['/site/source', 'header-active' => 'news', 's' => 'https://www.zakon.kz/']) ?>
                                    <?= Html::a('informБЮРО', ['/site/source', 'header-active' => 'news', 's' => 'https://informburo.kz/']) ?>
                                    <?= Html::a('Baribar', ['/site/source', 'header-active' => 'news', 's' => 'https://baribar.kz/']) ?>
                                    <?= Html::a('Литера', ['/site/source', 'header-active' => 'news', 's' => 'https://liter.kz/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Flag_of_Kyrgyzstan.svg/160px-Flag_of_Kyrgyzstan.svg.png" class="image-country"> Киргизия', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#kir-dropown").show();']) ?>
                                <div id="kir-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('Sputnik Кыргызстан', ['/site/source', 'header-active' => 'news', 's' => 'https://sputnik.kg/']) ?>
                                    <?= Html::a('АКИпресс', ['/site/source', 'header-active' => 'news', 's' => 'https://m.akipress.org/kg/']) ?>
                                    <?= Html::a('Новости 24.KG', ['/site/source', 'header-active' => 'news', 's' => 'https://24.kg/']) ?>
                                    <?= Html::a('Акчабар', ['/site/source', 'header-active' => 'news', 's' => 'https://www.akchabar.kg/ru/']) ?>
                                    <?= Html::a('Портал Супер.KG', ['/site/source', 'header-active' => 'news', 's' => 'https://www.super.kg/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Flag_of_Latvia.svg/160px-Flag_of_Latvia.svg.png" class="image-country"> Латвия', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#lat-dropown").show();']) ?>
                                <div id="lat-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('DELFI', ['/site/source', 'header-active' => 'news', 's' => 'https://www.delfi.lv/']) ?>
                                    <?php
//                                        echo Html::a('TV NET', ['/site/source', 'header-active' => 'news', 's' => 'https://rus.tvnet.lv/'])
                                    ?>
                                    <?= Html::a('Открыто.lv', ['/site/source', 'header-active' => 'news', 's' => 'https://rus.jauns.lv/']) ?>
                                    <?= Html::a('SKATIES', ['/site/source', 'header-active' => 'news', 's' => 'https://skaties.lv/']) ?>
                                    <?= Html::a('SPOKI', ['/site/source', 'header-active' => 'news', 's' => 'https://spoki.lv/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Flag_of_Lithuania.svg/160px-Flag_of_Lithuania.svg.png" class="image-country"> Литва', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#lit-dropown").show();']) ?>
                                <div id="lit-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('DELFI', ['/site/source', 'header-active' => 'news', 's' => 'https://www.delfi.lt/']) ?>
                                    <?php
//                                        echo Html::a('15 минут новостей', ['/site/source', 'header-active' => 'news', 's' => 'https://www.15min.lt/'])
                                    ?>
                                    <?= Html::a('Литва TV3', ['/site/source', 'header-active' => 'news', 's' => 'https://www.tv3.lt/']) ?>
                                    <?= Html::a('Irytas.lt', ['/site/source', 'header-active' => 'news', 's' => 'https://www.lrytas.lt/']) ?>
                                    <?= Html::a('Alfa.lt', ['/site/source', 'header-active' => 'news', 's' => 'https://www.alfa.lt/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Flag_of_Moldova.svg/160px-Flag_of_Moldova.svg.png" class="image-country"> Молдавия', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#mol-dropown").show();']) ?>
                                <div id="mol-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('Sputnik Молдова', ['/site/source', 'header-active' => 'news', 's' => 'https://ru.sputnik.md/']) ?>
                                    <?= Html::a('Новости для молодежи', ['/site/source', 'header-active' => 'news', 's' => 'https://diez.md/']) ?>
                                    <?php
//                                        echo Html::a('Комсомольская правда', ['/site/source', 'header-active' => 'news', 's' => 'https://www.kp.md/'])
                                    ?>
                                    <?= Html::a('MLD-Media', ['/site/source', 'header-active' => 'news', 's' => 'https://noi.md/']) ?>
                                    <?= Html::a('ИНФОТАГ', ['/site/source', 'header-active' => 'news', 's' => 'http://www.infotag.md/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/160px-Flag_of_Russia.svg.png" class="image-country"> Россия', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#ru-dropown").show();']) ?>
                                <div id="ru-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?php
//                                        echo Html::a('РИА Новости', ['/site/source', 'header-active' => 'news', 's' => 'https://ria.ru/'])
                                    ?>
                                    <?php
//                                        echo Html::a('Комсомольская правда', ['/site/source', 'header-active' => 'news', 's' => 'https://www.kp.ru/'])
                                    ?>
                                    <?= Html::a('Газета.ru', ['/site/source', 'header-active' => 'news', 's' => 'https://www.gazeta.ru/']) ?>
                                    <?php
//                                        echo Html::a('Lenta.ru', ['/site/source', 'header-active' => 'news', 's' => 'https://lenta.ru/'])
                                    ?>
                                    <?= Html::a('РБК', ['/site/source', 'header-active' => 'news', 's' => 'https://www.rbc.ru/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Flag_of_Tajikistan.svg/160px-Flag_of_Tajikistan.svg.png" class="image-country"> Таджикистан', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#tdz-dropown").show();']) ?>
                                <div id="tdz-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?php
//                                        echo Html::a('ASIA-Plus', ['/site/source', 'header-active' => 'news', 's' => 'https://www.asiaplustj.info/'])
                                    ?>
                                    <?= Html::a('Пресса', ['/site/source', 'header-active' => 'news', 's' => 'https://tajikistantimes.com/']) ?>
                                    <?php
                                        echo Html::a('Новости Таджикистана', ['/site/source', 'header-active' => 'news', 's' => 'https://www.tojnews.org/'])
                                    ?>
                                    <?= Html::a('Вечёрка', ['/site/source', 'header-active' => 'news', 's' => 'https://vecherka.tj/']) ?>
                                    <?= Html::a('Авеста', ['/site/source', 'header-active' => 'news', 's' => 'http://avesta.tj/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Flag_of_Turkmenistan.svg/160px-Flag_of_Turkmenistan.svg.png" class="image-country"> Туркменистан', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#tur-dropown").show();']) ?>
                                <div id="tur-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('Золотой век', ['/site/source', 'header-active' => 'news', 's' => 'http://www.turkmenistan.gov.tm/']) ?>
                                    <?= Html::a('Туркмен Юрт ТВ', ['/site/source', 'header-active' => 'news', 's' => 'https://turkmenyurt.tv/']) ?>
                                    <?= Html::a('Информ-агентство', ['/site/source', 'header-active' => 'news', 's' => 'http://www.tdh.gov.tm/']) ?>
                                    <?= Html::a('Интернет-газета Туркменистан', ['/site/source', 'header-active' => 'news', 's' => 'http://www.turkmenistan.ru/']) ?>
                                    <?= Html::a('Медиа Туркмен', ['/site/source', 'header-active' => 'news', 's' => 'https://orient.tm/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Flag_of_Uzbekistan.svg/160px-Flag_of_Uzbekistan.svg.png" class="image-country"> Узбекистан', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#uzb-dropown").show();']) ?>
                                <div id="uzb-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('Daryo', ['/site/source', 'header-active' => 'news', 's' => 'https://daryo.uz/']) ?>
                                    <?= Html::a('АН Перец', ['/site/source', 'header-active' => 'news', 's' => 'https://qalampir.uz/']) ?>
                                    <?php
//                                        echo Html::a('АН Подробно', ['/site/source', 'header-active' => 'news', 's' => 'https://podrobno.uz/'])
                                    ?>
                                    <?= Html::a('Новости Узбекистана', ['/site/source', 'header-active' => 'news', 's' => 'https://uznews.uz/']) ?>
                                    <?= Html::a('Анализ новостей', ['/site/source', 'header-active' => 'news', 's' => 'https://sof.uz/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Ukraine.svg/160px-Flag_of_Ukraine.svg.png" class="image-country"> Украина', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#ua-dropown").show();']) ?>
                                <div id="ua-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?php
//                                        echo Html::a('Украина сегодня', ['/site/source', 'header-active' => 'news', 's' => 'https://www.segodnya.ua/'])
                                    ?>
                                    <?php
//                                        echo Html::a('Обозреватель', ['/site/source', 'header-active' => 'news', 's' => 'https://www.obozrevatel.com/'])
                                    ?>
                                    <?= Html::a('Корреспондент', ['/site/source', 'header-active' => 'news', 's' => 'https://korrespondent.net/']) ?>
                                    <?php
//                                        echo Html::a('Актуальные новости', ['/site/source', 'header-active' => 'news', 's' => 'https://znaj.ua/'])
                                    ?>
                                    <?= Html::a('НВ Украина', ['/site/source', 'header-active' => 'news', 's' => 'https://nv.ua/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Flag_of_Estonia.svg/160px-Flag_of_Estonia.svg.png" class="image-country"> Эстония', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#est-dropown").show();']) ?>
                                <div id="est-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <?= Html::a('МК-Эстония', ['/site/source', 'header-active' => 'news', 's' => 'https://www.mke.ee/']) ?>
                                    <?= Html::a('Деловые ведомости', ['/site/source', 'header-active' => 'news', 's' => 'https://www.dv.ee/']) ?>
                                    <?= Html::a('Postimees', ['/site/source', 'header-active' => 'news', 's' => 'https://www.postimees.ee/']) ?>
                                    <?= Html::a('DELFI', ['/site/source', 'header-active' => 'news', 's' => 'https://www.delfi.ee/']) ?>
                                    <?= Html::a('Sputnik', ['/site/source', 'header-active' => 'news', 's' => 'https://ee.sputniknews.ru/']) ?>
                                </div>
                            </div>
                            <div class="dropdown">
                                <?= Html::a('Другие страны', '#', ['class' => 'dropdown-item',
                                    'onmouseover' => '$(".country-dropdown").hide(); $("#other-dropown").show();']) ?>
                                <div id="other-dropown" class="country-dropdown dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                    <div class="dropdown">
                                        <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/160px-Flag_of_the_United_Kingdom.svg.png" class="image-country"> Великобритания ', '#', ['class' => 'dropdown-item',
                                            'onmouseover' => '$(".country-dropdown-d").hide(); $("#uk-dropown").show();']) ?>
                                        <div id="uk-dropown" class="country-dropdown-d dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                            <?= Html::a('The Guardian', ['/site/source', 'header-active' => 'news', 's' => 'https://www.theguardian.com/uk']) ?>
                                            <?= Html::a('BBC News', ['/site/source', 'header-active' => 'news', 's' => 'https://www.bbc.com/news/uk']) ?>
                                            <?= Html::a('Mail Online', ['/site/source', 'header-active' => 'news', 's' => 'https://www.dailymail.co.uk/']) ?>
                                            <?= Html::a('The Telegraph', ['/site/source', 'header-active' => 'news', 's' => 'https://www.telegraph.co.uk/']) ?>
                                            <?= Html::a('INDEPENDENT', ['/site/source', 'header-active' => 'news', 's' => 'https://www.independent.co.uk/']) ?>
                                        </div>
                                    </div>
                                    <div class="dropdown">
                                        <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/160px-Flag_of_Germany.svg.png" class="image-country"> Германия ', '#', ['class' => 'dropdown-item',
                                            'onmouseover' => '$(".country-dropdown-d").hide(); $("#de-dropown").show();']) ?>
                                        <div id="de-dropown" class="country-dropdown-d dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                            <?= Html::a('Bild', ['/site/source', 'header-active' => 'news', 's' => 'https://www.bild.de/']) ?>
                                            <?= Html::a('BZ Berlin', ['/site/source', 'header-active' => 'news', 's' => 'https://www.bz-berlin.de/']) ?>
                                            <?= Html::a('WELT', ['/site/source', 'header-active' => 'news', 's' => 'https://www.welt.de/']) ?>
                                            <?= Html::a('Morgenpost', ['/site/source', 'header-active' => 'news', 's' => 'https://www.morgenpost.de/']) ?>
                                            <?= Html::a('GERMANY24', ['/site/source', 'header-active' => 'news', 's' => 'https://germany24.ru/ru/news/']) ?>
                                        </div>
                                    </div>
                                    <div class="dropdown">
                                        <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_India.svg/160px-Flag_of_India.svg.png" class="image-country"> Индия ', '#', ['class' => 'dropdown-item',
                                            'onmouseover' => '$(".country-dropdown-d").hide(); $("#in-dropown").show();']) ?>
                                        <div id="in-dropown" class="country-dropdown-d dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                            <?= Html::a('The Times of India', ['/site/source', 'header-active' => 'news', 's' => 'https://timesofindia.indiatimes.com/']) ?>
                                            <?= Html::a('hindustantimes', ['/site/source', 'header-active' => 'news', 's' => 'https://www.hindustantimes.com/']) ?>
                                            <?= Html::a('THE HINDU', ['/site/source', 'header-active' => 'news', 's' => 'https://www.thehindu.com/']) ?>
                                            <?= Html::a('FINANCIAL EXPRESS', ['/site/source', 'header-active' => 'news', 's' => 'https://www.financialexpress.com/']) ?>
                                            <?= Html::a('The Indian EXPRESS', ['/site/source', 'header-active' => 'news', 's' => 'https://indianexpress.com/']) ?>
                                        </div>
                                    </div>
                                    <div class="dropdown">
                                        <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/160px-Flag_of_Spain.svg.png" class="image-country"> Испания', '#', ['class' => 'dropdown-item',
                                            'onmouseover' => '$(".country-dropdown-d").hide(); $("#sp-dropown").show();']) ?>
                                        <div id="sp-dropown" class="country-dropdown-d dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                            <?= Html::a('EL PAIS', ['/site/source', 'header-active' => 'news', 's' => 'https://elpais.com/']) ?>
                                            <?= Html::a('EL MUNDO', ['/site/source', 'header-active' => 'news', 's' => 'https://www.elmundo.es/']) ?>
                                            <?= Html::a('ABC', ['/site/source', 'header-active' => 'news', 's' => 'https://www.abc.es/']) ?>
                                            <?= Html::a('LA VANGUARDYA', ['/site/source', 'header-active' => 'news', 's' => 'https://www.lavanguardia.com/']) ?>
                                            <?= Html::a('El Periodico', ['/site/source', 'header-active' => 'news', 's' => 'https://www.elperiodico.com/']) ?>
                                        </div>
                                    </div>
                                    <div class="dropdown">
                                        <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/160px-Flag_of_Italy.svg.png" class="image-country"> Италия', '#', ['class' => 'dropdown-item',
                                            'onmouseover' => '$(".country-dropdown-d").hide(); $("#it-dropown").show();']) ?>
                                        <div id="it-dropown" class="country-dropdown-d dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                            <?= Html::a('la Repubblica', ['/site/source', 'header-active' => 'news', 's' => 'https://www.repubblica.it/']) ?>
                                            <?= Html::a('CORRIERE DELLA SERA', ['/site/source', 'header-active' => 'news', 's' => 'https://www.corriere.it/']) ?>
                                            <?= Html::a('TGCOM24', ['/site/source', 'header-active' => 'news', 's' => 'https://www.tgcom24.mediaset.it/']) ?>
                                            <?= Html::a('Il Messaggero', ['/site/source', 'header-active' => 'news', 's' => 'https://www.ilmessaggero.it/']) ?>
                                            <?= Html::a('ANSAit', ['/site/source', 'header-active' => 'news', 's' => 'https://www.ansa.it/']) ?>
                                        </div>
                                    </div>
                                    <div class="dropdown">
                                        <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/160px-Flag_of_the_People%27s_Republic_of_China.svg.png" class="image-country"> Китай', '#', ['class' => 'dropdown-item',
                                            'onmouseover' => '$(".country-dropdown-d").hide(); $("#ch-dropown").show();']) ?>
                                        <div id="ch-dropown" class="country-dropdown-d dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                            <?= Html::a('Сhina News', ['/site/source', 'header-active' => 'news', 's' => 'https://m.chinanews.com/']) ?>
                                            <?= Html::a('Tencent Home', ['/site/source', 'header-active' => 'news', 's' => 'https://www.qq.com/']) ?>
                                            <?= Html::a('SOHU.com', ['/site/source', 'header-active' => 'news', 's' => 'https://www.sohu.com/']) ?>
                                            <?= Html::a('sina.com.cn', ['/site/source', 'header-active' => 'news', 's' => 'https://www.sina.com.cn/']) ?>
                                            <?= Html::a('Gmw.cn', ['/site/source', 'header-active' => 'news', 's' => 'http://gmw.cn/']) ?>
                                        </div>
                                    </div>
                                    <div class="dropdown">
                                        <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flag_of_the_United_States.svg/160px-Flag_of_the_United_States.svg.png" class="image-country"> США', '#', ['class' => 'dropdown-item',
                                            'onmouseover' => '$(".country-dropdown-d").hide(); $("#us-dropown").show();']) ?>
                                        <div id="us-dropown" class="country-dropdown-d dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                            <?= Html::a('CNN', ['/site/source', 'header-active' => 'news', 's' => 'https://us.cnn.com/']) ?>
                                            <?= Html::a('FOX NEWS', ['/site/source', 'header-active' => 'news', 's' => 'https://www.foxnews.com/']) ?>
                                            <?= Html::a('USA TODAY', ['/site/source', 'header-active' => 'news', 's' => 'https://eu.usatoday.com/']) ?>
                                            <?= Html::a('The Wall Street Journal', ['/site/source', 'header-active' => 'news', 's' => 'https://www.wsj.com/']) ?>
                                            <?= Html::a('The Huffington Post', ['/site/source', 'header-active' => 'news', 's' => 'https://www.huffpost.com/']) ?>
                                        </div>
                                    </div>
                                    <div class="dropdown">
                                        <?= Html::a('<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/160px-Flag_of_France.svg.png" class="image-country"> Франция', '#', ['class' => 'dropdown-item',
                                            'onmouseover' => '$(".country-dropdown-d").hide(); $("#fr-dropown").show();']) ?>
                                        <div id="fr-dropown" class="country-dropdown-d dropdown-menu left-header left-header-gray left-header-margin animated fadeInLeft">
                                            <?= Html::a('LE FIGARO', ['/site/source', 'header-active' => 'news', 's' => 'https://www.lefigaro.fr']) ?>
                                            <?= Html::a('LIBERATION', ['/site/source', 'header-active' => 'news', 's' => 'https://www.liberation.fr']) ?>
                                            <?= Html::a('LesEchos', ['/site/source', 'header-active' => 'news', 's' => 'https://www.lesechos.fr']) ?>
                                            <?= Html::a('Le Monde', ['/site/source', 'header-active' => 'news', 's' => 'https://www.lemonde.fr']) ?>
                                            <?= Html::a('LCI - Центр новостей', ['/site/source', 'header-active' => 'news', 's' => 'https://www.lci.fr']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </li>
                    <li class="mdsm-hidden"><?= Html::a('Погода', ['/site/source', 'header-active' => 'weather', 's' => 'https://www.gismeteo.ru/'], ['class' => (isset($_GET['header-active']) && $_GET['header-active'] == 'weather' ? ' active' : '')]) ?></li>
                    <li class="mdsm-hidden"><?= Html::a('ТВ', ['/site/source', 'header-active' => 'tv', 's' => 'http://lime-tv.ru/'], ['class' => (isset($_GET['header-active']) && $_GET['header-active'] == 'tv' ? ' active' : '')]) ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Радио', ['/site/source', 'header-active' => 'radio', 's' => 'https://radiopleer.ru/'], ['class' => (isset($_GET['header-active']) && $_GET['header-active'] == 'radio' ? ' active' : '')]) ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Кино', ['/site/source', 'header-active' => 'film', 's' => 'https://kinogo.by/'], ['class' => (isset($_GET['header-active']) && $_GET['header-active'] == 'film' ? ' active' : '')]) ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Игры', ['/site/source', 'header-active' => 'games', 's' => 'https://onlajnigry.net/'], ['class' => (isset($_GET['header-active']) && $_GET['header-active'] == 'games' ? ' active' : '')]) ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Работа', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Знакомства', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Офис', '#') ?></li>
                    <li><?= Html::a('Ещё', '#') ?></li>
                </ul>
                <?php endif; ?>
                <?php if(Yii::$app->user->isGuest == false): ?>
                    <!-- begin header navigation right -->
                    <ul class="nav navbar-nav navbar-right" style="
    background: #f0da58;
    height: 41px;">
                        <li class="dropdown navbar-user">
                            <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" style="margin-top: -7px;">
                                <img src="/<?= Yii::$app->user->identity->getRealAvatarPath() ?>" data-role="avatar-view" alt="">
                                <?php
                                    $login = Yii::$app->user->identity->login;

                                    if(strlen($login) > 15){
                                        $login = iconv_substr ($login, 0, 13, "UTF-8").'...';
                                    }
                                ?>
                                <span class="hidden-xs"><?=$login?> - $<?=Yii::$app->user->identity->balance_usd?></span> <b class="caret"></b>
                            </a>
                            <div style="text-align: right;">
                                <ul class="dropdown-menu animated fadeInLeft">
                                    <li> <?= Html::a('Счет', ['accruals/index']) ?> </li>
                                    <li> <?= Html::a('Профиль', ['user/profile']) ?> </li>
                                    <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
<!--                    <ul class="nav navbar-nav navbar-right">-->
<!--                        <li class="dropdown navbar-user" style="margin-top: 12px;">-->
<!--                            <p>--><?php //echo Yii::$app->user->identity->selfRefHref;?><!--</p>-->
<!--                        </li>-->
<!--                    </ul>-->
                    <!-- end header navigation right -->
                <?php endif; ?>
            </div>
            <!-- end container-fluid -->
        </div>