<?php

use app\admintheme\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

$level1Count = 0;
$level2Count = 0;
$level3Count = 0;
$level4Count = 0;
$level5Count = 0;
$level6Count = 0;
$level7Count = 0;

if (Yii::$app->user->isGuest == false) {
    $levels = Yii::$app->user->identity->refer->getLevelsCount();

    $level1Count = $levels['1'];
    $level2Count = $levels['2'];
    $level3Count = $levels['3'];
    $level4Count = $levels['4'];
    $level5Count = $levels['5'];
    $level6Count = $levels['6'];
    $level7Count = $levels['7'];
}

?>


    <div id="sidebar" class="sidebar" style="padding-top: 0; z-index: 2000; <?=(isset($_GET['s']) == true ? 'height: 10px;' : '')?>">


        <div class="sidebar-header">
            <?= Html::a('<img src="/img/menu-logo.png" style="height: 25px; margin: 8px 8px;">', ['dashboard/index']) ?>
            <div class="dropdown" style="display: inline-block;">
                <?= Html::a('Инфо <i class="fa fa-angle-down"></i>', '#', ['class' => 'dropdown-toggle', 'data-toggle'=>'dropdown', 'role' => 'button',
                    'onclick' => '
                    $("#header-dropown").toggle();
                ']) ?>
                <div id="header-dropown" class="dropdown-menu left-header animated fadeInLeft">
                    <?= Html::a('Правила и возможности', 'https://mirumir24.ru', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                    <?= Html::a('Игровые валюты и курсы', 'https://mirumir24.ru/tokens', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                    <?= Html::a('Зарабатывать на титулах', 'https://mirumir24.ru/titles', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                    <?= Html::a('Поставлять товары и услуги', 'https://mirumir24.ru/suppliers', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                    <?= Html::a('Работать в Интеркомах', 'https://mirumir24.ru/intercoms', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                    <?= Html::a('Приглашать и зарабатывать', 'https://mirumir24.ru/refprog', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                    <?= Html::a('Валютные кошельки', 'https://docs.google.com/document/d/1_h1tmkntZTFi_tnSUPREzv6s-k69ua1dLL0oiWJ4yMc/edit', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                    <?= Html::a('Справочная система', 'https://docs.google.com/spreadsheets/d/1x0jY9sJQZDLpHerxxjPuH4EtaKRre1XDmKtdpnf2S8M/edit?usp=sharing ', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                    <?= Html::a('Помощь', 'https://mirumir24.ru/help', ['class' => 'dropdown-item', 'target' => '_blank']) ?>
                </div>
            </div>
            <?= Html::a('Telegram', '#') ?>
            <?= Html::a('Дайджест', '#') ?>
            <?= Html::a('Блог', Url::to(['/blog/news/index'])) ?>
        </div>


        <?php if (Yii::$app->controller->id != 'chat'): ?>
            <?php if(isset($_GET['s']) == false): ?>
                <div class="left-menu">

                    <?= Html::a('Заработать', '#', [
                        'class' => 'btn btn-warning btn-sm',
                        'style' => 'margin-left: 10px; margin-top: 5px;',
                        'data-ref-url' => Yii::$app->user->identity->getSelfRefHref(),
                        'onclick' => 'copyToClipboard($(this).data("ref-url")); alert("Ссылка скопирована в буфер обмена")'
                    ]); ?>

                    <?php if (Yii::$app->user->isGuest == false): ?>
                        <?php
                        echo Menu::widget(
                            [
                                'options' => ['class' => 'nav', 'style' => 'margin-top: 20px;'],
                                'items' => [
                                    [
                                        'label' => 'Структура',
                                        'icon' => 'fa  fa-users',
                                        'url' => ['/dashboard/index'],
                                        'visible' => Yii::$app->user->identity->isAdmin()
                                    ],
                                    [
                                        'label' => 'Пользователи',
                                        'icon' => 'fa  fa-user-o',
                                        'url' => ['/user'],
                                        'visible' => Yii::$app->user->identity->isSuperAdmin()
                                    ],
                                    [
                                        'label' => 'Начисления',
                                        'icon' => 'fa  fa-plus',
                                        'url' => ['/accruals/index'],
                                        'visible' => Yii::$app->user->identity->isSuperAdmin()
                                    ],
                                    [
                                        'label' => 'Настройки',
                                        'icon' => 'fa  fa-cog',
                                        'url' => ['/settings'],
                                        'visible' => Yii::$app->user->identity->isSuperAdmin()
                                    ],
//                    ['label' => 'Тикеты', 'icon' => 'fa  fa-pencil', 'url' => ['/ticket'],],
//                    ['label' => 'Новости', 'icon' => 'fa  fa-th', 'url' => ['/news'],],
//                    ['label' => 'ЧАВО', 'icon' => 'fa  fa-th', 'url' => ['/faq'],],
                                ],
                            ]
                        );
                        ?>

                        <h4 class="refer-header"><?= Html::a('Все рефералы (' . Yii::$app->user->identity->refer->getTotalCount() . ')',
                                ['dashboard/index'], ['style' => 'color: #fff;']) ?></h4>
                        <ul class="users-filter-list">
                            <li><?= Html::a("Уровень 1 ({$level1Count})", ['dashboard/index', 'UserSearch[level]' => 1]) ?></li>
                            <li><?= Html::a("Уровень 2 ({$level2Count})", ['dashboard/index', 'UserSearch[level]' => 2]) ?></li>
                            <li><?= Html::a("Уровень 3 ({$level3Count})", ['dashboard/index', 'UserSearch[level]' => 3]) ?></li>
                            <li><?= Html::a("Уровень 4 ({$level4Count})", ['dashboard/index', 'UserSearch[level]' => 4]) ?></li>
                            <li><?= Html::a("Уровень 5 ({$level5Count})", ['dashboard/index', 'UserSearch[level]' => 5]) ?></li>
                            <li><?= Html::a("Уровень 6 ({$level6Count})", ['dashboard/index', 'UserSearch[level]' => 6]) ?></li>
                            <li><?= Html::a("Уровень 7 ({$level7Count})", ['dashboard/index', 'UserSearch[level]' => 7]) ?></li>
                            <li><?= Html::a("Рефереры", ['dashboard/parent']) ?></li>
                        </ul>

                        <?= Html::a('Оповестить', ['/chat/notify'],
                            [
                                'class' => 'btn btn-success btn-sm',
                                'style' => 'margin-left: 10px; margin-top: 5px;',
                                'role' => 'modal-remote',
                                'data-pjax' => 1,
                            ]); ?>

                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <?= $this->render('/chat/partner_list.php') ?>
        <?php endif; ?>
    </div>
