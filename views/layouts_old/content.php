<?php
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

use app\models\UserSearch;

?>
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<?php if(isset($_GET['s']) == false): ?>
<div class="content" <?php if(Yii::$app->controller->id == 'dashboard'): ?>style="width: 70%; overflow: hidden;"<?php endif; ?> id="content">
    <?php else: ?>
    <div class="content" style="padding: 0; margin: 0;" id="content">
    <?php endif; ?>

        <?php Pjax::begin(['enablePushState' => false, 'id' => 'report-messages-pjax']) ?>
        <?php if(Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success fade in m-b-15">
                <strong>Успех!</strong>
                <?=Yii::$app->session->getFlash('success')?>
                <span class="close" data-dismiss="alert">×</span>
                <div>

                </div>
            </div>
        <?php endif; ?>

        <?php if(Yii::$app->session->hasFlash('info')): ?>
            <div class="alert alert-info fade in m-b-15">
                <strong>Уведомление!</strong>
                <?=Yii::$app->session->getFlash('info')?>
                <span class="close" data-dismiss="alert">×</span>
                <div>

                </div>
            </div>
        <?php endif; ?>

        <?php if(Yii::$app->session->hasFlash('warning')): ?>
            <div class="alert alert-warning fade in m-b-15">
                <strong>Внимание!</strong>
                <?=Yii::$app->session->getFlash('warning')?>
                <span class="close" data-dismiss="alert">×</span>
                <div>

                </div>
            </div>
        <?php endif; ?>

        <?php if(Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger fade in m-b-15">
                <strong>Ошибка!</strong>
                <?=Yii::$app->session->getFlash('error')?>
                <span class="close" data-dismiss="alert">×</span>
                <div>

                </div>
            </div>
        <?php endif; ?>
        <?php Pjax::end() ?>

        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1 class="page-header">Basic Tables</h1>
        <?php } else { ?>
        <?php } ?>

<!--         <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?> -->

        <?= $content ?>
    <a href="<?= \yii\helpers\Url::toRoute(['chat/index']) ?>" style="display: block; width: 65px; height: 65px; background: #5f9abb; border-radius: 100%; position: fixed; bottom: 2%; right: 23%; z-index: 100; box-shadow: 1px 1px 7px;">
        <i class="fa fa-envelope" style="color: #fff; position: absolute; top: 20px; left: 21px; font-size: 22px;"></i>
        <?php
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $chat_partners = $dataProvider->getModels();
        $count = 0;

        foreach($chat_partners as $partner){
            $count += $partner->getUnreadMessagesCount();
        }

        ?>
        <span style="display: block; position: absolute; width: 20px; height: 20px; border-radius: 100%; background: #ff5b57; color: #fff; font-size: 11px; bottom: 0; right: 2px; text-align: center;"><?=$count?></span>
    </a>

</div>