<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Accruals */

?>
<div class="accruals-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
