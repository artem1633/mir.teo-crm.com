<?php

use yii\widgets\DetailView;
use yii\bootstrap4\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = "Пользователь «{$model->name}»";

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<div class="user-view">

    <div class="panel panel-warning">
        <div class="panel-heading">
            <h4 class="panel-title">Пользователь</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-9">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'play_name',
                        [
                            'attribute' => 'birth_date',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        'country',
                        'living_place',
                        'education',
                        'profession',
                        [
                            'attribute' => 'work_place',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'attribute' => 'work_position',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'attribute' => 'work_status',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'attribute' => 'phone',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        'interests',
                        'hobby',
                        [
                            'attribute' => 'role',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'attribute' => 'balance_usd',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'attribute' => 'last_active_datetime',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                        [
                            'attribute' => 'created_at',
                            'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        ],
                    ],
                ]) ?>
            </div>
            <div class="col-md-3">
                <img src="/<?=$model->getRealAvatarPath()?>" class="circle-img" style="height: 250px; width: 250px; border-radius: 100%; object-fit: contain; border: 2px solid #cecece;">
            </div>
        </div>
    </div>

</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
