<?php

use app\helpers\AvatarResolver;
use yii\bootstrap4\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Грузия - Новости';

\app\assets\plugins\EqualsHeightAsset::register($this);
\johnitvn\ajaxcrud\CrudAsset::register($this);


$pages = new \yii\data\Pagination(['totalCount' => $dataProvider->count]);

?>

<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>

<div class="main">
    <div class="main_frame">
        <iframe id="frame_frame" src="https://www.apsny.ge/" left="2.1%" frameborder="no" width="100%" height="100%" style="position:absolute;"></iframe>
    </div>
</div>
<script>
     document.getElementById("header").remove();     
 </script>

<?php

$script = <<< JS

$('#user-main-form input').change(function(){
    $('#user-main-form').submit();
});

$('.card-content-text').equalHeights();

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
