<?php

use yii\widgets\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */

if($model){
$this->title = "Викистраница «{$model->name}»";
}else{
   $this->title = "Викистраница не найдена"; 
}
if($model){
?>

<div class="account-page" style="height: 100%;">
<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>


<div class="account color_bg">
    
    <div class="scroll-block mCustomScrollbar" data-mcs-theme="minimal-dark">
        <div class="account__inner">
            <?= $this->render('/layouts/header-new-menu.php'); ?>
            <div class="account__body">
                <div class="account__body-top">
                    <div class="container-fluid">
                        <div class="account__user-info">
                            <div class="account__user-photo">
                                 <?php if($user_money->avatar): ?>
                                    <img src="/<?= $user_money->avatar?>" alt="">
                                <?php endif; ?>
                            </div>
                            <div class="account__user-content">
                                <div class="account__user-title">Викистраница</div>
                                <div class="account__user-name"><?php echo($model['name']);?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wiki_page wiki_in_page">   
   
    

    <div class="body">
        <div class="left_block forms_block">
            <div class="form-group field-wiki-name" style="width: 1445.08px;">
             <label ><?php echo($model['name']);?></label>
            </div>          
            
        </div>
        <div class="right_block  forms_block">
            <?php if($model->photo): ?>
                <img src="/uploads/<?= $model->photo?>" alt="">
            <?php endif; ?>
            <?php

            if ($model['birth']) {
                ?>
               <div class="birth_wiki">
               <div>Рождение </div>
               <div><?php echo($model['birth']);?> <br>
                <?php if($model['city_birth'] != 'Город рождения'){?>
               <?php echo($model['city_birth']);?>,
               <?php };
                    if($model['country_birth'] != 'Страна рождения'){
               ?>
               <?php echo($model['country_birth']);?> 
               <?php } ?></div> 
            </div>
            <?php
            }                        
            

if ($model['father'] != 'Отец' && $model['father'] != '') {
                ?>
               <div class="wiki-father" >
                <div>Отец</div>
             <div> <?php echo($model['father']);?></div>                
            </div>
            <?php
            }
                        if ($model['mother'] != 'Мать' && $model['mother'] != '') {
                ?>
               <div class="wiki-mother" >
                <div>Мать</div>
              <div><?php echo($model['mother']);?></div>                
            </div>
            <?php
            }
                        if ($model['wife'] != 'Жена' && $model['wife'] != '') {
                ?>
                <div class="wiki-wife" >   
                   <div> Супруга</div>
              <div><?php echo($model['wife']);?></div>             
            </div>
            <?php
            }
                        if ($model['children'] != 'Дети' && $model['children'] != '') {
                ?>
               <div class="wiki-children" > 
                <div>Дети</div>
              <div><?php echo($model['children']);?>    </div>           
            </div>
            <?php
            }
            if ($model['religion'] != 'Вероисповедание' && $model['religion'] != '') {
                ?>
               <div class="wiki-religion" >  
               <div> Вероисповедание</div>
              <div><?php echo($model['religion']);?> </div>             
            </div> 
            <?php
            }
            ?>                       
        </div>
        <div class="left_block body_left__block forms_block">
         <?php 
         if($model['description']){
         
            ?>         
            <div class="wiki-description" >  
            <?php echo($model['description']);?>               
            </div>
            <?php
            }
            if($model['purpose']){
         
            ?>
         <h2>Жизненное кредо, миссия, цели, мечты</h2>
            <div class="wiki-purpose" >  
            <?php echo($model['purpose']);?>                
            </div>
            <?php
            }
            if($model['childhood']){
         
            ?>
         <h2>Детство, юность</h2>
            <div class="wiki-childhood" >  
            <?php echo($model['childhood']);?>               
            </div>
            <?php
            }
            if($model['education']){
         
            ?>
          <h2>Образование</h2>
            <div class="wiki-education" > 
            <?php echo($model['education']);?>                   
            </div>
            <?php
            }
            if($model['work']){
         
            ?>
         <h2>Трудовая деятельность</h2>
            <div class="wiki-work" >   
            <?php echo($model['work']);?>                
            </div>
            <?php
            }
            if($model['today']){
         
            ?>
         <h2>Жизнь в настоящее время</h2>
            <div class="wiki-today" >   
            <?php echo($model['today']);?>              
            </div>
            <?php
            }
            if($model['charity']){
         
            ?>
         <h2>Общественная деятельность, благотворительность</h2>
            <div class="wiki-charity" > 
            <?php echo($model['charity']);?>                   
            </div>
            <?php
            }
            if($model['interests']){
         
            ?>
          <h2>Интересы</h2>
            <div class="wiki-interests" >  
            <?php echo($model['interests']);?>                  
            </div>
            <?php
            }
            if($model['hobby']){
         
            ?>
          <h2>Хобби, увлечения</h2>
            <div class="wiki-hobby" > 
            <?php echo($model['hobby']);?>                    
            </div>
            <?php
            }
            if($model['language']){
         
            ?>
         <h2>Иностранные языки</h2>
            <div class="wiki-language" > 
            <?php echo($model['language']);?>                  
            </div>
            <?php
            }
            if($model['dignities']){
         
            ?>
            <h2>Достоинства и недостатки</h2>
            <div class="wiki-dignities" >      
            <?php echo($model['dignities']);?>            
            </div>
            <?php
            }
            if($model['family']){
         
            ?>
         <h2>Семья</h2>
            <div class="wiki-family" >  
             <?php echo($model['family']);?>                
            </div>
            <?php
            }
            if($model['pets']){
         
            ?>
          <h2>Домашние питомцы</h2>
            <div class="wiki-pets" >    
             <?php echo($model['pets']);?>               
            </div>
            <?php
            }
            if($model['idols']){
         
            ?>
          <h2>Кумиры</h2>
            <div class="wiki-idols" >     
            <?php echo($model['idols']);?>             
            </div>
            <?php
            }
            if($model['joke']){
         
            ?>
           <h2>Любимый анекдот</h2>
            <div class="wiki-joke" > 
            <?php echo($model['joke']);?>                 
            </div>
            <?php
            }
            if($model['cars']){
         
            ?>
            <h2>Любимые автомобили</h2>
            <div class="wiki-cars" > 
            <?php echo($model['cars']);?>                   
            </div>
            <?php
            }
            
            if($model['brand']){
         
            ?>
           <h2>Любимые марки одежды, обуви, аксессуаров</h2>
            <div class="wiki-brand" >                
                <?php echo($model['brand']);?>  
            </div>
            <?php
            }
            
            if($model['food']){
         
            ?>
           <h2>Любимая еда, напитки</h2>
            <div class="wiki-food" >      
             <?php echo($model['food']);?>            
            </div>
            <?php
            }
            
            if($model['countries']){
         
            ?>
          <h2>Любимые страны, города, места</h2>
            <div class="wiki-countries" >  
            <?php echo($model['countries']);?>                
            </div>
            <?php
            }
            
            if($model['sport']){
         
            ?>
           <h2>Любимые виды спорта</h2>
            <div class="wiki-sport" >    
            <?php echo($model['sport']);?>              
            </div>
            <?php
            }
            
            if($model['book']){         
            ?>
           <h2>Любимые книги, авторы</h2>
            <div class="wiki-book" >   
            <?php echo($model['book']);?>                
            </div>
            <?php
            }
            
            if($model['music']){         
            ?>
          <h2>Любимая музыка, фильмы, сериалы</h2>
            <div class="wiki-music" >   
            <?php echo($model['music']);?>                
            </div>
            <?php
            }
            
            if($model['recommend']){         
            ?>
            <h2>Я рекомендую, советую</h2>
            <div class="wiki-recommend" >  
            <?php echo($model['recommend']);?>              
            </div>  
            <?php
            }
            

         ?>      
        </div>
        
    </div>
    
    <div class="stats_block">
        <hr>
        <img src="/img/views.png" alt=""> <?php echo($model['views']);?> <br>
        <img src="/img/likes.png" alt=""> 0 <br>
        <img src="/img/moneys.png" alt=""> <?php echo($user_money['balance_usd']);?> <br>
    </div>
  
</div>
</div>
            </div>
            
            
        </div>
    </div>
</div>





<?php
}else{
    ?>Пользователь не создал ещё вики страницу<?php 
}

?>


<script>
    $(document).ready(function(){
         $('.dropdown_right').on('click', function(e) {
                $(".account__edition-dropdown").toggleClass('account__edition-dropdown_block');
                $(".dropdown_right").toggleClass('dropdown_right_active');
            });
         $('.account__dropdown-block').on('click', function(e) {
                $(".account__dropdown-block").toggleClass('dropdown_right_active');
            });

         $(document).mouseup(function (e) {
    var container = $(".account__dropdown-block");
    var container2 = $(".account__edition-dropdown");
    var container3 = $(".dropdown_right");
    if (container.has(e.target).length === 0){
        container.removeClass('dropdown_right_active');
    }
    if (container3.has(e.target).length === 0){
       $(".dropdown_right").removeClass('dropdown_right_active');
    }
     if (container3.has(e.target).length === 0){
       $(".account__edition-dropdown").removeClass('account__edition-dropdown_block');
    }

});
        });
</script>


<script>
    $(document).ready(function(){
    $('#wiki_page').addClass('active_img_new');
    $('#wiki_page img').attr('src', '/img/orange_menu/6_1.png');
    $('#header').remove();
    $('body').removeClass('news_bg');
    $('.d-none').remove();
    setTimeout(function(){
        $('.main').removeClass('news_bg');
    }, 100); 

    $('.right_block label').text('');
    $('.right_block label').append( $('.right_block>img') );
    

    });
</script>

<script>
    $(document).ready(function(){
        if($(window).width() < 631){
    $('.account__user-content').css('width', $('.account__user-info').width() - 80)
}else{
    $('.account__user-content').css('width', '100%')
}
        if($(window).width() > 825){
            let tmp = -20;
            $('.left_block>div').each(function() {   
                tmp+=$(this).height();
                tmp+= 28;
                if(tmp <= $('.right_block').height()){
                   $(this).css('max-width', $('.wiki_page').width() - 310); 
                   
                }
            });       
        }else{
            $('.left_block>div').each(function() { 
                $(this).css('max-width', '100%');
            });
        }

    $(window).resize(function() {
if($(window).width() < 631){
    $('.account__user-content').css('width', $('.account__user-info').width() - 80)
}else{
    $('.account__user-content').css('width', '100%')
}
        if($(window).width() > 825){
            let tmp = 0;
            $('.left_block>div').each(function() {   
                tmp+=$(this).height();
                tmp+= 20;
                if(tmp <= $('.right_block').height()){
                   $(this).css('max-width', $('.wiki_page').width() - 310); 
                   
                }
            });       
        }else{
            $('.left_block>div').each(function() { 
                $(this).css('max-width', '100%');
            });
        }
});
});
</script>




<script>
    $(document).ready(function() {
        
        let tmp2=false;
        let tmp3=false;
        $('.hamburger-for-bluemenu').on('click', function() {
            //alert('ok');
            $('.blue_menu').toggleClass('opened');
            $('.account-page').toggleClass('move-left');
            
            if (tmp2 == false) {
                tmp2 = true;
                $('.left_block>div').each(function() {   
                if( $('.wiki_page').css('max-width') != '100%'){
                    let tmp = parseInt($('.left_block>div').css('max-width')) -2;
                    $('.left_block>div').css('max-width', tmp);
                }
            });
            }else{
 tmp2 = false;
                $('.left_block>div').each(function() {   
                if( $('.wiki_page').css('max-width') != '100%'){
                    let tmp = parseInt($('.left_block>div').css('max-width')) +2;
                    $('.left_block>div').css('max-width', tmp);
                }
            });
            }
            
        });
        
        $('.hamburger-for-orangemenu').on('click', function() {
            $('.orange_menu').toggleClass('opened');
            $('.account-page').toggleClass('move-right');

            if (tmp3 == false) {
                tmp3 = true;
                $('.left_block>div').each(function() {   
                if( $('.wiki_page').css('max-width') != '100%'){
                    let tmp = parseInt($('.left_block>div').css('max-width')) -2;
                    $('.left_block>div').css('max-width', tmp);
                }
            });
            }else{
 tmp3 = false;
                $('.left_block>div').each(function() {   
                if( $('.wiki_page').css('max-width') != '100%'){
                    let tmp = parseInt($('.left_block>div').css('max-width')) +2;
                    $('.left_block>div').css('max-width', tmp);
                }
            });
            }
        });
    });
</script>