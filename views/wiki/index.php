<?php

use app\helpers\AvatarResolver;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Вики';
?>
<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>

<div class="profile main color_bg wiki">
    <div class="main_main mCustomScrollbar" data-mcs-theme="minimal-dark">
        <div class="main__inner">
            <div class="profile__header">
                <div class="page-name">Моя викистраница</div>

                <div class="profile__header-right">
                    
                    <div class="watch_prew">Смотреть образец</div>
                    

                    <div class="hreff_but_info">Вставка и удаление ссылок</div>
                </div>
            </div>

            <div class="profile__body">
                <div class="wiki_page">
   
    <div class="hreff_block" >
        <div id="hreff_del_block" class="heff_block_in">
            <img src="/img/delete_hreff.png" alt="">
        </div>
        <div id="hreff_add_block" class="heff_block_in">
            <img src="/img/add_hreff.png" alt="">
        </div>        
    </div>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<div class="hidden_form" style="display: none">
   
        <?= $form->field($model, 'description')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'purpose')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'childhood')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'education')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'work')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'today')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'charity')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'interests')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'hobby')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'language')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'dignities')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'family')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'pets')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'idols')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'quote')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'joke')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'cars')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'brand')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'food')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'countries')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'sport')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'book')->textarea(['rows' => 2]); ?>
         <?= $form->field($model, 'music')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'recommend')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'city_birth')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'country_birth')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'father')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'mother')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'wife')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'children')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'religion')->textarea(['rows' => 2]); ?>
        <?= $form->field($model, 'birth')->textInput(); ?>
</div>

    <div class="body">
        <div class="left_block forms_block">
            <?= $form->field($model, 'name')->textInput(); ?>
        </div>
        <div class="right_block  forms_block">
            <?php if($model->photo): ?>
                <img src="/uploads/<?= $model->photo?>" alt="">
            <?php endif; ?>
            <?= $form->field($model, 'photo')->fileInput() ?>
            <div class="birth_wiki">
               Дата рождения 
               <select name="" id="day">
                   <option value="1">1</option>
                   <option value="2">2</option>
                   <option value="3">3</option>
                   <option value="4">4</option>
                   <option value="5">5</option>
                   <option value="6">6</option>
                   <option value="7">7</option>
                   <option value="8">8</option>
                   <option value="9">9</option>
                   <option value="10">10</option>
                   <option value="11">11</option>
                   <option value="12">12</option>
                   <option value="13">13</option>
                   <option value="14">14</option>
                   <option value="15">15</option>
                   <option value="16">16</option>
                   <option value="17">17</option>
                   <option value="18">18</option>
                   <option value="19">19</option>
                   <option value="20">20</option>
                   <option value="21">21</option>
                   <option value="22">22</option>
                   <option value="23">23</option>
                   <option value="24">24</option>
                   <option value="25">25</option>
                   <option value="26">26</option>
                   <option value="27">27</option>
                   <option value="28">28</option>
                   <option value="29">29</option>
                   <option value="30">30</option>
                   <option value="31">31</option>
               </select>
               <select name="" id="mouth">
                   <option value="январь">январь</option>
                   <option value="февраль">февраль</option>
                   <option value="март">март</option>
                   <option value="апрель">апрель</option>
                   <option value="май">май</option>
                   <option value="июнь">июнь</option>
                   <option value="июль">июль</option>
                   <option value="август">август</option>
                   <option value="сентябрь">сентябрь</option>
                   <option value="октябрь">октябрь</option>
                   <option value="ноябрь">ноябрь</option>
                   <option value="декабрь">декабрь</option>
               </select>
               <input type="text" class='form-control' id='year'>
            </div>
              <div class="wiki-city_birth" contenteditable="true" placeholder='Город рождения'>
            </div>
              <div class="wiki-country_birth" contenteditable="true" placeholder='Страна рождения'>                
            </div>
              <div class="wiki-father" contenteditable="true" placeholder='Отец'>                
            </div>
              <div class="wiki-mother" contenteditable="true" placeholder='Мать'>                
            </div>
              <div class="wiki-wife" contenteditable="true" placeholder='Супруга'>                
            </div>
              <div class="wiki-children" contenteditable="true" placeholder='Дети'>                
            </div>
              <div class="wiki-religion" contenteditable="true" placeholder='Вероисповедание'>                
            </div>             
        </div>
        <div class="left_block body_left__block forms_block">
         
            <h2>Кратко о себе в данный момент</h2>
            <div class="wiki-description" contenteditable>               
            </div>
            <h2>Жизненное кредо, миссия, цели, мечты</h2>
            <div class="wiki-purpose" contenteditable>                
            </div>
            <h2>Детство, юность</h2>
            <div class="wiki-childhood" contenteditable>                
            </div>
            <h2>Образование</h2>
            <div class="wiki-education" contenteditable>                
            </div>
            <h2>Трудовая деятельность</h2>
            <div class="wiki-work" contenteditable>                
            </div>
            <h2>Жизнь в настоящее время</h2>
            <div class="wiki-today" contenteditable>                
            </div>
            <h2>Общественная деятельность, благотворительность</h2>
            <div class="wiki-charity" contenteditable>                
            </div>
            <h2>Интересы</h2>
            <div class="wiki-interests" contenteditable>                
            </div>
            <h2>Хобби, увлечения</h2>
            <div class="wiki-hobby" contenteditable>                
            </div>
            <h2>Иностранные языки</h2>
            <div class="wiki-language" contenteditable>                
            </div>
            <h2>Достоинства и недостатки</h2>
            <div class="wiki-dignities" contenteditable>                
            </div>
            <h2>Семья</h2>
            <div class="wiki-family" contenteditable>                
            </div>
            <h2>Домашние питомцы</h2>
            <div class="wiki-pets" contenteditable>                
            </div>
            <h2>Кумиры</h2>
            <div class="wiki-idols" contenteditable>                
            </div>
            <h2>Любимая цитата</h2>
            <div class="wiki-quote" contenteditable>                
            </div>
            <h2>Любимый анекдот</h2>
            <div class="wiki-joke" contenteditable>                
            </div>
            <h2>Любимые автомобили</h2>
            <div class="wiki-cars" contenteditable>                
            </div>
            <h2>Любимые марки одежды, обуви, аксессуаров</h2>
            <div class="wiki-brand" contenteditable>                
            </div>
            <h2>Любимая еда, напитки</h2>
            <div class="wiki-food" contenteditable>                
            </div>
            <h2>Любимые страны, города, места</h2>
            <div class="wiki-countries" contenteditable>                
            </div>
            <h2>Любимые виды спорта</h2>
            <div class="wiki-sport" contenteditable>                
            </div>
            <h2>Любимые книги, авторы</h2>
            <div class="wiki-book" contenteditable>                
            </div>
            <h2>Любимая музыка, фильмы, сериалы</h2>
            <div class="wiki-music" contenteditable>                
            </div>
            <h2>Я рекомендую, советую</h2>
            <div class="wiki-recommend" contenteditable>                
            </div>            
        </div>
        
    </div>
    <div class="button_block">
        <?= Html::submitButton('Сохранить викистраницу', ['class' => 'btn btn--green profile__save-btn']); ?>
        <a href="/wiki/view?id=<?= $model->user_id?>" target="_blank"><button type="button" class="btn btn--blue profile__preview-btn">Смотреть превью</button></a>

    </div>
     <div class="stats_block">
        <hr>
        <img src="/img/views.png" alt=""> <?php echo($model['views']);?> <br>
        <img src="/img/likes_off.png" alt=""> 0 <br>
        <img src="/img/moneys_off.png" alt=""> <?php echo($user_money['balance_usd']);?> <br>
    </div>
  <?php ActiveForm::end(); ?>   
</div>
            </div>            
        </div>
    </div>
</div>




<script>
    $(document).ready(function(){
    $('#wiki_page').addClass('active_img_new');
    $('#wiki_page img').attr('src', '/img/orange_menu/6_1.png');
    $('#header').remove();
    $('body').removeClass('news_bg');
    setTimeout(function(){
        $('.main').removeClass('news_bg');
    }, 100); 

    $('.right_block label').text('');
    $('.right_block label').append( $('.right_block>img') );
    

    });
</script>





<script>
    $(document).ready(function(){
      setTimeout(function(){
        if($(window).width() > 825){
          let tmp2 = $('.profile__header').width();
             $(".hreff_block").css('width', tmp2);
            let tmp = -20;
            $('.left_block>div').each(function() {   
                tmp+=$(this).height();
                tmp+= 28;
                if(tmp < $('.right_block').height()){
                   $(this).css('width', $('.wiki_page').width() - 310); 
                   
                }
            });       
        }else{
          let tmp2 = $('.profile__header').width();
             $(".hreff_block").css('width', tmp2);
            $('.left_block>div').each(function() { 
                $(this).css('width', '100%');
            });
        }
}, 200); 
    $(window).resize(function() {

        if($(window).width() > 825){
            let tmp2 = $('.profile__header').width();
             $(".hreff_block").css('width', tmp2);
            let tmp = -20;
            $('.left_block>div').each(function() {   
                tmp+=$(this).height();
                tmp+= 28;
                if(tmp < $('.right_block').height()){
                   $(this).css('width', $('.wiki_page').width() - 310); 
                   
                }
            });       
        }else{
          let tmp2 = $('.profile__header').width();
             $(".hreff_block").css('width', tmp2);
            $('.left_block>div').each(function() { 
                $(this).css('width', '100%');
            });
        }
});
});
</script>

<script>/*
    $(document).ready(function(){        
        $(".profile__preview-btn").click(function(){  
            if($('.wiki_page').hasClass('wiki_in_page')){
                $('.wiki_page').removeClass('wiki_in_page')
                $('.left_block>div').each(function() { 
                $(this).attr('contenteditable', 'true');  
                if($(this).text() == ''){
                   $(this).css('display', 'block')
                    $(this).prev().css('display', 'block')                    
                }

            });
                $('.right_block div.demo').remove();                   
                $('.right_block>div').each(function() {
                     $(this).css('display', 'flex');
                     $(this).attr('contenteditable', 'true');   
                });
            }else{
                
                 $('.wiki_page').addClass('wiki_in_page')
            $('.left_block>div').each(function() { 
            $(this).attr('contenteditable', 'false');    
                if($(this).text() == ''){
                    $(this).css('display', 'none')
                    $(this).prev().css('display', 'none')                                        
                }
            }); 
                let tmp = 0;
                 $('.right_block>div').each(function() { 
                  tmp++;
                  if(tmp > 2){
                    $(this).attr('contenteditable', 'false');
                    let tmp2 = $(this).text()
                    switch (tmp) {
                    case 3:
                      if (tmp2 != 'Город') {
                        tmp2 = '<div class="demo">Город</div> <div> ' + tmp2 + ' </div>'
                        $(this).html(tmp2);
                      }else{
                        $(this).css('display', 'none');
                      }                      
                      break;
                    case 4:
                      if (tmp2 != 'Страна') {
                        tmp2 = '<div class="demo">Страна</div> <div> ' + tmp2 + ' </div>'
                        $(this).html(tmp2);
                      }else{
                        $(this).css('display', 'none');
                      }  
                      break;
                    case 5:
                      if (tmp2 != 'Отец') {
                        tmp2 = '<div class="demo">Отец</div> <div> ' + tmp2 + ' </div>'
                        $(this).html(tmp2);
                      }else{
                        $(this).css('display', 'none');
                      }  
                      break;
                      case 6:
                      if (tmp2 != 'Мать') {
                        tmp2 = '<div class="demo">Мать</div> <div> ' + tmp2 + ' </div>'
                        $(this).html(tmp2);
                      }else{
                        $(this).css('display', 'none');
                      }  
                      break;
                      case 7:
                      if (tmp2 != 'Жена') {
                        tmp2 = '<div class="demo">Жена</div> <div> ' + tmp2 + ' </div>'
                        $(this).html(tmp2);
                      }else{
                        $(this).css('display', 'none');
                      }  
                      break;
                      case 8:
                      if (tmp2 != 'Дети') {
                        tmp2 = '<div class="demo">Дети</div> <div> ' + tmp2 + ' </div>'
                        $(this).html(tmp2);
                      }else{
                        $(this).css('display', 'none');
                      }  
                      break;
                      case 9:
                      if (tmp2 != 'Вероисповедание') {
                        tmp2 = '<div class="demo">Вероисповедание</div> <div> ' + tmp2 + ' </div>'
                        $(this).html(tmp2);
                      }else{
                        $(this).css('display', 'none');
                      }  
                      break;
                    default:
                      
                  }
                    
                  }

            });
            }
             
        });
    });*/
</script>


<script>
    $(document).ready(function(){
        function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.wiki_page .right_block .field-wiki-photo label img').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$("#wiki-photo").change(function(){
    readURL(this);
});
    });
</script>


<script>
    $(document).ready(function(){
        $('.hidden_form textarea').each(function() {
            let tmp = $(this).val();    
            let tmp2 = $(this).attr('id');
            tmp2 = '.'+tmp2;
            $(tmp2).html(tmp);
        });

        $("body").on('DOMSubtreeModified', ".forms_block>div", function() {
            let tmp = $(this).html();    
            let tmp2 = $(this).attr('class');
            tmp2 ='#'+tmp2;
            $(tmp2).val(tmp); 
        });
       
    });
</script>
<script>
    $(document).ready(function(){
        let tmp = $('#wiki-birth').val().slice(0, -8);        
        tmp = parseInt(tmp);
        
        if (tmp < 10){
            tmp = '0'+tmp;
            
        }     
        tmp = parseInt(tmp);
        $('.birth_wiki #day').val(tmp);

        tmp = $('#wiki-birth').val().slice(0, -5);        
        tmp = tmp.slice(3); 
        switch (tmp) {
  case '01':
        tmp = 'январь';
    break;
  case '02':
        tmp = 'февраль';
    break;
  case '03':
        tmp = 'март';
    break;
    case '04':
        tmp = 'апрель';
    break;
  case '05':
        tmp = 'май';
    break;
  case '06':
        tmp = 'июнь';
    break;
    case '07':
        tmp = 'июль';
    break;
  case '08':
        tmp = 'август';
    break;
  case '09':
        tmp = 'сентябрь';
    break;
    case '10':
        tmp = 'октябрь';
    break;
  case '11':
        tmp = 'ноябрь';
    break;
  case '12':
        tmp = 'декабрь';
    break;
  default:
    tmp = '00';
} 
        $('.birth_wiki #mouth').val(tmp);

        tmp = $('#wiki-birth').val().slice(6);
        $('.birth_wiki #year').val(tmp);
    });
</script>
<script>
    $(document).ready(function(){
        let day = '01';
        $('.birth_wiki #day').change(function(){
            day = parseInt( $('.birth_wiki #day').val() )
            if(day < 10){
                day = '0'+day; 
            }          
           date = day + '.' + mouth + '.' + year;    
    $('#wiki-birth').val(date); 
    });
        let year = '1910';
        $('.birth_wiki #year').change(function(){
             year = parseInt( $('.birth_wiki #year').val() )
            if(year < 1900){
                year = 1900;
            }   
           date = day + '.' + mouth + '.' + year;    
    $('#wiki-birth').val(date);               
    });
        let mouth = '02';
        $('.birth_wiki #mouth').change(function(){
            mouth = $('.birth_wiki #mouth').val();
            switch (mouth) {
  case 'январь':
        mouth = '01';
    break;
  case 'февраль':
        mouth = '02';
    break;
  case 'март':
        mouth = '03';
    break;
    case 'апрель':
        mouth = '04';
    break;
  case 'май':
        mouth = '05';
    break;
  case 'июнь':
        mouth = '06';
    break;
    case 'июль':
        mouth = '07';
    break;
  case 'август':
        mouth = '08';
    break;
  case 'сентябрь':
        mouth = '09';
    break;
    case 'октябрь':
        mouth = '10';
    break;
  case 'ноябрь':
        mouth = '11';
    break;
  case 'декабрь':
        mouth = '12';
    break;
  default:
    mouth = '00';
} 

date = day + '.' + mouth + '.' + year;    
    $('#wiki-birth').val(date);
    });
    
});
</script>

<script>
    $(document).ready(function() {
        $('.hamburger-for-bluemenu').on('click', function() {
            //alert('ok');
            $('.blue_menu').toggleClass('opened');
            $('.account-page').toggleClass('move-left');
        });
        
        $('.hamburger-for-orangemenu').on('click', function() {
            $('.orange_menu').toggleClass('opened');
            $('.account-page').toggleClass('move-right');
        });
    });
</script>
<script>
  
  setInterval(
   function (){
      if (parseInt($('#mCSB_1_container').css('top')) != this.bT) {
        if($(window).width() > 769){
          if(parseInt($('#mCSB_1_container').css('top')) < -54){          
          $('.hreff_block').css({
        'position': 'fixed',
        'top': '0px'
      });
        }else{
          $('.hreff_block').css({
        'position': 'absolute',
        'top': '54px'
      });
        }
      }else{
        if($(window).width() > 599){
          if(parseInt($('#mCSB_1_container').css('top')) < -54){          
          $('.hreff_block').css({
        'position': 'fixed',
        'top': '40px'
      });
        }else{
          $('.hreff_block').css({
        'position': 'absolute',
        'top': '54px'
      });
        } 
        }else{
          if(parseInt($('#mCSB_1_container').css('top')) < -54){          
          $('.hreff_block').css({
        'position': 'fixed',
        'top': '50px'
      });
        }else{
          $('.hreff_block').css({
        'position': 'absolute',
        'top': '54px'
      });
        } 
        }
               
      }
      }
   }.bind({bT: parseInt($('#mCSB_1_container').css('top'))}),
   200
)
   
</script>
<script>
  // Ссылка (a)
$('body').on('click', '#hreff_add_block', function(){
  var url = prompt('Введите URL', '');
  document.execCommand('CreateLink', false, url);
  return false;
});

// Удаление ссылки
$('body').on('click', '#hreff_del_block', function(){
  document.execCommand('unlink', false, null);
  return false;
});
</script>
<?php

$script = <<< JS

$('#user-main-form input').change(function(){
    $('#user-main-form').submit();
});

$('.card-content-text').equalHeights();

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
