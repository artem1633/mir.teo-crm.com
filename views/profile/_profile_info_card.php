<?php

use app\helpers\AvatarResolver;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var \app\models\User $User
 */

?>

<div class="element col-xl-1 col-lg-2 col-md-2 col-sm-4 col-6">
    <div class="profile-block">
        <div class="block_img">
            <img src="/<?= AvatarResolver::getRealAvatarPath($User->avatar) ?>" alt="" class="mCS_img_loaded">
        </div>
        <div class="block-content">
            <h3><?= Html::encode($User->name) ?></h3>
        </div>
    </div>
</div>
