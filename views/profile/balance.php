<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Transfer;
use app\components\UserBalanceFormatter;
use yii\widgets\Pjax;
use app\models\Accruals;

/**
 * @var $this \yii\web\View
 * @var string $type
 * @var $currencies \app\models\Currency[]
 * @var $searchModel \app\models\TransferSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $model \app\models\forms\MoneyTransactionForm
 * @var integer $count_balance_records
 * @var array|bool $balance
 * @var array $ref_names
 **/

$limit = Yii::$app->request->post('limit');

$this->title = 'Счет';

?>

<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>

<div class="balance main color_bg">
    <div class="main_main mCustomScrollbar" data-mcs-theme="minimal-dark">
        <div class="main__inner">
            <div class="balance__header">
                <div class="balance__header-inner">
                    <div class="balance__header-left">
                        <div class="page-name">
                            Мой счет
                            <?= Html::a('Курсы', 'https://mirumir24.ru/tokens', ['class' => 'page-subname', 'target' => '_blank']) ?>
                        </div>
                        <div class="balance__total balance__total--mod">
                            <div class="balance__total-title">Всего в USD</div>
                            <div class="balance__total-summa"><?= UserBalanceFormatter::asBalance(Yii::$app->user->identity->balance_usd) ?></div>
                        </div>
                    </div>
                    <?php if($currencies): ?>
                        <div class="balance__currency">
                            <?php $i = 0; foreach($currencies as $currency): ?>
                                <?= Html::a(Html::encode($currency->name).' <span class="balance__currency-summa">'.($i == 0 ? UserBalanceFormatter::asBalance(Yii::$app->user->identity->balance_usd) : '0.00').'</span>', ['#'], ['class' => 'balance__currency-link '.($i == 0 ? 'active' : '')]) ?>
                            <?php $i++; endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <div class="balance__total balance__total--pc">
                        <div class="balance__total-title">Всего в USD</div>
                        <div class="balance__total-summa"><?= UserBalanceFormatter::asBalance(Yii::$app->user->identity->balance_usd) ?></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            
            <div class="balance__actions">
                <ul class="nav nav-tabs balance__actions-tabs" id="balanceTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="transfer-tab" data-toggle="tab" href="#balance__transfer" role="tab" aria-controls="balance__transfer" aria-selected="true">Перевод</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="exchange-tab" data-toggle="tab" href="#balance__exchange" role="tab" aria-controls="balance__exchange" aria-selected="false">Обмен</a>
                    </li>
                </ul>
                <div class="tab-content balance__actions-content" id="balanceTabContent">
                    <div class="tab-pane fade show active" id="balance__transfer" role="tabpanel" aria-labelledby="transfer-tab">
                        <?php $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'balance__action-form'
                            ],
                            'fieldConfig' => [
                                'errorOptions' => [
                                    'class' => 'help-block action-form__error'
                                ]
                            ]
                        ]) ?>

                            <?= $form->field($model, 'sum', ['enableAjaxValidation' => true, 'options' => ['class' => 'action-form__block']])->textInput(['class' => 'profile__input', 'placeholder' => 'Сумма*'])->label(false) ?>
                            <?= $form->field($model, 'profile_url', ['enableAjaxValidation' => true, 'template' => "{input}\n{error}<div id='profile-card' class='profile-card'></div>", 'options' => ['class' => 'action-form__block']])->textInput(['class' => 'profile__input', 'placeholder' => 'Ссылка на профиль*', 'data-url' => Url::to(['get-profile-info'])])->label(false) ?>
                            <?= $form->field($model, 'purpose_of_payment', ['options' => ['class' => 'action-form__block']])->textInput(['class' => 'profile__input', 'placeholder' => 'Назначение платежа'])->label(false) ?>

                            <div class="action-form__button">
                                <?= Html::submitButton('Отправить', ['class' => 'btn btn--green balance__action-btn']) ?>
                            </div>
                        <?php ActiveForm::end() ?>
                    </div>
                    <div class="tab-pane fade" id="balance__exchange" role="tabpanel" aria-labelledby="exchange-tab"></div>
                </div>
            </div>

            <?php Pjax::begin([
                'id' => 'balance',
                'timeout' => 5000
            ]) ?>
                <div class="balance__bills">
                    <div class="balance__bills-filters">
                        <?= Html::a('<span class="balance__bills-filter selected all-bills '.(!$type ? 'active' : '').'" data-type="all">Все транзакции </span>', ['profile/balance']) ?>
                        <?= Html::a('<span class="balance__bills-filter coming-bills '.($type == 'coming' ? 'active' : '').'" data-type="coming">Приходы</span>', ['profile/balance', 'type' => 'coming']) ?>
                        <?= Html::a('<span class="balance__bills-filter expenses-bills '.($type == 'expenses' ? 'active' : '').'" data-type="expenses">Расходы</span>', ['profile/balance', 'type' => 'expenses']) ?>
                        <?= Html::a('<span class="balance__bills-filter rod-bills '.($type == 'ref' ? 'active' : '').'" data-type="rod">Родословная</span>', ['profile/balance', 'type' => 'ref']) ?>
                        <span class="balance__bills-filter promotions-bills" data-type="promotions">Поощрения</span>
                        <div class="bills-search">
                            <?php $search = ActiveForm::begin([
                                'id' => 'balance-search',
                                'options' => [
                                    'data-pjax' => true
                                ]
                            ]) ?>
                                <?= $search->field($searchModel, 'search', ['options' => ['tag' => false]])->textInput(['class' => 'bills-search__input', 'placeholder' => 'Поиск...'])->label(false) ?>
                            <?php ActiveForm::end() ?>
                        </div>
                    </div>
                    <div class="balance__bills-lines">
                        <?php foreach($dataProvider->getModels() as $key => $model): ?>
                            <div class="balance__bills-line balance__bills-line--<?= Transfer::getBalanceBillsLineClass($model->from_user_id, $model->for_user_id) ?>">

                                <div class="balance__bills-block balance__bills-block--number">
                                    <div class="balance__bills-title"># платежа</div>
                                    <div class="balance__bills-info"><?= $model->payment_number ?></div>
                                </div>
                                <div class="balance__bills-block balance__bills-block--data">
                                    <div class="balance__bills-title">Дата и время</div>
                                    <div class="balance__bills-info"><?= date('d.m.Y, H:i', strtotime($model->date)) ?></div>
                                </div>
                                <div class="balance__bills-block">
                                    <div class="balance__bills-title">Отправитель</div>
                                    <?php if(Yii::$app->user->getId() == $model->from_user_id): ?>
                                        <div class="balance__bills-info">Я</div>
                                    <?php elseif(!$model->from_user_id): ?>
                                        <div class="balance__bills-info">МИР</div>
                                    <?php elseif(Yii::$app->user->getId() != $model->from_user_id): ?>
                                        <?php if($model instanceof Accruals): ?>
                                            <div class="balance__bills-info">МИР</div>
                                        <?php else: ?>
                                            <a href="<?= Url::to(['user/view', 'id' => isset($searchModel->user_ids[$model->id]) ? $searchModel->user_ids[$model->id] : '']) ?>" class="balance__bills-link" data-pjax="0">
                                                <?php if($model->fromUser): ?>
                                                    <?= Html::encode($model->fromUser->name ? $model->fromUser->name : $model->fromUser->login) ?>
                                                <?php else: ?>
                                                    <div class="balance__bills-info">Такого пользователя у нас нет</div>
                                                <?php endif ?>
                                            </a>
                                        <?php endif; ?>
                                    <?php elseif(!$model->from_user_id): ?>
                                        <div class="balance__bills-info">МИР</div>
                                    <?php endif ?>
                                </div>
                                <div class="balance__bills-block">
                                    <div class="balance__bills-title">Получатель</div>
                                    <?php if(Yii::$app->user->getId() == $model->for_user_id): ?>
                                        <div class="balance__bills-info">Я</div>
                                    <?php elseif(Yii::$app->user->getId() != $model->for_user_id): ?>
                                        <a href="<?= Url::to(['user/view', 'id' => $model->for_user_id]) ?>" class="balance__bills-link" data-pjax="0">
                                            <?php if($model->forUser): ?>
                                                <?= $model->forUser->name ? Html::encode($model->forUser->name) : Html::encode($model->forUser->login) ?>
                                            <?php else: ?>
                                                <div class="balance__bills-info">Такого пользователя у нас нет</div>
                                            <?php endif ?>
                                        </a>
                                    <?php elseif(!$model->for_user_id): ?>
                                        <div class="balance__bills-info">МИР</div>
                                    <?php endif ?>
                                </div>
                                <div class="balance__bills-block">
                                    <div class="balance__bills-title">Назначения платежа</div>
                                    <?php if($model instanceof Transfer): ?>
                                        <?php if(!$model->purpose_of_payment): ?>
                                            <div class="balance__bills-info">Не указано</div>
                                        <?php else: ?>
                                            <div class="balance__bills-info">
                                                <?= Html::encode($model->purpose_of_payment) ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <a href="<?= $model->from_user_id ? Url::to(['user/view', 'id' => $model->from_user_id]) : '' ?>" class="balance__bills-link" data-pjax="0"><?= Html::encode('Реферальная выплата '.$model->user_level.'-го уровня') ?></a>
                                    <?php endif ?>
                                </div>

                                <div class="bills-line__right">
                                    <div class="balance__bills-block">
                                        <div class="balance__bills-title"><?= Yii::$app->user->getId() == $model->from_user_id ? 'Расход' : 'Приход' ?></div>
                                        <div class="balance__bills-summa--<?= Transfer::getBalanceBillsLineClass($model->from_user_id, $model->for_user_id, true) ?>"><?= UserBalanceFormatter::asBalance($model->amount) ?></div>
                                    </div>
                                    <div class="balance__bills-block">
                                        <div class="balance__bills-title">Баланс</div>
                                        <div class="balance__bills-summa--total">
                                            <?= isset($balance[$key]) ? UserBalanceFormatter::asBalance($balance[$key]) : '' ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="balance__bills-bottom">
                        <?= $count_balance_records > 25 && $limit < $count_balance_records ? Html::a('<button class="btn btn--green balance__bill-more">Показать еще</button>', ['profile/balance', 'type' => $type], ['class' => '', 'data' => ['method' => 'post', 'pjax' => true, 'params' => ['limit' => $limit ? $limit + 25 : 50]]]) : '' ?>
                    </div>
                </div>
            <?php Pjax::end() ?>
            
<!--            <div class="balance__bills">-->
<!--                <div class="balance__bills-filters">-->
<!--                    <span class="balance__bills-filter selected all-bills active" data-type="all">Все транзакции </span>-->
<!--                    <span class="balance__bills-filter coming-bills" data-type="coming">Приходы</span>-->
<!--                    <span class="balance__bills-filter expenses-bills" data-type="expenses">Расходы</span>-->
<!--                    <span class="balance__bills-filter rod-bills" data-type="rod">Родословная</span>-->
<!--                    <span class="balance__bills-filter promotions-bills" data-type="promotions">Поощрения</span>-->
<!--                    <div class="bills-search">-->
<!--                        <input type="text" class="bills-search__input" placeholder="Поиск...">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="balance__bills-lines">-->
<!--                    <div class="balance__bills-line balance__bills-line--coming">-->
<!---->
<!--                            <div class="balance__bills-block balance__bills-block--number">-->
<!--                                <div class="balance__bills-title"># платежа</div>-->
<!--                                <div class="balance__bills-info">2525858</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block balance__bills-block--data">-->
<!--                                <div class="balance__bills-title">Дата и время</div>-->
<!--                                <div class="balance__bills-info">15.05.2020, 22:00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Отправитель</div>-->
<!--                                <a href="#" class="balance__bills-link">Николай</a>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Получатель</div>-->
<!--                                <div class="balance__bills-info">Я</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Назначения платежа</div>-->
<!--                                <a href="#" class="balance__bills-link">Поощрение</a>-->
<!--                            </div>-->
<!---->
<!--                        <div class="bills-line__right">-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Приход</div>-->
<!--                                <div class="balance__bills-summa--comming">5.00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Баланс</div>-->
<!--                                <div class="balance__bills-summa--total">91.00</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="balance__bills-line balance__bills-line--coming">-->
<!---->
<!--                            <div class="balance__bills-block balance__bills-block--number">-->
<!--                                <div class="balance__bills-title"># платежа</div>-->
<!--                                <div class="balance__bills-info">2525858</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block balance__bills-block--data">-->
<!--                                <div class="balance__bills-title">Дата и время</div>-->
<!--                                <div class="balance__bills-info">15.05.2020, 22:00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Отправитель</div>-->
<!--                                <div class="balance__bills-info">Игра "МИР"</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Получатель</div>-->
<!--                                <div class="balance__bills-info">Я</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Назначения платежа</div>-->
<!--                                <a href="#" class="balance__bills-link">Реферальная выплата 2-го уровня</a>-->
<!--                            </div>-->
<!---->
<!--                        <div class="bills-line__right">-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Приход</div>-->
<!--                                <div class="balance__bills-summa--comming">5.00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Баланс</div>-->
<!--                                <div class="balance__bills-summa--total">91.00</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="balance__bills-line balance__bills-line--expenses">-->
<!---->
<!--                            <div class="balance__bills-block  balance__bills-block--number">-->
<!--                                <div class="balance__bills-title"># платежа</div>-->
<!--                                <div class="balance__bills-info">2525858</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block balance__bills-block--data">-->
<!--                                <div class="balance__bills-title">Дата и время</div>-->
<!--                                <div class="balance__bills-info">15.05.2020, 22:00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Отправитель</div>-->
<!--                                <a href="#" class="balance__bills-link">Я</a>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Получатель</div>-->
<!--                                <a href="" class="balance__bills-link">Николай Петровский</a>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Назначения платежа</div>-->
<!--                                <div class="balance__bills-info">Подарок тебе на твой день рождения</div>-->
<!--                            </div>-->
<!---->
<!--                        <div class="bills-line__right">-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Расход</div>-->
<!--                                <div class="balance__bills-summa--expenses">5.00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Баланс</div>-->
<!--                                <div class="balance__bills-summa--total">91.00</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="balance__bills-line balance__bills-line--coming">-->
<!---->
<!--                            <div class="balance__bills-block balance__bills-block--number">-->
<!--                                <div class="balance__bills-title"># платежа</div>-->
<!--                                <div class="balance__bills-info">2525858</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block balance__bills-block--data">-->
<!--                                <div class="balance__bills-title">Дата и время</div>-->
<!--                                <div class="balance__bills-info">15.05.2020, 22:00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Отправитель</div>-->
<!--                                <a href="#" class="balance__bills-link">Николай</a>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Получатель</div>-->
<!--                                <div class="balance__bills-info">Я</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Назначения платежа</div>-->
<!--                                <div class="balance__bills-info">Не указано</div>-->
<!--                            </div>-->
<!---->
<!--                        <div class="bills-line__right">-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Приход</div>-->
<!--                                <div class="balance__bills-summa--comming">5.00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Баланс</div>-->
<!--                                <div class="balance__bills-summa--total">91.00</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="balance__bills-line balance__bills-line--coming">-->
<!---->
<!--                            <div class="balance__bills-block balance__bills-block--number">-->
<!--                                <div class="balance__bills-title"># платежа</div>-->
<!--                                <div class="balance__bills-info">2525858</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block balance__bills-block--data">-->
<!--                                <div class="balance__bills-title">Дата и время</div>-->
<!--                                <div class="balance__bills-info">15.05.2020, 22:00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Отправитель</div>-->
<!--                                <a href="#" class="balance__bills-link">Николай</a>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Получатель</div>-->
<!--                                <div class="balance__bills-info">Я</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Назначения платежа</div>-->
<!--                                <a href="#" class="balance__bills-link">Реферальная выплата 1-го уровня</a>-->
<!--                            </div>-->
<!---->
<!--                        <div class="bills-line__right">-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Приход</div>-->
<!--                                <div class="balance__bills-summa--comming">5.00</div>-->
<!--                            </div>-->
<!--                            <div class="balance__bills-block">-->
<!--                                <div class="balance__bills-title">Баланс</div>-->
<!--                                <div class="balance__bills-summa--total">91.00</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="balance__bills-bottom">-->
<!--                    <button class="btn btn--green balance__bill-more">Показать еще</button>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
    </div>
</div>

