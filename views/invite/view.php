<?php

use yii\widgets\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */

if($model){
$this->title = "Викистраница «{$model->name}»";
}else{
   $this->title = "Викистраница не найдена"; 
}
if($model){
?>

<div class="account-page" style="height: 100%;">
<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>
<?= $this->render('/layouts/header-new-menu.php'); ?>

<div class="account color_bg">
    <div class="account__header">
        <div class="container-fluid">
            <div class="account__header-inner">
                <div class="account__header-left">
                    <div class="account__header-block ">
                        <div class="hamburger-for-bluemenu">
                            <div class="hamburger hamburger1 ">
                                <span class="bar bar1"></span>
                                <span class="bar bar2"></span>
                                <span class="bar bar4"></span>
                            </div>
                        </div>
                        <a href="#" class="d-block">
                            <div class="account__photo">
                                <?php if($user_money->avatar): ?>
                                    <img src="/<?= $user_money->avatar?>" alt="">
                                <?php endif; ?>
                            </div>                            
                        </a>    
                        <div class="account__name">
                                <?php echo($user_money['name']);?> <br>
                                <span><?php echo($user_money['living_place']);?></span>
                            </div>                    
                    </div>
                </div>
                <div class="account__header-right">
                    <div class="account__header-block">
                        <div class="dropdown account__dropdown-block">
                            <button type="button" id="profileMenuButton" class="profile__menu-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="account__info-text">
                                    <div class="menu-dots">
                                    <span class="menu-dot dot1"></span>
                                    <span class="menu-dot dot2"></span>
                                    <span class="menu-dot dot3"></span>
                                </div></div>
                            </button>
                            <div class="account__dropdown-menu dropdown-menu" aria-labelledby="profileMenuButton">
                                <a href="#" class="account__dropdown-item account__dropdown-item--chat" title="Чат"><span>Чат</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item-lenta" title="Лента"><span>Лента</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--rod" title="Родословная"><span>Родословная</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--profile " title="Профиль"><span>Профиль</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--wiki active" title="Викистраница"><span>Викистраница</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--photo" title="Фото"><span>Фото</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--video" title="Видео"><span>Видео</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--blog" title="Блог"><span>Блог</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--conect" title="Связи"><span>Связи</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--community" title="Клубы"><span>Клубы</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--anketa" title="Анкета"><span>Анкета</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--rezume" title="Резюме"><span>Резюме</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--stor" title="Магазин"><span>Магазин</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--book" title="Книга отзывов"><span>Книга отзывов</span></a>
                                <a href="#" class="account__dropdown-item account__dropdown-item--office" title="Офис"><span>Офис</span></a>
                            </div>
                        </div>
                        <div class="dropdown dropdown_right">
                            <button type="button" id="profileEctionButton" class="profile__ection-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="menu-dots">
                                    <span class="menu-dot dot1"></span>
                                    <span class="menu-dot dot2"></span>
                                    <span class="menu-dot dot3"></span>
                                </div>
                            </button>
                            <div class="account__edition-dropdown dropdown-menu" aria-labelledby="profileEctionButton">
                                <a href="#" class="account__edition-item">Подружиться</a>
                                <a href="#" class="account__edition-item">Подписаться</a>
                                <a href="#" class="account__edition-item">Пожаловаться</a>
                                <a href="#" class="account__edition-item">Заблокировать</a>
                            </div>
                        </div>
                        <div class="hamburger-for-orangemenu">
                            <div class="hamburger hamburger1">
                                <span class="bar bar1"></span>
                                <span class="bar bar2"></span>
                                <span class="bar bar4"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="scroll-block mCustomScrollbar" data-mcs-theme="minimal-dark">
        <div class="account__inner">
            <div class="account__body">
                <div class="account__body-top">
                    <div class="container-fluid">
                        <div class="account__user-info">
                            <div class="account__user-photo">
                                 <?php if($user_money->avatar): ?>
                                    <img src="/<?= $user_money->avatar?>" alt="">
                                <?php endif; ?>
                            </div>
                            <div class="account__user-content">
                                <div class="account__user-title">Викистраница</div>
                                <div class="account__user-name"><?php echo($model['name']);?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wiki_page wiki_in_page">   
   
    

    <div class="body">
        <div class="left_block forms_block">
            <div class="form-group field-wiki-name" style="width: 1445.08px;">
             <label ><?php echo($model['name']);?></label>
            </div>          
            
        </div>
        <div class="right_block  forms_block">
            <?php if($model->photo): ?>
                <img src="/uploads/<?= $model->photo?>" alt="">
            <?php endif; ?>
            <?php

            if ($model['birth']) {
                ?>
               <div class="birth_wiki">
               <div>Рождение </div>
               <div><?php echo($model['birth']);?> <br>
                <?php if($model['city_birth'] != 'Город рождения'){?>
               <?php echo($model['city_birth']);?>,
               <?};
                    if($model['country_birth'] != 'Страна рождения'){
               ?>
               <?php echo($model['country_birth']);?> 
               <?}?></div> 
            </div>
            <?
            }                        
            

if ($model['father'] != 'Отец') {
                ?>
               <div class="wiki-father" >
                <div>Отец</div>
             <div> <?php echo($model['father']);?></div>                
            </div>
            <?
            }
                        if ($model['mother'] != 'Мать') {
                ?>
               <div class="wiki-mother" >
                <div>Мать</div>
              <div><?php echo($model['mother']);?></div>                
            </div>
            <?
            }
                        if ($model['wife'] != 'Жена') {
                ?>
                <div class="wiki-wife" >   
                   <div> Супруга</div>
              <div><?php echo($model['wife']);?></div>             
            </div>
            <?
            }
                        if ($model['children'] != 'Дети') {
                ?>
               <div class="wiki-children" > 
                <div>Дети</div>
              <div><?php echo($model['children']);?>    </div>           
            </div>
            <?
            }
            if ($model['religion'] != 'Вероисповедание') {
                ?>
               <div class="wiki-religion" >  
               <div> Вероисповедание</div>
              <div><?php echo($model['religion']);?> </div>             
            </div> 
            <?
            }
            ?>                       
        </div>
        <div class="left_block body_left__block forms_block">
         <?php 
         if($model['description']){
         
            ?>         
            <div class="wiki-description" >  
            <?php echo($model['description']);?>               
            </div>
            <?
            }
            if($model['purpose']){
         
            ?>
         <h2>Жизненное кредо, миссия, цели, мечты</h2>
            <div class="wiki-purpose" >  
            <?php echo($model['purpose']);?>                
            </div>
            <?
            }
            if($model['childhood']){
         
            ?>
         <h2>Детство, юность</h2>
            <div class="wiki-childhood" >  
            <?php echo($model['childhood']);?>               
            </div>
            <?
            }
            if($model['education']){
         
            ?>
          <h2>Образование</h2>
            <div class="wiki-education" > 
            <?php echo($model['education']);?>                   
            </div>
            <?
            }
            if($model['work']){
         
            ?>
         <h2>Трудовая деятельность</h2>
            <div class="wiki-work" >   
            <?php echo($model['work']);?>                
            </div>
            <?
            }
            if($model['today']){
         
            ?>
         <h2>Жизнь в настоящее время</h2>
            <div class="wiki-today" >   
            <?php echo($model['today']);?>              
            </div>
            <?
            }
            if($model['charity']){
         
            ?>
         <h2>Общественная деятельность, благотворительность</h2>
            <div class="wiki-charity" > 
            <?php echo($model['charity']);?>                   
            </div>
            <?
            }
            if($model['interests']){
         
            ?>
          <h2>Интересы</h2>
            <div class="wiki-interests" >  
            <?php echo($model['interests']);?>                  
            </div>
            <?
            }
            if($model['hobby']){
         
            ?>
          <h2>Хобби, увлечения</h2>
            <div class="wiki-hobby" > 
            <?php echo($model['hobby']);?>                    
            </div>
            <?
            }
            if($model['language']){
         
            ?>
         <h2>Иностранные языки</h2>
            <div class="wiki-language" > 
            <?php echo($model['language']);?>                  
            </div>
            <?
            }
            if($model['dignities']){
         
            ?>
            <h2>Достоинства и недостатки</h2>
            <div class="wiki-dignities" >      
            <?php echo($model['dignities']);?>            
            </div>
            <?
            }
            if($model['family']){
         
            ?>
         <h2>Семья</h2>
            <div class="wiki-family" >  
             <?php echo($model['family']);?>                
            </div>
            <?
            }
            if($model['pets']){
         
            ?>
          <h2>Домашние питомцы</h2>
            <div class="wiki-pets" >    
             <?php echo($model['pets']);?>               
            </div>
            <?
            }
            if($model['idols']){
         
            ?>
          <h2>Кумиры</h2>
            <div class="wiki-idols" >     
            <?php echo($model['idols']);?>             
            </div>
            <?
            }
            if($model['joke']){
         
            ?>
           <h2>Любимый анекдот</h2>
            <div class="wiki-joke" > 
            <?php echo($model['joke']);?>                 
            </div>
            <?
            }
            if($model['cars']){
         
            ?>
            <h2>Любимые автомобили</h2>
            <div class="wiki-cars" > 
            <?php echo($model['cars']);?>                   
            </div>
            <?
            }
            
            if($model['brand']){
         
            ?>
           <h2>Любимые марки одежды, обуви, аксессуаров</h2>
            <div class="wiki-brand" >                
                <?php echo($model['brand']);?>  
            </div>
            <?
            }
            
            if($model['food']){
         
            ?>
           <h2>Любимая еда, напитки</h2>
            <div class="wiki-food" >      
             <?php echo($model['food']);?>            
            </div>
            <?
            }
            
            if($model['countries']){
         
            ?>
          <h2>Любимые страны, города, места</h2>
            <div class="wiki-countries" >  
            <?php echo($model['countries']);?>                
            </div>
            <?
            }
            
            if($model['sport']){
         
            ?>
           <h2>Любимые виды спорта</h2>
            <div class="wiki-sport" >    
            <?php echo($model['sport']);?>              
            </div>
            <?
            }
            
            if($model['book']){         
            ?>
           <h2>Любимые книги, авторы</h2>
            <div class="wiki-book" >   
            <?php echo($model['book']);?>                
            </div>
            <?
            }
            
            if($model['music']){         
            ?>
          <h2>Любимая музыка, фильмы, сериалы</h2>
            <div class="wiki-music" >   
            <?php echo($model['music']);?>                
            </div>
            <?
            }
            
            if($model['recommend']){         
            ?>
            <h2>Я рекомендую, советую</h2>
            <div class="wiki-recommend" >  
            <?php echo($model['recommend']);?>              
            </div>  
            <?
            }
            

         ?>      
        </div>
        
    </div>
    
    <div class="stats_block">
        <hr>
        <img src="/img/views.png" alt=""> <?php echo($model['views']);?> <br>
        <img src="/img/likes.png" alt=""> 0 <br>
        <img src="/img/moneys.png" alt=""> <?php echo($user_money['balance_usd']);?> <br>
    </div>
  
</div>
</div>
            </div>
            
            
        </div>
    </div>
</div>





<?
}else{
    ?>Пользователь не создал ещё вики страницу<?
}

?>


<script>
    $(document).ready(function(){
    $('#wiki_page').addClass('active_img_new');
    $('#wiki_page img').attr('src', '/img/orange_menu/6_1.png');
    $('#header').remove();
    $('body').removeClass('news_bg');
    $('.d-none').remove();
    setTimeout(function(){
        $('.main').removeClass('news_bg');
    }, 100); 

    $('.right_block label').text('');
    $('.right_block label').append( $('.right_block>img') );
    

    });
</script>

<script>
    $(document).ready(function(){
        if($(window).width() < 631){
    $('.account__user-content').css('width', $('.account__user-info').width() - 80)
}else{
    $('.account__user-content').css('width', '100%')
}
        if($(window).width() > 825){
            let tmp = -20;
            $('.left_block>div').each(function() {   
                tmp+=$(this).height();
                tmp+= 28;
                if(tmp <= $('.right_block').height()){
                   $(this).css('max-width', $('.wiki_page').width() - 310); 
                   
                }
            });       
        }else{
            $('.left_block>div').each(function() { 
                $(this).css('max-width', '100%');
            });
        }

    $(window).resize(function() {
if($(window).width() < 631){
    $('.account__user-content').css('width', $('.account__user-info').width() - 80)
}else{
    $('.account__user-content').css('width', '100%')
}
        if($(window).width() > 825){
            let tmp = 0;
            $('.left_block>div').each(function() {   
                tmp+=$(this).height();
                tmp+= 20;
                if(tmp <= $('.right_block').height()){
                   $(this).css('max-width', $('.wiki_page').width() - 310); 
                   
                }
            });       
        }else{
            $('.left_block>div').each(function() { 
                $(this).css('max-width', '100%');
            });
        }
});
});
</script>




<script>
    $(document).ready(function() {
        
        let tmp2=false;
        let tmp3=false;
        $('.hamburger-for-bluemenu').on('click', function() {
            //alert('ok');            
            if (tmp2 == false) {
                tmp2 = true;
                $('.left_block>div').each(function() {   
                if( $('.wiki_page').css('max-width') != '100%'){
                    let tmp = parseInt($('.left_block>div').css('max-width')) -2;
                    $('.left_block>div').css('max-width', tmp);
                }
            });
            }else{
 tmp2 = false;
                $('.left_block>div').each(function() {   
                if( $('.wiki_page').css('max-width') != '100%'){
                    let tmp = parseInt($('.left_block>div').css('max-width')) +2;
                    $('.left_block>div').css('max-width', tmp);
                }
            });
            }
            
        });
        
        $('.hamburger-for-orangemenu').on('click', function() {
           
            if (tmp3 == false) {
                tmp3 = true;
                $('.left_block>div').each(function() {   
                if( $('.wiki_page').css('max-width') != '100%'){
                    let tmp = parseInt($('.left_block>div').css('max-width')) -2;
                    $('.left_block>div').css('max-width', tmp);
                }
            });
            }else{
 tmp3 = false;
                $('.left_block>div').each(function() {   
                if( $('.wiki_page').css('max-width') != '100%'){
                    let tmp = parseInt($('.left_block>div').css('max-width')) +2;
                    $('.left_block>div').css('max-width', tmp);
                }
            });
            }
        });
    });
</script>