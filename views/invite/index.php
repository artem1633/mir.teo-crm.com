<?php

use app\helpers\AvatarResolver;
use yii\bootstrap\Modal;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Пригласить';
?>
<script type="text/javascript" src="https://vk.com/js/api/share.js?93" charset="windows-1251"></script>

<?= $this->render('/layouts/blue_menu.php'); ?>
<?= $this->render('/layouts/orange_menu.php'); ?>

<div class="profile main color_bg invite_page">
    <div class="main_main mCustomScrollbar" data-mcs-theme="minimal-dark">
<div class="wrapper">
    <header class="header">
  <div class="header__body">
    <div class="header__item">
      <a href="#" class="header__link-money">Пригласить и заработать</a>
      <a href="https://mirumir24.ru/refprog" class="header__link-details">Подробности</a>
    </div>
    <div class="header__item">
       <?=
                            Html::a('<span> Скопировать рефссылку </span>', '#', [
                                'class' => 'header__reflivk-copie',
                                'data-ref-url' => Yii::$app->user->identity->getSelfRefHref(),
                                'onclick' => 'copyToClipboard($(this).data("ref-url")); alert("Ссылка скопирована в буфер обмена")'
                            ]);
                            ?>      
      <a  class="header__link-admin">Админка</a>
    </div>
  </div>
</header>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<div class="hiden_block" style='display: none;'>
  
  <?= $form->field($model, 'img')->fileInput() ?>  
  <?= $form->field($model, 'user_id')->textInput(); ?>
  <?= $form->field($model, 'banner')->textInput(); ?>
</div>

    <main class="main">
      <section class="invitation">
        <h1 class="invitation__title">ПРИГЛАСИТЬ ЧЕРЕЗ СОЦСЕТИ И МЕССЕНДЖЕРЫ</h1>
        <form action="" class="invitation__form">
          <div class="invitation__item-one item-one">
            <h2 class="item-one__title">1. Прикрепить графику (не обязательно, но желательно)</h2>
            <div class="item-one__body">
              <div class="item-one__column item-one__column_banner">
               <label for="invite-banner"><a href='#' class="item-one__banner"><span>Прикрепить баннер</span></a></label>
                <div class="item-one__area-banner">
                  
                   <label for="invite-banner"> <img src="" alt=""></label>
                   <               
                </div>

                <a href="#" class="item-one__del-banner"><span>Удалить</span></a>
              </div>
              <div class="item-one__column item-one__column_img">
                <label for="invite-img"><a class="item-one__image"><span>Прикрепить картинку</span></a></label>
                <div class="item-one__area-image">
                  <?php if($model->img){ ?>
                   <label for="invite-img"> <img src="/uploads/<?= $model->img?>" alt=""></label>
                   <?php }else{ ?>   
                   <label for="invite-img"> <img src="" alt=""></label>
                   <?php } ?> 
                </div>
                <a href="#" class="item-one__del-image"><span>Удалить</span></a>
              </div>
            </div>
          </div>
          <div class="invitation__item-two item-two">
            <h2 class="item-two__ttile">2. Написать сообщение (не обязательно, но желательно)</h2>
              <?= $form
              ->field($model, 'title')
              ->label(false)
              ->textarea(['class' =>'item-two__textarea input']); ?>
          </div>
           <div class="invitation__item-two item-two">
            <h2 class="item-two__ttile">3. Сохраните изменения</h2>
            <div class="item-three__body">
               <input type="submit" class="btn btn--green profile__save-setting" value="Сохранить изменения">
            </div>
             </div>
          <div class="invitation__item-three item-three">
            <h2 class="item-three__title">4. Отправить (выбрать иконку)</h2>
            <div class="item-three__body">
<script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="https://yastatic.net/share2/share.js"></script>
<div class="ya-share2" data-services="vkontakte,odnoklassniki,facebook,twitter,linkedin,whatsapp,telegram,viber,skype"  data-url='https://mirumir24.ru/welcome?ref_id=<?=$model->user_id?>' data-title='' data-image="" ></div>
               
            </div>
          </div>


        </form>
      </section>
    </main> 

     <?php ActiveForm::end(); ?>   
  </div>
  

  <div class="banner_popup">
  <div class="wrapper">
    <div class="banner">
      <div class="banner__links">
        <div class="banner__column banner__column_left">
          <a href="#" class="banner__category-link"><span>Все категории</span></a>
          <?php if($user_money->role != '3'){ ?><a  class="banner__add-link"><span>Добавить</span></a><?php }?>
        </div>
        
        <div class="banner__column">
          <a href="#" class="banner__close-link"><picture><source srcset="img/icons/close.webp" type="image/webp"><img src="img/icons/close.png" alt=""></picture></a>
        </div>
      </div>
       <?php if($user_money->role != '3'){ ?><div class="banner__add-img">
          <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
          <div class="banner__add-hidden">
            <?= $form->field($models_new, 'banner')->fileInput() ?>  
          </div>
          <div class="banner__add-img_iner">
            <label for="banner-banner"> <img src="/" alt=""></label>
          </div>          
          <div class="banner__add-button">
            <?= Html::submitButton('Загрузить баннер', ['class' => 'btn btn--green profile__save-btn']); ?>
          </div>
           <?php ActiveForm::end(); ?>   
        </div><?php }?>
      <div class="banner__content">
        <?php foreach ($models as $modeling): ?>
          <?php if($modeling){ ?>
          <div id='img_banner_<?=$modeling['id']?>' class="banner__item">

            <a class="banner__image"><picture><img src="/uploads/banner/<?=$modeling['banner']?>" alt=""></picture></a>
           <?php if($user_money->role != '3'){ ?> <a href="#" id="img_del_<?=$modeling['id']?>" class="banner__del-image">Удалить</a><?php }?>
          <?php }?>
        </div>         
        <?php endforeach; ?>
      </div>
    </div>
  </div>

  </div>


</div>
</div>


<script>
    $(document).ready(function(){
        function readURLS(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.banner__add-img_iner label img').attr('src', e.target.result);            

        };
        $('.banner__add-img .banner__add-img_iner label').css('width', 'inherit')
        $('.banner__add-img .banner__add-img_iner label').css('height', 'inherit')
        $('.banner__add-img .banner__add-img_iner label').css('background', 'transparent')
        $('#mCSB_1_container > .wrapper').css('height', $('.banner_popup').height());
        reader.readAsDataURL(input.files[0]);
    }
}


$("#banner-banner").change(function(){
    readURLS(this);
});




    });
</script>

<script>
 $(document).ready(function(){ 
   $('.banner__add-link').on('click', function(e) {
        $('.banner__add-img').toggleClass('banner__add-img_open');
        $('#mCSB_1_container > .wrapper').css('height', $('.banner_popup').height());
    });
  });
</script>

<script>
  

$('.green_menu').addClass('active_img_new');
$('.green_menu').removeClass('green_menu');

$('.item-one__banner').on('click', function(e) {
  if(!$(".item-one__column_banner").hasClass("lock")){
    $('.banner_popup').css('display', 'block');    
    $('#mCSB_1_container > .wrapper').css('height', $('.banner_popup').height());
} 
});
$('.item-one__area-banner').on('click', function(e) {
  if(!$(".item-one__column_banner").hasClass("lock")){
  $('.banner_popup').css('display', 'block');  
  $('#mCSB_1_container > .wrapper').css('height', $('.banner_popup').height());
  } 
});
$('.banner__close-link').on('click', function(e) {
  $('.banner_popup').css('display', 'none');  
  $('#mCSB_1_container > .wrapper').css('height', 'inherit');
});
</script>



<script>
  $(document).ready(function(){

    let titles = $('.field-invite-title textarea').val(); 
      $('.ya-share2').attr('data-title', titles);         

  if($('.item-one__area-image label img').attr('src') != ''){
      let img = $('.item-one__area-image img').attr('src'); 
              let tmp2 = window.location.protocol;
              tmp2+= '//';
              tmp2+= window.location.host;
              img = tmp2 + img;
            $('.ya-share2').attr('data-image', img);
            
  }else{
    let img = $('.item-one__area-banner label img').attr('src'); 
              let tmp2 = window.location.protocol;
              tmp2+= '//';
              tmp2+= window.location.host;
              img = tmp2 +'/' + img;
            $('.ya-share2').attr('data-image', img);
  }

});
</script>

<script>



   $('.banner .banner__del-image').on('click', function(){
    let tmp = $(this).attr('id');
    tmp = tmp.replace('img_del_', '')
    let tmp2 ='#img_banner_'  + tmp;
      $.ajax({
        url:   '/invite/test',
        data:  {id   : tmp},
        type: 'POST',
        success:function(res){
          console.log(res);
          $(tmp2).remove();
        },
        error: function(res){
          alert("error");
        }
      })



  });
</script>
 
  


<script>
    $(document).ready(function(){
        function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.item-one__area-image label img').attr('src', e.target.result);            

        };

        $('.item-one__column_banner').addClass('lock');
        $('.item-one__column_banner label').attr('for', '');

        reader.readAsDataURL(input.files[0]);
    }
}

$("#invite-img").change(function(){
    readURL(this);
});


$('.item-one__del-image').on('click', function(e) {
  $('.item-one__area-image label img').attr('src', ''); 
    $('#invite-user_id').val('0');  
    $('.item-one__column_banner').removeClass('lock');
    $('.item-one__column_banner label').attr('for', 'invite-img')

});

$('.item-one__del-banner').on('click', function(e) {   
  $('.item-one__area-banner label img').attr('src', '');
  $('#invite-banner').val('');
  $('.item-one__column_img').removeClass('lock');
   $('.item-one__column_img label').attr('for', 'invite-img')
});


    });
</script>

<script>
    
    if($('#invite-banner').val() != ''){
      let tmp = 'uploads/banner/' + $('#invite-banner').val();
      $('.item-one__area-banner label img').attr('src', tmp); 
    }

    if($('.item-one__area-image label img').attr('src') != ''){
        $('.item-one__column_banner').addClass('lock');
        $('.item-one__column_banner label').attr('for', '');
    }
     if($('.item-one__area-banner label img').attr('src') != ''){
        $('.item-one__column_img').addClass('lock');
        $('.item-one__column_img label').attr('for', '');
    }
    
</script>
<script>
   $(document).ready(function(){ 
$('.banner_popup .banner__content img').on('click', function(e) {
 let tmp = $(this).attr('src');
 $('.banner_popup').css('display', 'none');  
  $('#mCSB_1_container > .wrapper').css('height', 'inherit');
  $('.item-one__area-banner label img').attr('src', tmp); 
  
  tmp = tmp.replace('uploads/banner/', '')
 
  $('#invite-banner').val(tmp);
   $('.item-one__column_img').addClass('lock');
    $('.item-one__column_img label').attr('for', '');
});
   }); 
    
</script>