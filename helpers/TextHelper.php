<?php

namespace app\helpers;

/**
 * Class TextHelper
 * @package app\helpers
 */
class TextHelper
{
    /**
     * @param $str
     * @return mixed
     */
    public static function parseLinks($str)
    {
        $output = $str;
        $output = preg_replace('/\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/i', '<a href="$1" target="_blank">$1</a>', $output);
//        $output = preg_replace('/(([a-z0-9+_-]+)(.[a-z0-9+_-]+)*@([a-z0-9-?=_]+.)+[a-z]{2,6})/', '<a href="mailto:$1" target="_blank">$1</a>', $output);



//        $pos1 = strpos($str, 'http');

//        if(strpos($str, 'http') !== false){
//            $a = strpos($str, 'http');
//            $str2 = substr($str, $a);
//            $a2 = strpos($str2, ' ');
//
//            if($a2 == false){
//
//            }
//
//            $str3 = substr($str2, 0, $a2);
//
//            var_dump($str3);
//        }

//        return $output;


        if(strstr($str, '.mp3') > 0){
            $output = '<video controls="" autoplay="" name="media"><source src="'.$str.'" type="audio/mpeg"></video>';
        }

        return $output;
    }
}