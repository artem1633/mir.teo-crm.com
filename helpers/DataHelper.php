<?php

namespace app\helpers;

/**
 * Class DataHelper
 * @package app\helpers
 */
class DataHelper
{
    /**
     * @return mixed
     */
    public static function getCountries()
    {
        $arr = require('../data/country.php');

        return $arr;
    }
}