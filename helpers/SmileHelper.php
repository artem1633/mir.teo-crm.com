<?php

namespace app\helpers;


class SmileHelper
{
    private $smiles;

    function __construct()
    {
        $this->smiles = $this->setSmiles();
    }

    private function setSmiles()
    {
        $path = 'img/smiles';
        $smile_files = array_diff(scandir($path), ['.', '..']);

        \Yii::info($smile_files, 'test');

        $smiles = [];

        foreach ($smile_files as $file){
            $smiles['|' . $file . '|'] = '/' . $path . '/' . $file;
        }

        return $smiles;
    }

    public function getSmiles()
    {
        return $this->smiles;
    }

    /**
     * Получает имя файла картинки по тексту
     * @param $smile_text
     * @return string|null
     */
    public function toImg($smile_text)
    {
        $smile_text = str_replace('|', '', $smile_text);
        if (array_key_exists($smile_text, $this->smiles)){
            return $this->smiles[$smile_text];
        }
        return null;
    }

    /**
     * Получает текстовое обозначение для картинки по имени файла
     * @param $img
     * @return false|int|string
     */
    public function toText($img)
    {
        return array_search($img, $this->smiles);
    }

}