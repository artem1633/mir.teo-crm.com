<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_visits}}`.
 */
class m200715_210031_create_user_visits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_visits}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'counter' => $this->integer()->comment('Количество просмотров'),
        ]);

        $this->createIndex(
            'idx-user_visits-user_id',
            'user_visits',
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_visits-user_id',
            'user_visits',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-user_visits-user_idd',
            'user_visits'
        );

        $this->dropForeignKey(
            'fk-user_visits-user_id',
            'user_visits'
        );

        $this->dropTable('{{%user_visits}}');
    }
}
