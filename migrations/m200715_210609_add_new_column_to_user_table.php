<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m200715_210609_add_new_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'marital_status', $this->string()->defaultValue(false)->comment('Семейное положение'));
        $this->addColumn('user', 'link_vk', $this->string()->defaultValue(false)->comment('Ссылка вк'));
        $this->addColumn('user', 'link_odnok', $this->string()->defaultValue(false)->comment('Ссылка одноклассники'));
        $this->addColumn('user', 'link_facebook', $this->string()->defaultValue(false)->comment('Ссылка фейсбук'));
        $this->addColumn('user', 'link_instagram', $this->string()->defaultValue(false)->comment('Ссылка инст'));
        $this->addColumn('user', 'link_twitter', $this->string()->defaultValue(false)->comment('Ссылка твитер'));
        $this->addColumn('user', 'link_youtube', $this->string()->defaultValue(false)->comment('Ссылка ютуб'));
        $this->addColumn('user', 'code_state', $this->string()->defaultValue(false)->comment('Код статуса в государстве'));
        $this->addColumn('user', 'position_state', $this->string()->defaultValue(false)->comment('Должность в государстве МИР'));
        $this->addColumn('user', 'code_service', $this->string()->defaultValue(false)->comment('Код Госслужбы'));
        $this->addColumn('user', 'code_vip', $this->string()->defaultValue(false)->comment('Код випп клуба'));
        $this->addColumn('user', 'totalistic_dignity', $this->string()->defaultValue(false)->comment('Должность (должности) в Тоталистической Партии'));
        $this->addColumn('user', 'code_tpm', $this->string()->defaultValue(false)->comment('Код ТПМ'));
        $this->addColumn('user', 'code_dsm', $this->string()->defaultValue(false)->comment('Код ДСМ'));
        $this->addColumn('user', 'code_epm', $this->string()->defaultValue(false)->comment('Код ЕПМ'));
        $this->addColumn('user', 'code_ppm', $this->string()->defaultValue(false)->comment('Код ППМ'));
        $this->addColumn('user', 'gerb', $this->string()->defaultValue(false)->comment('Герб лого'));
        $this->addColumn('user', 'is_simple', $this->string()->defaultValue(false)->comment('Членство в клубе Simple'));
        $this->addColumn('user', 'aristocrat_dignity1', $this->string()->defaultValue(false)->comment('Аристократический титул'));
        $this->addColumn('user', 'code_sity', $this->string()->defaultValue(false)->comment('Владение франшизой MIR-City'));
        $this->addColumn('user', 'code_matrix', $this->string()->defaultValue(false)->comment('Владение лицензией MIR-Matrix'));
        $this->addColumn('user', 'totem', $this->string()->defaultValue(false)->comment('Тотем'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'marital_status');
        $this->dropColumn('user', 'link_vk');
        $this->dropColumn('user', 'link_odnok');
        $this->dropColumn('user', 'link_facebook');
        $this->dropColumn('user', 'link_instagram');
        $this->dropColumn('user', 'link_twitter');
        $this->dropColumn('user', 'link_youtube');
        $this->dropColumn('user', 'code_state');
        $this->dropColumn('user', 'position_state');
        $this->dropColumn('user', 'code_service');
        $this->dropColumn('user', 'code_vip');
        $this->dropColumn('user', 'totalistic_dignity');
        $this->dropColumn('user', 'code_tpm');
        $this->dropColumn('user', 'code_dsm');
        $this->dropColumn('user', 'code_epm');
        $this->dropColumn('user', 'code_ppm');
        $this->dropColumn('user', 'gerb');
        $this->dropColumn('user', 'is_simple');
        $this->dropColumn('user', 'aristocrat_dignity1');
        $this->dropColumn('user', 'code_sity');
        $this->dropColumn('user', 'code_matrix');
        $this->dropColumn('user', 'totem');



    }
}
