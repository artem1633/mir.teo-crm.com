<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m200327_135346_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('news');
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->comment('Заголовок'),
            'english_title' => $this->string()->comment('Транслитерация'),
            'content' => $this->binary()->comment('Содержимое'),
            'date_published' => $this->integer()->comment('Дата публикации'),
            'status_published' => $this->integer()->defaultValue(0)->comment('Статус публикации'),
            'main_news' => $this->integer()->defaultValue(0)->comment('Главная новость?'),
            'count_news' => $this->integer()->defaultValue(0)->comment('Кол-во просмотров')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
