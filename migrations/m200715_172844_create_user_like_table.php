<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_like}}`.
 */
class m200715_172844_create_user_like_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_like}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь, который лайкает'),
            'user_profile_id' => $this->integer()->comment('Пользователь, которого лайкают'),
        ]);

        $this->createIndex(
            'idx-user_like-user_id',
            'user_like',
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_like-user_id',
            'user_like',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-user_like-user_profile_id',
            'user_like',
            'user_profile_id'
        );

        $this->addForeignKey(
            'fk-user_like-user_profile_id',
            'user_like',
            'user_profile_id',
            'user',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user_like-user_id',
            'user_like'
        );

        $this->dropIndex(
            'idx-news_like-user_id',
            'user_like'
        );

        $this->dropForeignKey(
            'fk-user_like-user_profile_id',
            'user_like'
        );

        $this->dropIndex(
            'idx-user_like-user_profile_id',
            'user_like'
        );
        $this->dropTable('{{%user_like}}');
    }
}
