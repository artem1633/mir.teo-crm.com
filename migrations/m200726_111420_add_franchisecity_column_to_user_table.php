<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m200726_111420_add_franchisecity_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'franchise_city', $this->string()->after('code_sity')->comment('Город франшизы'));
        $this->addColumn('{{%user}}', 'totem_code', $this->string()->comment('Код тотема'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'franchise_city');
         $this->dropColumn('{{%user}}', 'totem_code');
    }
}
