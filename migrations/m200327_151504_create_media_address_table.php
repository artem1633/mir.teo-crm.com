<?php

use yii\db\Migration;

/**
 * Handles the creation of table `media_address`.
 */
class m200327_151504_create_media_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('media_address', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->comment('ID новости'),
            'link' => $this->string()->comment('Ссылка на ресурс'),
            'type' => $this->string()->comment('Тип ресурса')
        ]);

        $this->addForeignKey('fk_mews_id', 'media_address', 'news_id', 'news', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_mews_id', 'media_address');

        $this->dropTable('media_address');
    }
}
