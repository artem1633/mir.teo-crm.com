<?php

use yii\db\Migration;

/**
 * Class m200326_153431_set_email_approved_1_to_admin
 */
class m200326_153431_set_email_approved_1_to_admin extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update('user', ['email_approved' => 1], ['login' => 'admin']);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m200326_153431_set_email_approved_1_to_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200326_153431_set_email_approved_1_to_admin cannot be reverted.\n";

        return false;
    }
    */
}
