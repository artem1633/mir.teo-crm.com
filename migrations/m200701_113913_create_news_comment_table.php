<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_comment`.
 */
class m200701_113913_create_news_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_comment', [
            'id' => $this->primaryKey(),
            'new_id' => $this->integer()->comment('Новоти'),
            'content' => $this->text()->comment('Текст'),
            'author_id' => $this->integer()->comment('Автор'),
            'created_at' => $this->dateTime()->comment('Дата и время'),
        ]);

        $this->createIndex(
            'idx-news_comment-new_id',
            'news_comment',
            'new_id'
        );

        $this->addForeignKey(
            'fk-news_comment-new_id',
            'news_comment',
            'new_id',
            'news',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-news_comment-author_id',
            'news_comment',
            'author_id'
        );

        $this->addForeignKey(
            'fk-news_comment-author_id',
            'news_comment',
            'author_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-news_comment-author_id',
            'news_comment'
        );

        $this->dropIndex(
            'idx-news_comment-author_id',
            'news_comment'
        );

        $this->dropForeignKey(
            'fk-news_comment-new_id',
            'news_comment'
        );

        $this->dropIndex(
            'idx-news_comment-author_id',
            'news_comment'
        );

        $this->dropTable('news_comment');
    }
}
