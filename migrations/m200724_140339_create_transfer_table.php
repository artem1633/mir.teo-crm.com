<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transfer}}`.
 */
class m200724_140339_create_transfer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transfer}}', [
            'id' => $this->primaryKey(),
            'currency_id' => $this->integer()->comment('Id валюты'),
            'from_user_id' => $this->integer()->defaultValue(null)->comment('Отправитель'),
            'for_user_id' => $this->integer()->defaultValue(null)->comment('Получатель'),
            'payment_number' => $this->integer()->unique()->comment('Номер платежа'),
            'purpose_of_payment' => $this->text()->defaultValue(null)->comment('Назначение платежа'),
            'amount' => $this->double()->comment('Сумма'),
            'user_level' => $this->integer()->defaultValue(null)->comment('Уровень тому, кому начислено'),
            'type' => $this->tinyInteger()->comment('Тип платежа'),
            'date' => $this->dateTime()->comment('Дата создания')
        ]);

        $this->createIndex(
            'idx-transfer-currency_id',
            'transfer',
            'currency_id'
        );

        $this->addForeignKey(
            'fk-transfer-currency_id',
            'transfer',
            'currency_id',
            'currency',
            'id'
        );

        $this->createIndex(
            'idx-transfer-from_user_id',
            'transfer',
            'from_user_id'
        );

        $this->addForeignKey(
            'fk-transfer-from_user_id',
            'transfer',
            'from_user_id',
            'user',
            'id'
        );

        $this->createIndex(
            'idx-transfer-for_user_id',
            'transfer',
            'for_user_id'
        );

        $this->addForeignKey(
            'fk-transfer-for_user_id',
            'transfer',
            'for_user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-transfer-currency_id',
            'transfer'
        );

        $this->dropIndex(
            'idx-transfer-currency_id',
            'transfer'
        );

        $this->dropForeignKey(
            'fk-transfer-from_user_id',
            'transfer'
        );

        $this->dropIndex(
            'idx-transfer-from_user_id',
            'transfer'
        );

        $this->dropForeignKey(
            'fk-transfer-for_user_id',
            'transfer'
        );

        $this->dropIndex(
            'idx-transfer-for_user_id',
            'transfer'
        );

        $this->dropTable('{{%transfer}}');
    }
}
