<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_like`.
 */
class m200701_122847_create_news_like_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_like', [
            'id' => $this->primaryKey(),
            'new_id' => $this->integer()->comment('Новости'),
            'user_id' => $this->integer()->comment('Пользователь'),
        ]);

        $this->createIndex(
            'idx-news_like-new_id',
            'news_like',
            'new_id'
        );

        $this->addForeignKey(
            'fk-news_like-new_id',
            'news_like',
            'new_id',
            'news',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-news_like-user_id',
            'news_like',
            'user_id'
        );

        $this->addForeignKey(
            'fk-news_like-user_id',
            'news_like',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-news_like-user_id',
            'news_like'
        );

        $this->dropIndex(
            'idx-news_like-user_id',
            'news_like'
        );

        $this->dropForeignKey(
            'fk-news_like-new_id',
            'news_like'
        );

        $this->dropIndex(
            'idx-news_like-new_id',
            'news_like'
        );

        $this->dropTable('news_like');
    }
}
