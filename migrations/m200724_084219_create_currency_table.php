<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%currency}}`.
 */
class m200724_084219_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'slug' => $this->string()->comment('Слаг'),
            'course' => $this->double()->defaultValue(0)->comment('Курс')
        ]);

        $this->insert('currency', [
            'name' => 'Rollar',
            'slug' => 'rollar'
        ]);

        $this->insert('currency', [
            'name' => 'Douro',
            'slug' => 'douro'
        ]);

        $this->insert('currency', [
            'name' => 'Onetoken',
            'slug' => 'onetoken'
        ]);

        $this->insert('currency', [
            'name' => 'Oneanti',
            'slug' => 'oneanti'
        ]);

        $this->insert('currency', [
            'name' => 'Trill',
            'slug' => 'trill'
        ]);

        $this->insert('currency', [
            'name' => 'Trillanti',
            'slug' => 'trillanti'
        ]);

        $this->insert('currency', [
            'name' => 'Anva',
            'slug' => 'anva'
        ]);

        $this->insert('currency', [
            'name' => 'Anvanti',
            'slug' => 'anvanti'
        ]);

        $this->insert('currency', [
            'name' => 'USD',
            'slug' => 'usd'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
