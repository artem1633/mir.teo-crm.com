<?php

use yii\db\Migration;

/**
 * Handles adding is_express to table `news`.
 */
class m200630_123107_add_is_express_column_to_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('news', 'is_express', $this->boolean()->defaultValue(false)->comment('Срочная'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('news', 'is_express');
    }
}
