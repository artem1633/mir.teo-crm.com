<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rubric`.
 */
class m200327_134518_create_rubric_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rubric', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->defaultValue('')->comment('Наименование рубрики')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rubric');
    }
}
