<?php

use yii\db\Migration;

/**
 * Handles adding is_important to table `news`.
 */
class m200630_130417_add_is_important_column_to_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('news', 'is_important', $this->boolean()->defaultValue(false)->comment('Важная'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('news', 'is_important');
    }
}
