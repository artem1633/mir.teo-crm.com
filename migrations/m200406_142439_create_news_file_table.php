<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_file`.
 */
class m200406_142439_create_news_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_file', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->comment('Пост'),
            'dir' => $this->string()->comment('Папка'),
            'file_name' => $this->string()->comment('Наименование'),
            'original_file_name' => $this->string()->comment('Файл'),
            'type' => $this->integer()->comment('Тип'),
            'url' => $this->string()->comment('Путь'),
        ]);

        $this->createIndex(
            'idx-news_file-news_id',
            'news_file',
            'news_id'
        );

        $this->addForeignKey(
            'fk-news_file-news_id',
            'news_file',
            'news_id',
            'news',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-news_file-news_id',
            'news_file'
        );

        $this->dropIndex(
            'idx-news_file-news_id',
            'news_file'
        );

        $this->dropTable('news_file');
    }
}
