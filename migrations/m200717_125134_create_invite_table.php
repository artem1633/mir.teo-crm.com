<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%invite}}`.
 */
class m200717_125134_create_invite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%invite}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->string()->comment('Сообщение'),
            'title' => $this->string()->comment('Сообщение'),
            'banner' => $this->string()->comment('Баннер'),
            'img' => $this->string()->comment('Картинка'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%invite}}');
    }
}
