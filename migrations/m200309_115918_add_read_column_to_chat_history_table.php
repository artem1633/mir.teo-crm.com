<?php

use yii\db\Migration;

/**
 * Handles adding read to table `chat_history`.
 */
class m200309_115918_add_read_column_to_chat_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('chat_history', 'read', $this->smallInteger(1)->defaultValue(0)->comment('Прочитано собеседником'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('chat_history', 'read');
    }
}
