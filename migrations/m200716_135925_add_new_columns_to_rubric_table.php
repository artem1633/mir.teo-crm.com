<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%rubric}}`.
 */
class m200716_135925_add_new_columns_to_rubric_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rubric', 'slug', $this->string()->comment('Слаг'));
        $this->addColumn('rubric', 'type', $this->tinyInteger()->comment('Тип'));

        $this->insert('rubric', [
            'name' => 'Последние публикации',
            'slug' => 'poslednie-publikacii',
            'type' => 1
        ]);

        $this->insert('rubric', [
            'name' => 'Самые срочные',
            'slug' => 'samye-srocnye',
            'type' => 1
        ]);

        $this->insert('rubric', [
            'name' => 'Самые популярные',
            'slug' => 'samye-popularnye',
            'type' => 1
        ]);

        $this->insert('rubric', [
            'name' => 'Самые важные',
            'slug' => 'samye-vaznye',
            'type' => 1
        ]);

        $this->insert('rubric', [
            'name' => 'Миру - МИР',
            'slug' => 'miru-mir',
            'type' => 1
        ]);

        $this->insert('rubric', [
            'name' => 'Страны, регионы, города',
            'slug' => 'strany-regiony-goroda',
            'type' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rubric', 'slug');
        $this->dropColumn('rubric', 'type');
    }
}
