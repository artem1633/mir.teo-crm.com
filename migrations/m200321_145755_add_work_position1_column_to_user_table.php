<?php

use yii\db\Migration;

/**
 * Handles adding work_position1 to table `user`.
 */
class m200321_145755_add_work_position1_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'work_position1', $this->string()->comment('Должность 2'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'work_position1');
    }
}
