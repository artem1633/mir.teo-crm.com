<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170428_140722_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->comment('Логин'),
            'name' => $this->string()->comment('ФИО'),
            'avatar' => $this->string()->comment('Аватар'),
            'birth_date' => $this->date()->comment('дата рождения'),
            'country' =>  $this->string()->comment('страна проживания'),
            'living_place' =>  $this->string()->comment('место проживания'),
            'education' =>  $this->string()->comment('образование'),
            'profession' =>  $this->string()->comment('профессия'),
            'work_place' =>  $this->string()->comment('место работы'),
            'work_position' =>  $this->string()->comment('должность'),
            'work_status' =>  $this->string()->comment('статус работы'),
            'phone' =>  $this->string()->comment('телефон'),
            'email' =>  $this->string()->comment('почта'),
            'interests' =>  $this->string()->comment('интересы'),
            'hobby' =>  $this->string()->comment('хобби'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'role' => $this->integer()->comment('Роль'),
            'balance_usd' => $this->float()->unsigned()->defaultValue(0)->comment('Баланс (Доллар США)'),
            'ref_id' => $this->integer()->comment('Кто пригласил'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
            'last_active_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'points' => $this->integer()->comment('баллы'),
            'created_at' => $this->dateTime(),
        ]);

        $this->insert('user', [
            'login' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'role' => 1,
            'is_deletable' => false,
        ]);

        $this->createIndex(
            'idx-user-ref_id',
            'user',
            'ref_id'
        );

        $this->addForeignKey(
            'fk-user-ref_id',
            'user',
            'ref_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user-ref_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-ref_id',
            'user'
        );

        $this->dropTable('user');
    }
}
