<?php

use yii\db\Migration;

/**
 * Handles adding new to table `user`.
 */
class m200506_132958_add_new_columns_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'messager_whats_app', $this->boolean()->defaultValue(false)->comment('WhatsApp'));
        $this->addColumn('user', 'messager_viber', $this->boolean()->defaultValue(false)->comment('Viber'));
        $this->addColumn('user', 'messager_telegram', $this->boolean()->defaultValue(false)->comment('Telegram'));
        $this->addColumn('user', 'messager_none', $this->boolean()->defaultValue(false)->comment('Отсутствуют'));
        $this->addColumn('user', 'interest_aristocratic', $this->boolean()->defaultValue(false)->comment('Аристократические титулы'));
        $this->addColumn('user', 'interest_services', $this->boolean()->defaultValue(false)->comment('Поставки товаров и услуг'));
        $this->addColumn('user', 'interest_part', $this->boolean()->defaultValue(false)->comment('Членство в профсоюзе'));
        $this->addColumn('user', 'interest_intercom', $this->boolean()->defaultValue(false)->comment('Работа в интеркомах'));
        $this->addColumn('user', 'interest_help', $this->boolean()->defaultValue(false)->comment('Помогите определиться'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'messager_whats_app');
        $this->dropColumn('user', 'messager_viber');
        $this->dropColumn('user', 'messager_telegram');
        $this->dropColumn('user', 'messager_none');
        $this->dropColumn('user', 'interest_aristocratic');
        $this->dropColumn('user', 'interest_services');
        $this->dropColumn('user', 'interest_part');
        $this->dropColumn('user', 'interest_intercom');
        $this->dropColumn('user', 'interest_help');
    }
}
