<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rubric_news`.
 */
class m200327_151118_create_rubric_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rubric_news', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->comment('ID новости'),
            'rubric_id' => $this->integer()->comment('ID рубрики')
        ]);

        $this->addForeignKey('fk_news_id', 'rubric_news', 'news_id', 'news', 'id');

        $this->addForeignKey('fk_rubric_id', 'rubric_news', 'rubric_id', 'rubric', 'id');

        $this->createIndex('rubric_news_idx', 'rubric_news', ['news_id', 'rubric_id']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('rubric_news_idx', 'rubric_news');

        $this->dropForeignKey('fk_rubric_id', 'rubric_news');

        $this->dropForeignKey('fk_news_id','rubric_news');

        $this->dropTable('rubric_news');
    }
}
