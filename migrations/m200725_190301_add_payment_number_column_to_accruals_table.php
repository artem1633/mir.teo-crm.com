<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%accruals}}`.
 */
class m200725_190301_add_payment_number_column_to_accruals_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('accruals', 'payment_number', $this->integer()->after('date')->comment('Номер платежа'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('accruals', 'payment_number');
    }
}
