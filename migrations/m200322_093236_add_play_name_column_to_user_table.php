<?php

use yii\db\Migration;

/**
 * Handles adding play_name to table `user`.
 */
class m200322_093236_add_play_name_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'play_name', $this->string()->comment('Игровое имя'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'play_name');
    }
}
