<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'app\bootstrap\AppBootstrap'],
    'defaultRoute' => 'dashboard',
    'timeZone' => 'Europe/Moscow',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@blog'  => '@app/modules/blog',
        '@admin-blog'  => '@app/modules/admin',
    ],
    'name' => 'CRM',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'GhAcZ2j2hHCv9-XMRK1mi0wYRu29SWwu',
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => ['lifetime' => 7 * 24 *60 * 60*255],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [
                '/' => 'site/index',
				'rod' => 'dashboard/index',
                'account' => 'profile/balance',
                'news/usa' => 'usa/index',
                'news/am' => 'am/index',
                'news/by' => 'by/index',
                'news/az' => 'az/index',
                'news/ge' => 'ge/index',
                'news/kaz' => 'kaz/index',
                'news/kg' => 'kg/index',
                'news/lv' => 'lv/index',
                'news/lt' => 'lt/index',
                'news/md' => 'md/index',                
                'news/tj' => 'tj/index',
                'news/tm' => 'tm/index',
                'news/uz' => 'uz/index',
                'news/ua' => 'ua/index',
                'news/ee' => 'ee/index',
                'news/cn' => 'cn/index',
                'news/fr' => 'fr/index',
                'news/de' => 'de/index',
                'news/in' => 'in/index',
                'news/it' => 'it/index',
                'news/es' => 'es/index',
                'news/uk' => 'uk/index',
                'blog' => 'blog/news/index',
                'blog/admin' => 'admin/news/index',
                'blog/admin/create' => 'admin/news/create',
                'blog/admin/moderation' => 'admin/news/moderation',
                'blog/admin/rubric' => 'admin/rubric/index',
                'blog/admin/editors' => 'admin/news/editors',
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru',
                'username' => 'mir.notify.service@mail.ru',
                'password' => 'L0z:9m0v1',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'settings' => [
            'class' => 'app\components\Settings'
        ],
//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            // send all mails to a file by default. You have to set
//            // 'useFileTransport' to false and configure a transport
//            // for the mailer to send real emails.
//            'useFileTransport' => false,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.mail.ru',
//                'username' => 'money.notification.service@mail.ru',
//                'password' => '23;Mkh10mN089',
//                'port' => '465',
//                'encryption' => 'ssl',
//            ],
//        ],
        'formatter' => [
            'timeZone' => 'Europe/Moscow',
            'dateFormat' => 'dd.mm.yyyy',
            'timeFormat' => 'php: H:i',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => '₽',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
//            'useFileTransport' => true,
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'api' => [
            'class' => 'app\modules\api\Api',
        ],
        'blog' => [
            'class' => 'app\modules\blog\Blog',
            'layout' => '@blog/views/layouts/main'
        ],
        'admin' => [
            'class' => 'app\modules\admin\AdminBlog',
            'layout' => '@admin-blog/views/layouts/main'
        ],
    ],
    'language' => 'ru-RU',
    'params' => $params,
];

if (YII_ENV_DEV) {
//     configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//         uncomment the following to add your IP if you are not connecting from localhost.
//        'allowedIPs' => ['*'],
//    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => ['My' => '@app/vendor/yiisoft/yii2-gii/generators/crud/admincolor']
            ]
        ],
        'allowedIPs' => ['*'],
    ];
}

return $config;
