<?php

namespace app\commands;

use app\models\Accruals;
use yii\console\Controller;

class AccrualController extends Controller{

    public function actionIndex(){

        if($accruals = Accruals::find()->all()){
            foreach($accruals as $accrual){
                /* @var Accruals $accrual */
                if(!$accrual->payment_number){
                    $accrual->setPaymentNumber();
                    $accrual->save();
                }
            }
        }
    }
}