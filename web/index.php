<?php

require(__DIR__ . '/../vendor/autoload.php');
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();



defined('YII_DEBUG') or define('YII_DEBUG', ($_ENV['YII_DEBUG'] == 'true') ? true : false);
defined('YII_ENV') or define('YII_ENV', $_ENV['YII_ENV'] ?: 'prod');

if (YII_DEBUG) {
    error_reporting(-1);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
}

require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
