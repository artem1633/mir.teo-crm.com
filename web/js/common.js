$('#btn-dropdown_header').click(function(){
    if($(".dropdown-menu").is(':visible')){
        $(".dropdown-menu ").hide();
    } else {
        $(".dropdown-menu ").show();
    }
});


function copyToClipboard(str)
{
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}



$(document).ready(function(){
    layout.init();
});

$(document).ready(function(){
    $('#moneytransactionform-profile_url').on('input', function(e){
        let profile_card = $('#profile-card');
        if(profile_card.hasClass('d-none')){
            profile_card.removeClass('d-none');
        }
        $.ajax({
            headers: {
                Accept : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
            },
            url: $(e.target).data('url'),
            data: {url: e.target.value},
            type: 'POST',
            success: function(response){
                profile_card.html(response);
            },
            error: function(xhr, str){
                console.error(str);
            }
        });
    });
    $('#moneytransactionform-purpose_of_payment').on('focus', function(e){
        $('#profile-card').addClass('d-none');
    });
});

$(document).ready(function(){
    $('.article-container').on('click', function(e){
        if(e.target.hasAttribute('id')){
            if(e.target.id == 'delete-comment'){
                $.ajax({
                    headers: {
                        Accept : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
                    },
                    url: $(e.target).data('url'),
                    type: 'POST',
                    success: function(response){
                        e.target.closest('.article-reviews__block').remove();
                    },
                    error:  function(xhr, str){
                        console.error(str);
                    }
                });
            }else if(e.target.id == 'update-comment'){
                let content_block = e.target.closest('.article-reviews__action').previousElementSibling;
                let reviews_block = e.target.closest('.article-reviews__block');
                if(e.target.hasAttribute('update')){
                    $.ajax({
                        headers: {
                            Accept : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
                        },
                        url: $(e.target).data('url'),
                        data: {comment: content_block.firstElementChild.value},
                        type: 'POST',
                        success: function(response){
                            e.target.removeAttribute('update');
                            e.target.innerHTML = 'Редактировать';
                            reviews_block.classList.remove('nopublic');
                            reviews_block.classList.add('public');
                            content_block.innerHTML = '<div class="article-reviews__text">' + response + '</div>';
                        },
                        error:  function(xhr, str){
                            console.error(str);
                        }
                    });
                }else{
                    e.target.innerHTML = 'Опубликовать';
                    e.target.setAttribute('update', true);
                    if(reviews_block.classList.contains('public')){
                        reviews_block.classList.remove('public');
                        reviews_block.classList.add('nopublic');
                        let comment = content_block.firstElementChild.innerHTML;
                        content_block.innerHTML = '<textarea class="leave-comment__textarea">' + comment + '</textarea>';
                    }
                }
            }
        }
    });
});

$(document).ready(function(){
    $('.blog-last__like').on('click', function(e){
        e.preventDefault();
        let view_page = $('.blog-last__like').data('view');
        if(view_page !== undefined){
            $.ajax({
                headers: {
                    Accept : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
                },
                url: $(this)[0].href,
                type: 'POST',
                data: {id: view_page},
                success: function(response){
                    console.log('success');
                    e.target.text = response;
                },
                error:  function(xhr, str){
                    console.error(str);
                }
            });
        }
    });
});

$(document).ready(function() {
    // $('.blog-last__see-all').on('click', function(e) {
    //     e.preventDefault();
    //     $(this).parents('.blog-last').find('.blog-last__blocks').addClass('show-all');
    // })

    // $('.alfabet-post__btn-remove').on('click', function() {
    //     $('.before-remove__info').remove();
    //     let before_remove = $('<div class="before-remove__info"><div class="before-remove__info-inner">\
    //             В этой рубрике есть публикации.  Для ее удаления, переместите их или удалите.</div> \
    //             </div>')
    //
    //     $(this).parents('.alfabet-post__block').append(before_remove);
    // })
    $(".main_main").mCustomScrollbar({
    theme:"minimal-dark"
    //theme: "minimal-dark",
});

})


$(document).mouseup(function (e) {
    var container = $(".before-remove__info");
    if (container.has(e.target).length === 0){
        container.hide();
    }
});