"use strict";

let preloader;

let Layout = function () {

    let self = this;

    this.init = function() {
        self.lazyFunc();
        self.exists();
        self.ieStubFunc();
        self.safariClass();
        self.sidePanel();
        self.mobileMenu();
    };

    this.lazyFunc = function(){
        var lazyloadImages;

        if ("IntersectionObserver" in window) {
            lazyloadImages = document.querySelectorAll(".lazyClass");
            var imageObserver = new IntersectionObserver(function(entries, observer) {
                entries.forEach(function(entry) {
                    if (entry.isIntersecting) {
                        var image = entry.target;
                        image.src = image.dataset.src;
                        image.classList.remove("lazyClass");
                        if (image.classList.contains('lazyBg')) {
                            image.classList.remove("lazyBg");
                            image.setAttribute('style', 'background-image: url(' + image.src + ')');
                        }
                        image.classList.add("visible");
                        imageObserver.unobserve(image);
                    }
                });
            });

            lazyloadImages.forEach(function(image) {
                imageObserver.observe(image);
            });
        } else {
            var lazyloadThrottleTimeout;
            lazyloadImages = document.querySelectorAll(".lazyClass");

            function lazyload () {
                if(lazyloadThrottleTimeout) {
                    clearTimeout(lazyloadThrottleTimeout);
                }

                lazyloadThrottleTimeout = setTimeout(function() {
                    var scrollTop = window.pageYOffset;
                    lazyloadImages.forEach(function(img) {
                        if(img.offsetTop < (window.innerHeight + scrollTop)) {
                            img.src = img.dataset.src;
                            img.classList.remove('lazyClass');
                            if (img.classList.contains('lazyBg')) {
                                img.classList.remove("lazyBg");
                                img.setAttribute('style', 'background-image: url(' + image.src + ')');
                            }
                            img.classList.add("visible");
                        }
                    });
                    if(lazyloadImages.length === 0) {
                        document.removeEventListener("scroll", lazyload);
                        window.removeEventListener("resize", lazyload);
                        window.removeEventListener("orientationChange", lazyload);
                    }
                }, 20);
            }

            document.addEventListener("scroll", lazyload);
            window.addEventListener("resize", lazyload);
            window.addEventListener("orientationChange", lazyload);
        }
    };

    this.ieStubFunc = function(){
        let isIE = /*@cc_on!@*/false || !!document.documentMode;
        let isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./);
        if (isIE11 || isIE)
        {
            document.body.classList.add("ie");
            let ieStub = "<div class=\"ie-detect\" style=\"display\: none;\"><b>Ваш браузер устарел</b><p>Ви пользуетесь устаревшым браузером, который не поддерживает современные веб-стандарты и представляет угрозу Вашей безопасности.</p><p>Обновите или скачайте совресенную версию браузера.</p><p>Internet Explorer не поддерживается.</p></div>";
            document.querySelector("body").classList.add("ie");
            document.querySelector(".body-grid").innerHTML = ieStub;
        };

        var ua = window.navigator.userAgent;
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
    };

    this.safariClass = function() {
        let is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
        if (is_safari) {
            document.querySelector("body").classList.add("safari");
        }
    };

    this.mobileMenu = function () {
        if(document.querySelector('.hamburger')) {
            let hamburger = document.querySelector('.hamburger'),
                menu = document.querySelector('.header-center > ul.mobile'),
                link = document.querySelectorAll('.header-center > ul a');

            hamburger.addEventListener("click", function(){
                menu.classList.toggle("opened");
            });

            function closeMenu(){
                menu.classList.remove("opened");
            }
            for(let i=0; i<link.length; i++) {
                link[i].addEventListener("click", function(){
                    closeMenu();
                });
            }
        }
    };

    this.sidePanel = function () {
        // let sideArrow = document.querySelector('.side-arrow');
        // let bottomArrowTop = document.querySelector('.bottom-arrow--open');
        // let bottomArrowDown = document.querySelector('.bottom-arrow--close');
        // let rightSide = document.querySelector('.content-right');
        // let centerContent = document.querySelector('.content-center');
        //
        // if (document.querySelector('.content-right.opened-full')) {
        //     centerContent.classList.add('with-aside');
        //     rightSide.classList.remove('opened-full');
        // } else {
        //     centerContent.classList.remove('with-aside');
        // }
        //
        // sideArrow.addEventListener('click', function () {
        //     if (window.outerWidth > 1279) {
        //         rightSide.classList.toggle('opened-full');
        //         centerContent.classList.toggle('with-aside');
        //         // window.addEventListener("resize", lazyload);
        //     } else {
        //         // rightSide.classList.remove('opened-middle');
        //         rightSide.classList.add('opened-middle');
        //     }
        // });
        // bottomArrowTop.addEventListener('click', function () {
        //     rightSide.classList.remove('opened-middle');
        //     rightSide.classList.add('opened-full');
        //     centerContent.classList.add('with-aside');
        // });
        // bottomArrowDown.addEventListener('click', function () {
        //     if ( document.querySelector('.content-right.opened-full') ) {
        //         rightSide.classList.remove('opened-full');
        //         rightSide.classList.add('opened-middle');
        //     } else if ( document.querySelector('.content-right.opened-middle') ) {
        //         rightSide.classList.remove('opened-full');
        //         rightSide.classList.remove('opened-middle');
        //     }
        //     centerContent.classList.remove('with-aside');
        // });
    };

    this.exists = function (selector) {
        return (document.querySelectorAll(selector).length > 0);
    };
};

let layout = new Layout();

document.addEventListener("DOMContentLoaded", function(){

         layout.init();

});
$(document).ready(function(){
    layout.init();
});



/*NEW*/

   
   $('.owl-carousel').owlCarousel({
    loop:false, 
    margin: 1,
    nav: true,
    dots: false,
    navText: ['<span class="carousel_prev"></span>','<span class="carousel_prev"></span>'],
    autoplay: false,
    responsiveClass:true,
    responsive:{
        0:{
            items:6            
        },
        355:{
            items:7     
        },
        409:{
            items:8     
        },
        463:{
            items:9     
        },
        517:{
            items:10     
        },
        571:{
            items:11     
        },
         599:{
            items:12     
        },
         625:{
            items:16      
        },
        680:{
            items:17      
        },
        728:{
            items:18        
        },
        742:{
            items:18        
        },
        751:{
            items:20      
        }, 
        769:{
            items:19      
        },         
    },
});


    $(document).ready(function(){
     $(window).on('resize', function(){
      if($(window).width() <= 769) {
      $('.menu_carousel').addClass('owl-carousel');
      $('.menu_carousel').addClass('owl-theme');
      }
       else {
      $('.menu_carousel').removeClass('owl-carousel');
      $('.menu_carousel').removeClass('owl-theme');
      }
      }).trigger('resize');
    });

     $(document).ready(function(){
     $(window).on('resize', function(){
        var h = 0;
        if($(window).width() > 769 ) {   
        $('.main_main').attr('style', 'height: 100vh!important');
      } 
      if($(window).width() <= 769 && $(window).width() > 599) {

        h = $(window).height();
        h = h - 40;
        h = h / $(window).height();
        h = h*100;
        h = 'height:' + h + "vh!important";
        $('.main_main').attr('style', h);
      }   
        if($(window).width() <= 599) {
        h = $(window).height();
        h = h - 60;
        h = h / $(window).height();
        h = h*100;
        h = 'height:' + h + "vh!important";
        $('.main_main').attr('style', h);
      }   
      }).trigger('resize');
    });
    
    
    $(document).ready(function(){
        $(window).on('resize', function(){
      if($(window).height() <= 740 && $(window).width() >= 769) {
        var h = $(window).height();
        h = h - 22;
        $( ".blue_menu .menu_und .menu_carousel" ).css( "height", h );
        $( ".blue_menu .menu_und .menu_carousel" ).css( "display", "flex");
        $( ".blue_menu .menu_und .menu_carousel" ).css( "justify-content", "center");
     
      }
       else { 

        if($(window).width() <= 769 ) {
            if ($(window).width() >= 599 ) {
                $( ".blue_menu .menu_und .menu_carousel" ).css( "height", "40") 
            }
            if ($(window).width() < 599 ) {
                $( ".blue_menu .menu_und .menu_carousel" ).css( "height", "50") 
            }
            
        }else{
            $( ".blue_menu .menu_und .menu_carousel" ).css( "height", "100vh")  
        }
      }
      }).trigger('resize');
    });

    $(document).ready(function(){
        $(window).on('resize', function(){
      if($(window).height() <= 740 && $(window).width() >= 769) {
        var h = $(window).height();
        h = h - 35;
        $( ".orange_menu .menu_und .menu_carousel" ).css( "height", h )
        $( ".orange_menu .menu_und .menu_carousel" ).css( "display", "flex");
        $( ".orange_menu .menu_und .menu_carousel" ).css( "justify-content", "center");
     
      }
       else { 
            
        if($(window).width() <= 769 ) {

            if ($(window).width() >= 599  ) {                
                $( ".orange_menu .menu_und .menu_carousel" ).css( "height", "40") 
            }
            if ($(window).width() < 599  ) {               
                $( ".orange_menu .menu_und .menu_carousel" ).css( "height", "50")
            }            
        }else{
            $( ".orange_menu .menu_und .menu_carousel" ).css( "height", "100vh")  
        }
      }
      }).trigger('resize');
    });

    $('a.btn_menu_desc').on('click', function(){
    var btn_napr = $(this).attr('href');
    if (btn_napr == "#blue_menu_top") {
    var posit = $(".blue_menu .menu_und .menu_carousel").scrollTop();
    posit = posit - 35;

    $(".blue_menu .menu_und .menu_carousel").animate(
                { scrollTop: posit }, 200);     
    }
    if (btn_napr == "#orange_menu_top") {
    var posit = $(".blue_menu .menu_und .menu_carousel").scrollTop();
    posit = posit - 35;

    $(".orange_menu .menu_und .menu_carousel").animate(
                { scrollTop: posit }, 200);     
    }
    if (btn_napr == "#blue_menu_bottom") {
    var posit = $(".blue_menu .menu_und .menu_carousel").scrollTop();
    posit = posit + 35;

    $(".blue_menu .menu_und .menu_carousel").animate(
                { scrollTop: posit }, 200);
    }
    if (btn_napr == "#orange_menu_bottom") {
    var posit = $(".orange_menu .menu_und .menu_carousel").scrollTop();
    posit = posit + 35;

    $(".orange_menu .menu_und .menu_carousel").animate(
                { scrollTop: posit }, 200);
    }

});



  
     /*$(document).ready(function(){
        if(document.location.pathname != '/' && document.location.pathname != '/dashboard/index' && document.location.pathname != '/dashboard/' && document.location.pathname != '/dashboard/parent' && document.location.pathname != '/dashboard/lider'  && document.location.pathname != '/dashboard/loser'){
            $(".main").addClass("main_frame");
            $(".main").addClass("news_bg");        
        }
        
        if($('.shop').hasClass('rod')) {
            $(".main").addClass("main_main");
            $(".main").removeClass("main_frame");   
        }
        

        if($('.site-index').hasClass('index-page')) {
            $('body').addClass('home-page');
            $('body').find('.blue_menu').remove();
            $('body').find('.orange_menu').remove();
        }
     });*/

    /*if($('.site-index').hasClass('index-page')) {
        $('body').addClass('home-index');
    }*/

    $(document).ready(function(){
        /*if(document.location.pathname == '/' || document.location.pathname == '/dashboard/' || document.location.pathname == '/dashboard/index' || document.location.pathname == '/dashboard/parent' || document.location.pathname == '/dashboard/lider' || document.location.pathname == '/dashboard/loser'){
            $(".main").addClass("main_main");  
            $("body").removeClass("news_bg");  
            $(".main").removeClass("news_bg");        
        }*/
        newsShow();
        
        $(window).resize(function () {
            newsShow();
        });
        
        $('.news-block__parent').hover(function() {
            let height = $(this).find('.news-block__item').height();
            
            console.log(height);
        })
        
     });
     
     function newsShow() {
         if ($(window).width() < 1025) {
            $('.news-block__parent').on('click', function() {
                 $(this).toggleClass('opened');
            });
         }
     }

