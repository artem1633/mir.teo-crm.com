<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       'css/invite.css',
        'libs/flatpickr-master/dist/flatpickr.min.css',
        'https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/css/suggestions.min.css',
        'css/leaders-page.css',
        //'/bootstrap/bootstrap.min.css',
        '/css/owl.theme.green.css',
        '/css/owl.carousel.css',
        '/libs/jquery.mCustomScrollbar.min.css',
        '/css/style_new.css',
        '/css/wiki.css',
        'css/main.css',
        'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap',
    ];
    public $js = [
//        'libs/flatpickr-master/dist/flatpickr.min.js',
//        'libs/flatpickr-master/dist/I10n/ru.js',
//        'libs/cleave/cleave.min.js',
//        'https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/js/jquery.suggestions.min.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js',
//      'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js',//   отключил (конфликтует с yii.js)
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js',
        'js/owl.carousel.js',
        'libs/jquery.mCustomScrollbar.concat.min.js',
        'js/layout.js',
        'js/main-page.js',
        'js/common.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
