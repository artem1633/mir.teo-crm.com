<?php

namespace app\modules\api\controllers;

use app\models\Faq;
use app\models\MobileUser;
use app\models\Transaction;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class TransactionController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $models = Transaction::find()->where(['mobile_user_id' => $this->user->id])->all();

        return $models;
    }


    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Transaction();
        $model->mobile_user_id = $this->user->id;

        if($model->load($request->get()) && $model->save())
        {
            return $model;
        } else {
            return $model->errors;
        }
    }

    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $token = null;

        if($request->isPost){
            $token = isset($_POST['token']) ? $_POST['token'] : null;
        } else if ($request->isGet){
            $token = isset($_GET['token']) ? $_GET['token'] : null;
        }

        if($token){
            $this->user = MobileUser::find()->where(['token' => $token])->one();
        }

        if($this->user == null){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

}