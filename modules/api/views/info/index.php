Получение ЧАВО<br>
api/faq/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
[{"id","subject","content","views_count","created_at"}, [...], ...]<br>
<br>
Получение новостей<br>
api/news/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
[{"id","subject","content","views_count","created_at"}, [...], ...]<br>
<br>
Получение уведомлений<br>
api/notification/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
[{"id","subject","content","views_count","created_at"}, [...], ...]<br>
<br>
Регистрация пользователя<br>
api/user/register<br>
[<span style="color: red">POST</span>]<br>
-login* (string)<br>
-password* (string)<br>
-email* (string)<br>
ОТДАЕТ: токен для авторизации (string)<br>
<br>
Получение токена<br>
api/user/get-token<br>
[<span style="color: red">POST</span>]<br>
-login* (string)<br>
-password* (string)<br>
ОТДАЕТ: токен для авторизации (string)<br>
<br>
Получение сообщений тикета<br>
api/ticket/get-token<br>
[<span style="color: green">GET</span>]<br>
-postId* (int) ID тикета<br>
ОТДАЕТ:<br>
[{"id","ticket_id","text","from","created_at"}, [...], ...]<br>
<br>
Отправить сообщение<br>
api/ticket/send-message<br>
[<span style="color: red">POST</span>]<br>
-postId* (int) ID тикета<br>
-text* (string)<br>
ОТДАЕТ:<br>
true|false<br>
<br>
Получить транзакции<br>
api/transaction/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
[{"id","mobile_user_id","wallet_from_id","wallet_to","type","amount","commission","status","comment","created_at"}, [...], ...]<br>
<br>
Создать транзакцию<br>
api/transaction/create<br>
[<span style="color: red">POST</span>]<br>
int $mobile_user_id Пользователь<br>
int $wallet_from_id Кошелек отправитель<br>
string $wallet_to Кошелек получателя<br>
string $type Тип операции<br>
double $amount Сумма<br>
double $commission Комиссия<br>
string $status Статус<br>
string $comment Комментарий<br>
ОТДАЕТ:<br>
[{"id","mobile_user_id","wallet_from_id","wallet_to","type","amount","commission","status","comment","created_at"}, [...], ...]<br>