<?php

namespace app\modules\admin;

use yii\base\Module;

class AdminBlog extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}