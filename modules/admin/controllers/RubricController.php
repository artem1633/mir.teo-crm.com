<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\forms\RubricForm;
use Yii;
use app\modules\blog\models\Rubric;
use app\modules\admin\models\RubricSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * RubricController implements the CRUD actions for Rubric model.
 */
class RubricController extends Controller{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->role == 1 || Yii::$app->user->identity->role == 2;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rubric models.
     * @return mixed
     */
    public function actionIndex(){

        $searchModel = new RubricSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $Model = new RubricForm();
        if($Model->load(Yii::$app->request->post())){
            if($Model->validate()){
                $Model->saveData();
            }else{
                Yii::error(print_r($Model->getErrors(), true));
            }
            return $this->refresh();
        }
        return $this->render('index', compact('searchModel', 'dataProvider', 'Model'));
    }

    /**
     * Displays a single Rubric model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rubric model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rubric();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Rubric model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Rubric model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id){

        $Rubric = Rubric::find()->where(['rubric.id' => $id, 'rubric.type' => Rubric::TYPE_ALPHABETICAL])->one();
        if($Rubric->rubricNews){
            Yii::$app->session->setFlash('rubric_id', $Rubric->id);
        }else{
            $Rubric->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Rubric model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rubric the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rubric::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
