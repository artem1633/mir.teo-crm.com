<?php

namespace app\modules\admin\controllers;

use app\models\NewsFile;
use app\models\User;
use app\modules\admin\models\forms\EditorForm;
use app\modules\admin\models\RubricNews;
use app\modules\admin\models\RubricSearch;
use app\modules\blog\models\MediaAddress;
use Yii;
use app\modules\admin\models\News;
use app\modules\admin\models\NewsSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use app\modules\blog\models\Rubric;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->role == 1 || Yii::$app->user->identity->role == 2;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete-new' => ['POST'],
                    'delete-editor' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($action->id == 'upload-file') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex($limit = 5){

        $News = (new NewsSearch())->search(Yii::$app->request->queryParams, $limit);
        $news_count = (new NewsSearch())->search(Yii::$app->request->queryParams, $limit, true);
        $mainNew = News::findOneMain();
        return $this->render('index', compact('News', 'news_count', 'mainNew', 'limit'));
    }

    /**
     * @return string
     */
    public function actionUploadFile($responseType = null)
    {
        $file = UploadedFile::getInstanceByName('upload');

        if ($file)
        {
            $file_model = new NewsFile;

            if ($file_model->upload($file) && $file_model->save()){
                Yii::$app->response->format = Response::FORMAT_JSON;
//                return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("1", "'.$file_model->url.'", "");</script>';
//                return '<script type="text/javascript">copyToClipboard("'.$file_model->getUrl().'"); alert("Ссылка на загруженный файл скопирована в буфер обмена. Вставьте её в поле «Ссылка» во вкладке «Данные об изображении»");</script>';
//                return ['error' => [
//                    'message' => '123',
//                ]];
                return ['url' => 'http://mir.loc'.$file_model->getUrl()];
            }
            else {
                return "Возникла ошибка при загрузке файла\n";
            }
        }
        else
            return "Файл не загружен\n";
    }

    public function actionModeration($limit = 5){

        $searchModel = new NewsSearch();
        $News = $searchModel->search(Yii::$app->request->queryParams, $limit, false, true);
        $news_count = (new NewsSearch())->search(Yii::$app->request->queryParams, $limit, true, true);
        return $this->render('moderate', compact('searchModel', 'News', 'news_count', 'limit'));
    }

    public function actionDeleteMedia($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = MediaAddress::findOne($id);

        if($model){
            $model->delete();
        }

        return ['forceClose' => true, 'forceReload'=>'#pjax-media-container'];
    }

    /**
     * Displays a single News model.
     * @param string $title
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($title)
    {
        $model = News::find()->where(['english_title' => $title])->one();

        if($model == null){
            throw new NotFoundHttpException();
        }

        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('date_published desc');
        $dataProvider->query->andWhere(['!=', 'main_news', 1]);
        $dataProvider->query->andWhere(['status_published' => 1]);
        $dataProvider->query->andWhere(['!=', 'id', $model->id]);
        $dataProvider->query->limit(9);
        $dataProvider->pagination = false;

        $model->updateCountView();

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){

        $model = new News();
        $model->setScenario(News::SCENARIO_CREATE);
        $rubric = new Rubric();
        $rubricItems = ArrayHelper::map(Rubric::find()->where(['not', ['id' => [1, 2, 3, 4]]])->all(), 'id', 'name');
        $rubricNews = new RubricNews();
        $pick_out_rubrics = ArrayHelper::map(Rubric::findAll(['id' => [2, 4]]), 'id', 'name');
        if ($model->load(Yii::$app->request->post()) && $model->save()){
            if(!$model->status_published){
                return $this->redirect(['moderation']);
            }else{
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', compact('model', 'rubric', 'rubricItems', 'pick_out_rubrics'));
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $rubric = new Rubric();
        $rubricItems = RubricSearch::getAllRubric();
        $rubricNews = new RubricNews();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'title' => $model->english_title]);
        }

        return $this->render('update', [
            'model' => $model,
            'rubric' => $rubric,
            'rubricItems' => $rubricItems
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPublish($id)
    {
        $model = $this->findModel($id);

        $model->status_published = 1;
        $model->date_published = time();

        $model->save(false);

        return $this->redirect(['index']);
    }

    /**
     * Страница "Редакторы"
     *
     * @return string|Response
     */
    public function actionEditors(){

        $Editors = User::find()->where(['role' => [User::ROLE_ADMIN, User::ROLE_EDITOR]])->orderBy(['role' => SORT_ASC, 'name' => SORT_ASC])->all();
        $Model = new EditorForm();
        if($Model->load(Yii::$app->request->post())){
            if($Model->validate()){
                $Model->saveData();
            }else{
                Yii::error(print_r($Model->getErrors(), true));
            }
            return $this->redirect(['editors']);
        }
        return $this->render('editors', compact('Editors', 'Model'));
    }

    /**
     * Удаление редактора (меняем статус на пользователя)
     *
     * @param integer $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteEditor($id){

        if(!$User = User::findOne(['id' => $id])){
            throw new NotFoundHttpException('Страница не найдена');
        }
        $User->role = User::ROLE_USER;
        $User->save(false);
        return $this->redirect(['editors']);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteNew($id){

        $New = $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
