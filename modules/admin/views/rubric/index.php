<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\forms\RubricForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\RubricSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $Model RubricForm */
/* @var integer $rubric_id */

$this->title = 'Рубрики';
$this->params['breadcrumbs'][] = $this->title;

$remove_message = '<div class="before-remove__info"><div class="before-remove__info-inner">В этой рубрике есть публикации.  Для ее удаления, переместите их или удалите.</div></div>';

?>

<?php Pjax::begin() ?>
    <div class="rubric-index">
        <div class="article-container">
            <?php $form = ActiveForm::begin() ?>
                <div class="rubrika-post__inner">
                    <div class="rubrika-post__top fix-post">
                        <div class="rubrika-post__title">
                            Фиксированные
                        </div>
                        <div class="rubrika-post__blocks">
                            <?php foreach(RubricForm::getFixedRubricsList() as $id => $name): ?>
                                <?= $form->field($Model, 'fixed_rubric['.$id.']', ['options' => ['class' => 'rubrika-post__block']])->textInput(['value' => $name])->label(false) ?>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="rubrika-post__bottom alfabet-post">
                        <div class="rubrika-post__title">
                            Алфавитные
                        </div>
                        <div class="before-remove__info">
                            <div class="before-remove__info-inner">
                                В этой рубрике есть публикации.  Для ее удаления, переместите их или удалите.</div>
                        </div>
                        <div class="alfabet-post__blocks">
                            <?php $i = 1; foreach(RubricForm::getAlphabetRubricsList() as $id => $name): ?>
                                <?= $form->field($Model, 'alphabet_rubric['.$id.']', ['template' => '{input}'.Html::a('Удалить', ['delete', 'id' => $id], ['class' => 'btn btn--orange alfabet-post__btn-remove', 'data-method' => ['post']]).(Yii::$app->session->getFlash('rubric_id') == $id ? $remove_message : ''), 'options' => ['class' => 'alfabet-post__block']])->textInput(['value' => $name, 'class' => 'alfabet-post__input form-control'])->label(false) ?>
                            <?php $i++; endforeach; ?>
                            <?php if($i < 50): ?>
                                <?= $form->field($Model, 'alphabet_rubric[new]', ['template' => '{input}'.Html::submitButton('Добавить', ['class' => 'btn btn--green alfabet-post__btn-add']), 'options' => ['class' => 'alfabet-post__block']])->textInput(['class' => 'alfabet-post__input form-control', 'placeholder' => 'Новая рубрика'])->label(false) ?>
                            <?php endif; ?>
                        </div>
                        <div class="alfabet-post__bottom">
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn--green rubrika-post__save']) ?>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
<?php Pjax::end() ?>
