<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */
/* @var $rubric app\modules\admin\models\Rubric */
/* @var $rubricItems array Rubric */
/* @var array $pick_out_rubrics */

$this->title = 'Добавить статью';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">
    <div class="article-container">
        
        <!--<h1><?php // Html::encode($this->title) ?></h1> -->

        <?= $this->render('_form', compact('model', 'rubric', 'rubricItems', 'pick_out_rubrics')) ?>
    </div>
</div>
