<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $New \app\modules\blog\models\News */

?>

<div class="row">
    <div class="col-md-12">
<!--        <h3 style="padding-left: 10px; margin-bottom: 0;">Последнее</h3>-->
<!--        <hr style="margin-top: 10px;">-->
        <div class="small-panel">
            <div class="panel-content">
                <p class="panel-small-text"><?= ($New->date_published != null ? Yii::$app->formatter->asDate($New->date_published, 'php:d.m.Y в H:i') : '') ?></p>
                <?= Html::a($New->title, ['news/view', 'title' => $New->english_title]) ?>
            </div>
        </div>
    </div>
</div>
