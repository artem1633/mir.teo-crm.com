<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $Editors User[] */
/* @var $Model \app\modules\admin\models\forms\EditorForm */

$this->title = 'Редакторы';

?>

<div class="editors-post__inner">
    <div class="editors-post__top">
        <?php $form = ActiveForm::begin() ?>
            <div class="editors-post__profile">
                <?= $form->field($Model, 'url', ['options' => ['class' => false]])->textInput(['placeholder' => 'Ссылка на профиль', 'class' => 'form-control editors-post__profile-input'])->label(false) ?>
                <?= Html::submitButton('Добавить', ['class' => 'btn btn--green editors-post__profile-btn']) ?>
            </div>
        <?php ActiveForm::end() ?>
    </div>
    <?php if($Editors): ?>
        <div class="editors-post__blocks">
            <?php foreach($Editors as $Editor): ?>
                <div class="editors-post__block">
                    <a href="#" class="editors-post__name"><?= !$Editor->name ? Html::encode($Editor->login) : Html::encode($Editor->name) ?></a>
                    <span class="editors-post__post"><?= $Editor->role == User::ROLE_ADMIN ? 'Главный редактор' : '' ?></span>
                    <?php if($Editor->role != User::ROLE_ADMIN): ?>
                        <?= Html::a('<span class="editors-post__remove">Удалить</span>', ['/admin/news/delete-editor', 'id' => $Editor->id], ['data'=> ['method' => 'post']]) ?>
                    <?php endif ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>