<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<div class="news-view">

    <div class="row">
        <div class="col-md-6">
            <h1><?= Html::encode($this->title) ?> <span style="font-size: 16px; color: #9e9e9e;" title="Просмотров"><i class="fa fa-eye"></i> <?= $model->count_news ?></span></h1>
            <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php if($model->status_published != 1): ?>
                            <?= Html::a('Опубликовать <i class="fa fa-check"></i>', ['news/publish', 'id' => $model->id], ['class' => 'btn btn-success',
                                'role'=>'modal-remote', 'title'=>'Опубликовать',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверены?',
                                'data-confirm-message'=>'Вы действительно хотите опубликовать эту статью?'
                            ]) ?>
                        <?php endif; ?>
                        <?= Html::a('Редактировать <i class="fa fa-pencil"></i>', ['news/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить <i class="fa fa-trash"></i>', ['news/delete', 'id' => $model->id], ['class' => 'btn btn-danger',
                            'role'=>'modal-remote', 'title'=>'Удалить',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Вы уверены?',
                            'data-confirm-message'=>'Вы действительно хотите удалить эту статью?'
                            ]) ?>
                        <hr>
                    </div>
                </div>
            <?php endif; ?>
            <?=$model->content?>
        </div>
        <div class="col-md-6">
            <h3>Последнее</h3>
            <hr>
            <?= $this->render('_list', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]) ?>
        </div>
    </div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
