<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */
/* @var $form yii\widgets\ActiveForm */
/* @var $rubric app\modules\admin\models\Rubric */
/* @var $rubricItems array Rubric */
/* @var array $pick_out_rubrics */

$mediaAddress = [];

if($model->isNewRecord == false){
    $mediaAddress = \app\modules\blog\models\MediaAddress::find()->where(['news_id' => $model->id])->all();
}

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<div class="news-form">

    <?php $form = ActiveForm::begin(['id' => 'news-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'Заголовок публикации'])->label('') ?>

		<?= $form->field($model, 'content')->widget(CKEditor::class, [
            'preset' => 'custom',
            'clientOptions' => [
                'filebrowserImageUploadUrl' => '/admin/news/upload-file'
            ],
        ])->label('') ?>

        <div class="new-public__selectors">
            <div class="selector-block">
<!--                < ?php $form->field($model, 'main_news')->checkbox() ?>-->

<!--                < ?= $form->field($model, 'rubric')->widget(\kartik\select2\Select2::class, [-->
<!--                    'data' => $rubricItems,-->
<!--                    'pluginOptions' => [-->
<!--                        'multiple' => 'true',-->
<!--                        'placeholder' => 'Выбрать рубрику'-->
<!--                    ],-->
<!--                ], ['name' => 'Rubric1', 'id' => 'rubric1'])->label('') ?>-->
                <?= $form->field($model, 'rubric')->dropDownList($rubricItems, ['prompt' => 'Выбрать рубрику'])->label(false) ?>
            </div>
            
            <div class="selector-block">
                <?= $form->field($model, 'pick_out_rubric', ['options' => ['tag' => false]])->dropDownList($pick_out_rubrics, ['prompt' => 'Выделить', 'class' => 'highlight__selector'])->label(false) ?>
            </div>
        </div>

        <div class="hidden">
            <?= $form->field($model, 'status_published')->hiddenInput() ?>
        </div>

        <?php if($model->isNewRecord == false): ?>

            <h4>Загруженные файлы</h4>
            <hr>
            <?php \yii\widgets\Pjax::begin(['id' => 'pjax-media-container']) ?>
                <?php foreach ($mediaAddress as $address): ?>
                    <p><img src="/<?= $address->link ?>" style="width: 50px; height: 50px; object-fit: cover;"> <?= Html::a('<i class="fa fa-copy"></i>', '#', [
                            'onclick' => 'event.preventDefault(); copyToClipboard("/'.$address->link.'"); alert("Адрес скопирован в буфер обмена");',
                            'class' => 'btn btn-primary',
                        ]) ?> <?= Html::a('<i class="fa fa-trash"></i>', ['delete-media', 'id' => $address->id], [
                            'class' => 'btn btn-danger',
                            'role' => 'modal-remote',
                            'title'=>'Удалить',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Вы уверены?',
                            'data-confirm-message'=>'Вы действительно хотите удалить данный файл?'
                        ]) ?></p>
                <?php endforeach; ?>
            <?php \yii\widgets\Pjax::end() ?>

        <?php endif; ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'listFile')->widget(MultipleInput::className(), [

                    'id' => 'my_id',
                    'min' => 0,
                    'addButtonOptions' => [
                        'label' => 'Добавить ещё документ',
                    ],
                    'columns' => [
                        [
                            'name' => 'id',
                            'options' => [
                                'type' => 'hidden'
                            ]
                        ],
                        [
                            'name' => 'type',
                            'title' => 'Тип',
                            'type' => 'dropDownList',
                            'items' => [
                                0 => 'Фото',
                                1 => 'Видео',
                            ],
                        ],
                        [
                            'name' => 'file_new',
                            'title' => 'Файл',
                            'type'  => 'fileInput',
                            'options' => [
                                'pluginOptions' => [
                                    'initialPreview'=>[
                                        //add url here from current attribute
                                    ],
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                ]
                            ]
                        ],

                    ],
                ])->label(false) ?>
            </div>
        </div>

	<div class="new-public__buttons">
            <?php if($model->isNewRecord): ?>
                <?= Html::submitButton('Опубликовать', [
                    'class' => 'btn public__btn btn--green',
                    'onclick' => '
                    event.preventDefault();
                    
                    $("#news-status_published").val(1);
                    
                    $("#news-form").submit();
                ',
                ])?>
                <?= Html::submitButton('На модерацию', [
                    'class' => 'btn mederation__btn btn--blue',
                    'onclick' => '
                        event.preventDefault();
                        $("#news-form").submit();
                    '
                ])?>
                <?= Html::submitButton('Отменить', [
                    'class' => 'btn cancel__btn btn--orange',
                    'onclick' => '
                        event.preventDefault();
                        $("#news-form").trigger("reset");
                    '
                ])?>
            <?php else: ?>
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn btn-success'
                ])?>
            <?php endif; ?>
	</div>


<!--    <div class="form-group">
        <?/*= Html::submitButton('Save', ['class' => 'btn btn-success']) */?>
    </div>-->

    <?php ActiveForm::end(); ?>
	</div>



<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
