<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\NewsSearch */
/* @var $News yii\data\ActiveDataProvider */
/* @var integer $news_count */
/* @var integer $limit */

$this->title = 'На модерации';

?>

<div class="moderate-index article-container">
    <div class="blog-last__blocks">
        <?php foreach($News as $New): ?>
            <div class="blog-last__block">
                <a href="#" class="blog-image">
                    <img src="/img/blog-big-photo.png" alt="" title="">
                </a>
                <div class="blog-last__right">
                    <div class="blog-last-time">
                        <?= ($New->date_published != null ? Yii::$app->formatter->asDate($New->date_published, 'php:d.m.Y в H:i') : '') ?>
                    </div>
                    <?php foreach($New->rubricNews as $rubricNew): ?>
                        <?= Html::a(Html::encode($rubricNew->rubric->name), ['#'], ['class' => 'blog-last__type']) ?>
                    <?php endforeach; ?>
                    <a href="#" class="blog-last__title"><?= Html::encode($New->title) ?></a>
                    <a href="#" class="blog-last__anons"><?= HtmlPurifier::process(mb_strimwidth($New->content, 0, 150, '...')) ?></a>

                    <div class="public-post__bottom">
                        <div class="blog-last__bottom">
                            <span class="blog-last__see"><?= $New->count_news ?></span>
                            <span class="blog-last__like"><?= $New->likeCount ?></span>
                            <span class="blog-last__comment"><?= $New->commentCount ?></span>
                        </div>
                        <div class="public-post__ection">
                            <span class="public-post__redact">Редактировать</span>
                            <?= Html::a('<span class="public-post__remove">Удалить</span>', ['delete-new', 'id' => $New->id], ['data' => ['method' => 'post', 'confirm' => 'Удалить публикацию?']]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="bigcol__bottom">
        <?= $news_count > 5 && $limit < $news_count ? Html::a('Показать еще', ['index', 'limit' => $limit + 5], ['class' => 'blog-last__see-all']) : '' ?>
    </div>
</div>
