<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\models\MediaAddress;
use yii\helpers\HtmlPurifier;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $News app\modules\admin\models\NewsSearch */
/* @var $mainNew app\modules\admin\models\News */
/* @var integer $news_count */
/* @var integer $limit */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="news-index article-container">
    <?php Pjax::begin() ?>
        <?php if($News): ?>
            <?php foreach($News as $New): ?>
                <div class="blog-last__blocks">
                    <div class="blog-last__block">
                        <a href="#" class="blog-image">
                            <img src="/img/blog-big-photo.png" alt="" title="">
                        </a>
                        <div class="blog-last__right">
                            <div class="blog-last-time"><?= date('d.m.Y в H:i', $New->date_published) ?></div>
                            <a href="#"
                               class="blog-last__type"><?= isset($New->rubricNews[0]) ? $New->rubricNews[0]->rubric->name : '' ?></a>
                            <a href="#" class="blog-last__title"><?= Html::encode($New->title) ?></a>
                            <a href="#"
                               class="blog-last__anons"><?= HtmlPurifier::process(mb_strimwidth($New->content, 0, 150, '...')) ?></a>

                            <div class="public-post__bottom">
                                <div class="blog-last__bottom">
                                    <span class="blog-last__see"><?= $New->count_news ?></span>
                                    <span class="blog-last__like"><?= $New->likeCount ?></span>
                                    <span class="blog-last__comment"><?= $New->commentCount ?></span>
                                </div>
                                <div class="public-post__ection">
                                    <span class="public-post__redact">Редактировать</span>
                                    <?= Html::a('<span class="public-post__remove">Удалить</span>', ['delete-new', 'id' => $New->id], ['data' => ['method' => 'post', 'confirm' => 'Удалить публикацию?']]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="bigcol__bottom">
                <?= $news_count > 5 && $limit < $news_count ? Html::a('Показать еще', ['index', 'limit' => $limit + 5], ['class' => 'blog-last__see-all']) : '' ?>
            </div>
        <?php endif ?>
    <?php Pjax::end() ?>

<!--    <div class="row">-->
<!--        <div class="col-md-6">-->
<!--            < ?php if($mainNew != null):  ?>-->
<!--                <div class="small-panel">-->
<!--                    <div class="panel-content">-->
<!--                        < ?php-->
<!--                            $img = MediaAddress::find()->where(['news_id' => $mainNew->id, 'type' => MediaAddress::TYPE_PHOTO])->orderBy('id desc')->one();-->
<!--//                        ?>-->
<!--                        < ?php if($img != null): ?>-->
<!--                            <img src="/< ?= $img->link ?>" style="width: 100%; object-fit: cover; margin-bottom: 15px;">-->
<!--                        < ?php endif; ?>-->
<!--                        <p class="panel-small-text">< ?= ($mainNew->date_published != null ? Yii::$app->formatter->asDate($mainNew->date_published, 'php:d.m.Y в H:i') : '') ?></p>-->
<!--                        < ?= Html::a($mainNew->title, ['news/view', 'title' => $mainNew->english_title], ['style' => 'font-size: 25px;']) ?>-->
<!--                    </div>-->
<!--                </div>-->
<!--            < ?php endif; ?>-->
<!--            < ?= $this->render('_list', [-->
<!--                'searchModel' => $searchModel,-->
<!--                'dataProvider' => $dataProvider,-->
<!--            ]) ?>-->
<!--        </div>-->
<!--        <div class="col-md-6">-->
<!--            < ?= $this->render('_list', [-->
<!--                'searchModel' => $searchModel,-->
<!--                'dataProvider' => $rightDataProvider,-->
<!--            ]) ?>-->
<!--        </div>-->
<!--    </div>-->
</div>
