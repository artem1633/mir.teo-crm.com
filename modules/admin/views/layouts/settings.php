<?php

use app\models\UserSearch;
use yii\widgets\ActiveForm;
use app\helpers\AvatarResolver;
use yii\helpers\Html;


?>

    <?php

        $models = \app\models\News::find()->where(['status_published' => 1])->limit(10)->orderBy('count_news desc')->all();


    ?>

<div class="theme-panel active" style="top: 40px; height: 100%; z-index: 1; background: #efcb5b;">
<!--            <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>-->

    <h5 style="font-weight: bold;">Популярные</h5>
    <hr style="margin-top: 5px; margin-bottom: 20px;">

<?php foreach ($models as $model): ?>

    <h5><?= Html::a($model->title, ['news/view', 'title' => $model->english_title], ['style' => 'font-weight: bold;']) ?></h5>
    <hr style="margin-top: 5px; margin-bottom: 5px;">

<?php endforeach; ?>

<!--        <div class="col-md-12">-->
<!--            <div class="col-md-3" style="padding-right: 0px!important;">-->
<!--                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQvpvNrkI5_zm4Kbdp6EYuLcoJ5Ije93aOxSpbOyiOdgbwFrA5y" alt="" style="border-radius: 100%; width: 55px; height: 55px; object-fit: cover;">-->
<!--            </div>-->
<!--            <div class="col-md-3" style="padding-right: 0px!important;">-->
<!--                <p style="color:black;">120 чел</p>-->
<!--                <p style="color:black;">$85 000</p>-->
<!--            </div>-->
<!--            <div class="col-md-3" style="padding-right: 0px!important;">-->
<!--                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQvpvNrkI5_zm4Kbdp6EYuLcoJ5Ije93aOxSpbOyiOdgbwFrA5y" alt="" style="border-radius: 100%; width: 55px; height: 55px; object-fit: cover;">-->
<!--            </div>-->
<!--            <div class="col-md-3" style="padding-right: 0px!important;">-->
<!--                <p style="color:black;">120 чел</p>-->
<!--                <p style="color:black;">$85 000</p>-->
<!--            </div>-->

    </div>
        <div class="">
            <div class="">
                <div class="row">


                </div>
            </div>
        </div>
    </div>
        </div>

<?php

$script = <<< JS

$('#user-list-form input').change(function(){
    $('#user-list-form').submit();
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
