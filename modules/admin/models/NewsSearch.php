<?php

namespace app\modules\admin\models;

use phpDocumentor\Reflection\Types\False_;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\News;
use yii\helpers\ArrayHelper;

/**
 * NewsSearch represents the model behind the search form of `app\modules\admin\models\News`.
 */
class NewsSearch extends News
{
    public $rubric;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'date_published', 'status_published', 'main_news', 'count_news', 'rubric'], 'integer'],
            [['title', 'english_title', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params, $limit, $count = false, $moderation = false)
    {
        $query = News::find()
            ->innerJoinWith('rubricNews')
            ->orderBy(['news.date_published' => SORT_DESC]);

        // add conditions that should always apply here

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return false;
        }

        if($moderation){
            $query->where(['or', ['status_published' => 0], ['status_published' => null]]);
        }else{
            $query->where(['news.status_published' => 1]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'news.date_published' => $this->date_published,
            'news.status_published' => $this->status_published,
            'news.main_news' => $this->main_news,
            'news.count_news' => $this->count_news,
        ]);

        $query->andFilterWhere(['like', 'news.title', $this->title])
            ->andFilterWhere(['like', 'news.english_title', $this->english_title])
            ->andFilterWhere(['like', 'news.content', $this->content]);

        if($this->rubric != null){
            $rubricsPks = ArrayHelper::getColumn(RubricNews::find()->where(['rubric_id' => $this->rubric])->all(), 'news_id');

            $query->andFilterWhere(['news.id' => $rubricsPks]);
        }
        $query->limit($limit);

        return $count ? $query->count() : $query->all();
    }
}
