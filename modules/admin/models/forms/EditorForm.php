<?php

namespace app\modules\admin\models\forms;

use Yii;
use app\models\User;
use yii\base\Model;

/**
 * Class EditorForm
 *
 * @package app\modules\admin\models\forms
 *
 * @property string $link ссылка на профиль
 */

class EditorForm extends Model{

    public $url;

    /**
     * @inheritDoc
     * @return array
     */
    public function rules(){

        return [
            ['url', 'required'],
            ['url', 'url'],
            ['url', 'string']
        ];
    }

    /**
     * Сохранение данных
     *
     * @return bool
     */
    public function saveData(){

        if(preg_match('/id=\d+$/', $this->url)){
            $explode_url = explode('=', $this->url);
            if($User = User::findOne(['id' => end($explode_url)])){
                if($User->role == User::ROLE_EDITOR){
                    Yii::$app->session->setFlash('error', 'Пользователь уже добавлен как редактор');
                    return false;
                }
                $User->role = User::ROLE_EDITOR;
                if($User->save(false)){
                    Yii::$app->session->setFlash('success', 'Редактор успешно добавлен');
                    return true;
                }
            }else{
                Yii::$app->session->setFlash('error', 'Пользователь с таким id не найден');
            }
        }
        return false;
    }
}