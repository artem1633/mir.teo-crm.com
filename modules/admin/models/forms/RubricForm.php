<?php

namespace app\modules\admin\models\forms;

use Yii;
use app\modules\blog\models\Rubric;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class RubricForm
 *
 * @package app\modules\admin\models\forms
 *
 * @property array $fixed_rubric фиксированные рубрики
 * @property array $alphabet_rubric алфавитные рубрики
 */

class RubricForm extends Model{

    public $fixed_rubric = [];
    public $alphabet_rubric = [];

    /**
     * @return array
     */
    public static function getFixedRubricsList(){

        return ArrayHelper::map(Rubric::findAll(['type' => Rubric::TYPE_FIXED]), 'id', 'name');
    }

    /**
     * @return array
     */
    public static function getAlphabetRubricsList(){

        return ArrayHelper::map(Rubric::find()->where(['type' => Rubric::TYPE_ALPHABETICAL])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function rules(){

        return [
            [['fixed_rubric', 'alphabet_rubric'], 'each', 'rule' => ['string']],
            [['fixed_rubric', 'alphabet_rubric'], 'safe']
        ];
    }

    /**
     * Сохранение рубрик
     *
     * @return void
     */
    public function saveData(){

        if($this->fixed_rubric){
            foreach($this->fixed_rubric as $id => $value){
                if(($Rubric = Rubric::findOne(['id' => $id, 'type' => Rubric::TYPE_FIXED])) && $value){
                    $Rubric->name = $value;
                    if(!$Rubric->save()){
                        Yii::error(print_r($Rubric->getErrors(), true));
                    }
                }
            }
        }
        if($this->alphabet_rubric){
            foreach($this->alphabet_rubric as $id => $value){
                if($id === 'new' && $value){
                    $Rubric = new Rubric();
                    $Rubric->name = $value;
                    $Rubric->type = Rubric::TYPE_ALPHABETICAL;
                    if(!$Rubric->save()){
                        Yii::error(print_r($Rubric->getErrors(), true));
                    }
                }elseif($Rubric = Rubric::findOne(['id' => $id, 'type' => Rubric::TYPE_ALPHABETICAL])){
                    $Rubric->name = $value;
                    if(!$Rubric->save()){
                        Yii::error(print_r($Rubric->getErrors(), true));
                    }
                }
            }
        }
    }
}