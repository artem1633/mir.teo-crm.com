<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "rubric".
 *
 * @property int $id
 * @property string $name Наименование рубрики
 *
 * @property RubricNews[] $rubricNews
 */
class Rubric extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubric';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование рубрики',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubricNews()
    {
        return $this->hasMany(RubricNews::class, ['rubric_id' => 'id']);
    }
}
