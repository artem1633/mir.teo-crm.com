<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "media_address".
 *
 * @property int $id
 * @property int $news_id ID новости
 * @property string $link Ссылка на ресурс
 * @property string $type Тип ресурса
 *
 * @property News $news
 */
class MediaAddress extends \yii\db\ActiveRecord
{
    const TYPE_PHOTO = 0;
    const TYPE_VIDEO = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id'], 'integer'],
            [['link', 'type'], 'string', 'max' => 255],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'ID новости',
            'link' => 'Ссылка на ресурс',
            'type' => 'Тип ресурса',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
