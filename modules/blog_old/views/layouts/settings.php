<?php

use app\models\UserSearch;
use yii\widgets\ActiveForm;
use app\helpers\AvatarResolver;
use yii\helpers\Html;


?>
<?php if(Yii::$app->controller->id == 'dashboard'): ?>

    <?php

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchByRating(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

    ?>

<div class="theme-panel active" style="top: 40px; height: 100%; z-index: 1; background: #efcb5b;">
<!--            <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>-->


    <?php $form = ActiveForm::begin(['id' => 'user-list-form', 'method' => 'GET']); ?>

        <?= $form->field($searchModel, 'sortingRight')->radioList(
                ['6' => 'Лидеры', '1' => 'По чел.', '2' => 'По $'],
                [
                    'item' => function($index, $label, $name, $checked, $value){
                        return "
                        
                        <label class='checkcontainer ".($checked ? 'checkcontainer-checked' : '')."' style='width: 32%; text-align: center;'>
                            {$label}
                            <input type='radio' name='{$name}' value='{$value}' ".($checked ? "checked='checked'": '').">
                        </label>
                    ";
                    },
                ]
        )->label(false) ?>

    <?php ActiveForm::end() ?>

    <?php foreach ($dataProvider->models as $model): ?>

        <div class="col-md-6 col-sm-12">
            <div style="margin-bottom: 30px;" title="<?= $model['name'] ?>">
                <?= Html::a('<img src="/'.AvatarResolver::getRealAvatarPath($model['avatar']).'" alt="" style="display: inline-block; float: left; border-radius: 100%; width: 55px; height: 55px; object-fit: cover;">', ['user/view', 'id' => $model['id']]) ?>
                <p style="color:black;"><?= $model['count_ref'] ?> чел</p>
                <p style="color:black;">$<?= $model['balance_usd'] ?></p>
            </div>
        </div>

    <?php endforeach; ?>

<!--        <div class="col-md-12">-->
<!--            <div class="col-md-3" style="padding-right: 0px!important;">-->
<!--                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQvpvNrkI5_zm4Kbdp6EYuLcoJ5Ije93aOxSpbOyiOdgbwFrA5y" alt="" style="border-radius: 100%; width: 55px; height: 55px; object-fit: cover;">-->
<!--            </div>-->
<!--            <div class="col-md-3" style="padding-right: 0px!important;">-->
<!--                <p style="color:black;">120 чел</p>-->
<!--                <p style="color:black;">$85 000</p>-->
<!--            </div>-->
<!--            <div class="col-md-3" style="padding-right: 0px!important;">-->
<!--                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQvpvNrkI5_zm4Kbdp6EYuLcoJ5Ije93aOxSpbOyiOdgbwFrA5y" alt="" style="border-radius: 100%; width: 55px; height: 55px; object-fit: cover;">-->
<!--            </div>-->
<!--            <div class="col-md-3" style="padding-right: 0px!important;">-->
<!--                <p style="color:black;">120 чел</p>-->
<!--                <p style="color:black;">$85 000</p>-->
<!--            </div>-->

    </div>
        <div class="">
            <div class="">
                <div class="row">


                </div>
            </div>
        </div>
    </div>
        </div>
<?php endif; ?>


<?php

$script = <<< JS

$('#user-list-form input').change(function(){
    $('#user-list-form').submit();
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
