<?php

use app\admintheme\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

$level1Count = 0;
$level2Count = 0;
$level3Count = 0;
$level4Count = 0;
$level5Count = 0;
$level6Count = 0;
$level7Count = 0;

if (Yii::$app->user->isGuest == false) {
    $levels = Yii::$app->user->identity->refer->getLevelsCount();

    $level1Count = $levels['1'];
    $level2Count = $levels['2'];
    $level3Count = $levels['3'];
    $level4Count = $levels['4'];
    $level5Count = $levels['5'];
    $level6Count = $levels['6'];
    $level7Count = $levels['7'];
}

?>

<div id="sidebar" class="sidebar" style="padding-top: 0; z-index: 2000;">

    <div class="sidebar-header">
        <?= Html::a('<img src="/img/menu-logo.png" style="height: 25px; margin: 8px 8px;">', ['dashboard/index']) ?>
        <?= Html::a('24', '#') ?>
        <?= Html::a('Telegram', '#') ?>
        <?= Html::a('Дайджест', '#') ?>
        <?= Html::a('Блог', Url::to(['/blog/post/index'])) ?>
    </div>

    <?php if (Yii::$app->controller->id != 'chat'): ?>
        <div class="left-menu">

            <?/*= Html::a('Заработать', '#', [
                'class' => 'btn btn-warning btn-sm',
                'style' => 'margin-left: 10px; margin-top: 5px;',
                'data-ref-url' => Yii::$app->user->identity->getSelfRefHref(),
                'onclick' => 'copyToClipboard($(this).data("ref-url")); alert("Ссылка скопирована в буфер обмена")'
            ]); */?>

            <?php if (Yii::$app->user->isGuest == false): ?>
                <?php
                echo Menu::widget(
                    [
                        'options' => ['class' => 'nav', 'style' => 'margin-top: 20px;'],
                        'items' => [
                            [
                                'label' => 'Главное',
//                                'icon' => 'fa  fa-users',
                                'url' => ['/blog/post/index'],
                            ],
                            [
                                'label' => 'Важное',
//                                'icon' => 'fa  fa-user-o',
                                'url' => ['/user'],
                            ],
                            [
                                'label' => 'Внешний мир',
//                                'icon' => 'fa  fa-plus',
                                'url' => ['/accruals/index'],
                            ],
                            [
                                'label' => 'Сообщество',
//                                'icon' => 'fa  fa-cog',
                                'url' => ['/settings'],
                            ],
                    		[
                    			'label' => 'Лидеры',
//								'icon' => 'fa  fa-pencil',
								'url' => ['/ticket'],
							],
                    		[
                    			'label' => 'Политика',
//								'icon' => 'fa  fa-th',
								'url' => ['/news'],
							],
                    		[
                    			'label' => 'Экономика',
//								'icon' => 'fa  fa-th',
								'url' => ['/faq'],
							],
                            [
                                'label' => 'Культура',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Партия',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Профсоюз',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Тотсомол',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Январята',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Бизнес',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Инвестиции',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Заработать',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Криптовалюты',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Выборы',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Презентации',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Вебинары',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                            [
                                'label' => 'Разное',
//                                'icon' => 'fa  fa-th',
                                'url' => ['/faq'],
                            ],
                        ],
                    ]
                );
                ?>

<!--                --><?/*= Html::a('Оповестить', ['/chat/notify'],
                    [
                        'class' => 'btn btn-success btn-sm',
                        'style' => 'margin-left: 10px; margin-top: 5px;',
                        'role' => 'modal-remote',
                        'data-pjax' => 1,
                    ]); */?>

            <?php endif; ?>
        </div>
    <?php else: ?>
        <?= $this->render('/chat/partner_list.php') ?>
    <?php endif; ?>
</div>
