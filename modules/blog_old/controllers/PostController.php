<?php

namespace app\modules\blog\controllers;


use yii\web\Controller;

class PostController extends Controller
{

    /**
     * Вывод заглавной страницы модуля "Блог"
     */
    public function actionIndex()
    {
        $this->view->title = 'Module Blog';
        return $this->render('index');
    }

}