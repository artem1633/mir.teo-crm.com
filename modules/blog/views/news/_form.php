<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */
/* @var $form yii\widgets\ActiveForm */
/* @var $rubric app\modules\admin\models\Rubric */
/* @var $rubricItems array Rubric*/

?>
<div class="container-fluid">
	<div class="news-form col-sm-8 col-md-8 col-xs-8">

    <?php $form = ActiveForm::begin(['id' => 'news-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

		<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'content')->widget(\mihaildev\ckeditor\CKEditor::class, [

        ]) ?>

		<div class="row">
			<div class="col col-sm-4 col-md-4 col-xs-4">
				<?= $form->field($model, 'main_news')->checkbox() ?>

				<?= $form->field($model, 'rubric')->widget(\kartik\select2\Select2::class, [
                    'data' => $rubricItems,
                    'pluginOptions' => [
                        'multiple' => 'true',
                    ],
                ], ['name' => 'Rubric1', 'id' => 'rubric1'])->label('Рубрика 1') ?>
			</div>
		</div>

        <div class="hidden">
            <?= $form->field($model, 'status_published')->hiddenInput() ?>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'listFile')->widget(MultipleInput::className(), [

                    'id' => 'my_id',
                    'min' => 0,
                    'addButtonOptions' => [
                        'label' => 'Добавить ещё документ',
                    ],
                    'columns' => [
                        [
                            'name' => 'id',
                            'options' => [
                                'type' => 'hidden'
                            ]
                        ],
                        [
                            'name' => 'type',
                            'title' => 'Тип',
                            'type' => 'dropDownList',
                            'items' => [
                                0 => 'Фото',
                                1 => 'Видео',
                            ],
                        ],
                        [
                            'name' => 'file_new',
                            'title' => 'Файл',
                            'type'  => 'fileInput',
                            'options' => [
                                'pluginOptions' => [
                                    'initialPreview'=>[
                                        //add url here from current attribute
                                    ],
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                ]
                            ]
                        ],

                    ],
                ])->label(false) ?>
            </div>
        </div>
	<div class="row">
		<div class="btn-group">
            <?php if($model->isNewRecord): ?>
                <?= Html::submitButton('Опубликовать', [
                    'class' => 'btn btn-success',
                    'onclick' => '
                    event.preventDefault();
                    
                    $("#news-status_published").val(1);
                    
                    $("#news-form").submit();
                ',
                ])?>
                <?= Html::submitButton('На модерацию', [
                    'class' => 'btn btn-success'
                ])?>
                <?= Html::submitButton('Отменить', [
                    'class' => 'btn btn-danger',
                ])?>
            <?php else: ?>
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn btn-success'
                ])?>
            <?php endif; ?>
		</div>
	</div>

<!--    <div class="form-group">
        <?/*= Html::submitButton('Save', ['class' => 'btn btn-success']) */?>
    </div>-->

    <?php ActiveForm::end(); ?>
	</div>
</div>
