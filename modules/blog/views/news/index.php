<?php

use app\modules\blog\models\RubricNews;
use yii\helpers\Html;
use app\modules\admin\models\MediaAddress;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $rubrics \app\modules\blog\models\Rubric[] */
/* @var string $search */
/* @var $searchModel app\modules\admin\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $rightDataProvider yii\data\ActiveDataProvider */
/* @var $mainNew app\modules\admin\models\News */
/* @var $Rubric \app\modules\blog\models\Rubric */
/* @var integer $news_count */
/* @var $mainRubrics \app\modules\blog\models\Rubric[] */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;

$limit = Yii::$app->request->post('limit');

?>

<?php Pjax::begin() ?>
<div class="news-index container">
    <div class="blog-page__content">
        <div class="blog-page__blocks">
            <div class="blog-page__bigcol">
                <div class="blog-last">
                    <div class="blog-last__header">
                        <strong><?= $Rubric ? Html::encode($Rubric->name) : 'Последние публикации' ?></strong>
                        <div class="dropdown types-blog__dropdown">
                            <button class="types-blog" type="button" id="typesblogMenuButton" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">Рубрики
                            </button>
                            <div class="blog-menu dropdown-menu" aria-labelledby="typesblogMenuButton">
                                <?php foreach($rubrics as $rubric): ?>
                                    <?= Html::a(Html::encode($rubric->name), ['/blog/news/index', 'rubric' => $rubric->slug], ['class' => 'blog-menu__item']) ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="blog-last__blocks">
                        <?php foreach($dataProvider->models as $model): ?>
                            <?php $rubrics = RubricNews::find()->where(['news_id' => $model->id])->all(); ?>
                            <div class="blog-last__block">
                                <a href="<?= Url::toRoute(['news/view', 'title' => $model->english_title]) ?>"
                                   class="blog-image">
                                    <img src="/img/blog-big-photo.png" alt="" title="">
                                </a>
                                <div class="blog-last__right">
                                    <div class="blog-last-time"><?= Yii::$app->formatter->asDate($model->date_published, 'php:d.m.Y в H:i') ?></div>
                                    <?php foreach($rubrics as $rubric): ?>
                                        <?= Html::a(Html::encode($rubric->rubric->name), ['/blog/news/index', 'rubric' => $rubric->rubric->slug], ['class' => 'blog-last__type']) ?>
                                    <?php endforeach; ?>
                                    <a href="<?= Url::toRoute(['news/view', 'title' => $model->english_title]) ?>"
                                       class="blog-last__title"><?= $model->title ?></a>
                                    <a href="<?= Url::toRoute(['news/view', 'title' => $model->english_title]) ?>"
                                       class="blog-last__anons"><?= Html::encode($model->getSmallContent()) ?></a>

                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see"><?= $model->count_news ?></span>
                                        <span class="blog-last__like"><?= $model->likesCount ?></span>
                                        <span class="blog-last__comment"><?= $model->commentsCount ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="bigcol__bottom">
                        <?= ($news_count > 5 && $limit < $news_count && !$search) ? Html::a('Показать еще', ['/blog/news/index', 'rubric' => $Rubric ? $Rubric->slug : null], ['class' => 'blog-last__see-all', 'data' => ['method' => 'post', 'pjax' => true, 'params' => ['limit' => $limit ? $limit + 5 : 10]]]) : '' ?>
                    </div>
                </div>
            </div>
            <div class="blog-page__smallcol">
                <div class="exspress-blog">
                    <div class="blog-smallcol__header">
                        <div class="blog-smallcol__title">Самые срочные</div>
                    </div>
                    <div class="blog-smallcol__blocks">
                        <?php foreach ($expressDataProvider->models as $model): ?>
                            <div class="blog-smallcol__block">
                                <div class="smallcol-block__body">
                                    <a href="<?= Url::to(['/blog/news/view', 'title' => $model->english_title]) ?>" class="blog-smallcol__image">
                                        <img src="/img/blog-small-img.png" alt="" title="">
                                    </a>
                                    <a href="<?= Url::to(['/blog/news/view', 'title' => $model->english_title]) ?>" class="blog-smallcol__text">
                                        <?= $model->title ?>
                                    </a>
                                </div>
                                <div class="blog-last__bottom">
                                    <span class="blog-last__see"><?= $model->count_news ?></span>
                                    <span class="blog-last__like"><?= $model->likesCount ?></span>
                                    <span class="blog-last__comment"><?= $model->commentsCount ?></span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="exspress-blog__bottom">
                        <?= Html::a('Смотреть все', ['/blog/news/index', 'rubric' => isset($mainRubrics[2]) ? $mainRubrics[2]->slug : ''], ['class' => 'exspress-blog__see-all', 'data' => ['pjax' => 0]]) ?>
                    </div>
                </div>

                <div class="populat-blog">
                    <div class="blog-smallcol__header">
                        <div class="blog-smallcol__title">Самые популярные</div>
                        <ul class="nav nav-tabs" id="populat-blogTab" role="tablist">
                            <li class="nav-item active" role="presentation">
                                <a class="nav-link" id="pop-see-tab" data-toggle="tab" href="#pop-see" role="tab" aria-controls="pop-see" aria-selected="true">По просмотрам</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link " id="pop-like-tab" data-toggle="tab" href="#pop-like" role="tab" aria-controls="pop-like" aria-selected="false">По лайкам</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link " id="pop-comm-tab" data-toggle="tab" href="#pop-comm" role="tab" aria-controls="pop-comm" aria-selected="false">По комментам</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active in" id="pop-see" role="tabpanel" aria-labelledby="pop-see-tab">
                            <div class="blog-smallcol__blocks">
                                <?php foreach ($popularDataProvider->models as $model): ?>
                                    <div class="blog-smallcol__block">
                                        <div class="smallcol-block__body">
                                            <a href="<?= Url::to(['/blog/news/view', 'title' => $model->english_title]) ?>" class="blog-smallcol__image">
                                                <img src="/img/blog-small-img.png" alt="" title="">
                                            </a>
                                            <a href="<?= Url::to(['/blog/news/view', 'title' => $model->english_title]) ?>" class="blog-smallcol__text">
                                                <?= $model->title ?>
                                            </a>
                                        </div>
                                        <div class="blog-last__bottom">
                                            <span class="blog-last__see"><?= $model->count_news ?></span>
                                            <span class="blog-last__like"><?= $model->likesCount ?></span>
                                            <span class="blog-last__comment"><?= $model->commentsCount ?></span>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pop-like" role="tabpanel" aria-labelledby="pop-like-tab">
                            <div class="blog-smallcol__blocks">
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pop-comm" role="tabpanel" aria-labelledby="pop-comm-tab">
                            <div class="blog-smallcol__blocks">
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                                <div class="blog-smallcol__block">
                                    <div class="smallcol-block__body">
                                        <a href="#" class="blog-smallcol__image">
                                            <img src="/img/blog-small-img.png" alt="" title="">
                                        </a>
                                        <a href="#" class="blog-smallcol__text">
                                            Заголовок статьи будет здесь, а если он длинный, то будет в несколько строк, максимум три 
                                        </a>
                                    </div>
                                    <div class="blog-last__bottom">
                                        <span class="blog-last__see">1020</span>
                                        <span class="blog-last__like">138</span>
                                        <span class="blog-last__comment">22555</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="popular-blog__bottom">
                        <?= Html::a('Смотреть все', ['/blog/news/index', 'rubric' => isset($mainRubrics[3]) ? $mainRubrics[3]->slug : ''], ['class' => 'popular-blog__see-all', 'data' => ['pjax' => 0]]) ?>
                    </div>
                </div>

                <div class="important-blog">
                    <div class="blog-smallcol__header">
                        <div class="blog-smallcol__title">Самые важные</div>
                    </div>
                    <div class="blog-smallcol__blocks">
                        <?php foreach ($importantDataProvider->models as $model): ?>
                        <div class="blog-smallcol__block">
                            <div class="smallcol-block__body">
                                <a href="<?= Url::to(['/blog/news/view', 'title' => $model->english_title]) ?>" class="blog-smallcol__image">
                                    <img src="/img/blog-small-img.png" alt="" title="">
                                </a>
                                <a href="<?= Url::to(['/blog/news/view', 'title' => $model->english_title]) ?>" class="blog-smallcol__text">
                                    <?= $model->title ?>
                                </a>
                            </div>
                            <div class="blog-last__bottom">
                                <span class="blog-last__see"><?= $model->count_news ?></span>
                                <span class="blog-last__like"><?= $model->likesCount ?></span>
                                <span class="blog-last__comment"><?= $model->commentsCount ?></span>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="important-blog__bottom">
                        <?= Html::a('Смотреть все', ['/blog/news/index', 'rubric' => isset($mainRubrics[4]) ? $mainRubrics[4]->slug : ''], ['class' => 'important-blog__see-all', 'data' => ['pjax' => 0]]) ?>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!--<div class="row">
        <div class="col-md-6">
            <?php /* if($mainNew != null):  ?>
                <div class="small-panel">
                    <div class="panel-content">
                        <?php
                            $img = MediaAddress::find()->where(['news_id' => $mainNew->id, 'type' => MediaAddress::TYPE_PHOTO])->orderBy('id desc')->one();
                        ?>
                        <?php if($img != null): ?>
                            <img src="/<?= $img->link ?>" style="width: 100%; object-fit: cover; margin-bottom: 15px;">
                        <?php endif; ?>
                        <p class="panel-small-text"><?= ($mainNew->date_published != null ? Yii::$app->formatter->asDate($mainNew->date_published, 'php:d.m.Y в H:i') : '') ?></p>
                        <?= Html::a($mainNew->title, ['news/view', 'title' => $mainNew->english_title], ['style' => 'font-size: 25px;']) ?>
                    </div>
                </div>
            <?php endif; ?>
            <?= $this->render('_list', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $this->render('_list', [
                'searchModel' => $searchModel,
                'dataProvider' => $rightDataProvider,
            ]) */ ?>
        </div>
    </div> -->
</div>
<?php Pjax::end() ?>
