<?php

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'На модерации';

?>

<div class="moderate-index">
    <div class="row">
        <div class="col-md-12">
            <h3>На модерации</h3>
            <hr>
        </div>
    </div>
    <?= $this->render('_list', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
