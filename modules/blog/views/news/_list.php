<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="row">
    <div class="col-md-12">
<!--        <h3 style="padding-left: 10px; margin-bottom: 0;">Последнее</h3>-->
<!--        <hr style="margin-top: 10px;">-->
        <?php foreach ($dataProvider->models as $model): ?>
            <div class="small-panel">
                <div class="panel-content">
                    <p class="panel-small-text"><?= ($model->date_published != null ? Yii::$app->formatter->asDate($model->date_published, 'php:d.m.Y в H:i') : '') ?></p>
                    <?= Html::a($model->title, ['news/view', 'title' => $model->english_title]) ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
