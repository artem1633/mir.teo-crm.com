<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */
/* @var $rubrics \app\modules\blog\models\Rubric[] */
/* @var $comments \app\models\NewsComment[] */
/* @var integer $comments_count */
/* @var $commentForm \app\modules\blog\models\CommentForm */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<div class="news-view container">
    
    
    <div class="article__body">
        <div class="article-container">
            <div class="article__block">
                <div class="article__header">
                    <div class="acticle__data"><?= Yii::$app->formatter->asDate($model->date_published, 'php:d.m.Y в H:i') ?></div>
                    <?php foreach($rubrics as $rubric): ?>
                        <?= Html::a(Html::encode($rubric->rubric->name), ['/blog/news/index', 'rubric' => $rubric->rubric->slug], ['class' => 'acticle__type']) ?>
                    <?php endforeach; ?>
                    <div class="acticle__title"><?= Html::encode($model->title) ?></div>
                </div>
                <div class="article__content">
                    <?= HtmlPurifier::process($model->content) ?>
                </div>
                <div class="blog-last__bottom">
                    <span class="blog-last__see"><?= $model->count_news ?></span>
                    <?= Html::a($model->likesCount, ['/blog/news/add-like', 'id' => $model->id], ['class' => 'blog-last__like active', 'data-view' => '']) ?>
                    <span class="blog-last__comment active"><?= $model->commentsCount ?></span>
                </div>
            </div>
        </div>
    </div>
    
    <div class="article-reviews">
        <div class="article-container">
            <?php Pjax::begin([
                'id' => 'comments',
            ]) ?>
                <?php if($comments): ?>
                    <div class="article-reviews__blocks">
                        <?php foreach($comments as $comment): ?>
                            <div class="article-reviews__block public">
                                <div class="article-reviews__avatar">
                                    <img src="/img/nouser.png" alt="" title="">
                                </div>
                                <div class="article-reviews__block--right">
                                    <div class="article-reviews__content">
                                        <div class="article-reviews__text"><?= Html::encode($comment->content) ?></div>
                                    </div>
                                    <?php if(Yii::$app->user->getId() == $comment->author_id || Yii::$app->user->identity->role == User::ROLE_EDITOR): ?>
                                        <div class="article-reviews__action">
                                            <span class="article-reviews__edit" id="update-comment" data-url="<?=  Url::to(['/blog/news/update-comment', 'id' => $comment->id]) ?>">Редактировать</span>
                                            <span class="article-reviews__delete" id="delete-comment" data-url="<?= Url::to(['/blog/news/delete-comment', 'id' => $comment->id]) ?>">Удалить</span>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php if($comments_count > 3 && $comments_count != count($comments)): ?>
                            <div class="article-reviews__bottom">
                                <?= Html::a('<button type="button" class="article-reviews__btn">Еще комментарии</button>', ['/blog/news/view', 'title' => $model->english_title], ['style' => ['color' => '#fff'], 'data' => ['pjax' => 1, 'method' => 'post', 'params' => ['comment_limit' => 'all']]]) ?>
                            </div>
                        <?php endif ?>
                    </div>
                <?php endif ?>
            <?php Pjax::end() ?>

            <?php $form = ActiveForm::begin() ?>
                <div class="leave-comment">
                    <div class="article-reviews__block">
                        <div class="article-reviews__avatar">
                            <img src="/img/nouser.png" alt="" title="">
                        </div>
                        <?= $form->field($commentForm, 'comment', ['options' => ['tag' => false]])->textarea(['class' => 'leave-comment__textarea'])->label(false) ?>
                    </div>
                    <div class="leave-comment__bottom">
                        <?= Html::submitButton('Опубликовать', ['class' => 'article-reviews__btn']) ?>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>

<!--    <div class="row">-->
<!--        <div class="col-md-6">-->
<!--            <h1>< ?= Html::encode($this->title) ?> <span style="font-size: 16px; color: #9e9e9e;" title="Просмотров"><i class="fa fa-eye"></i> < ?= $model->count_news ?></span></h1>-->
<!--            < ?php if(Yii::$app->user->identity->isSuperAdmin()): ?>-->
<!--                <div class="row">-->
<!--                    <div class="col-md-12">-->
<!--                        < ?php if($model->status_published != 1): ?>-->
<!--                            < ?= Html::a('Опубликовать <i class="fa fa-check"></i>', ['news/publish', 'id' => $model->id], ['class' => 'btn btn-success',-->
<!--                                'role'=>'modal-remote', 'title'=>'Опубликовать',-->
<!--                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api-->
<!--                                'data-request-method'=>'post',-->
<!--                                'data-confirm-title'=>'Вы уверены?',-->
<!--                                'data-confirm-message'=>'Вы действительно хотите опубликовать эту статью?'-->
<!--                            ]) ?>-->
<!--                        < ?php endif; ?>-->
<!--                        < ?= Html::a('Редактировать <i class="fa fa-pencil"></i>', ['news/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>-->
<!--                        < ?= Html::a('Удалить <i class="fa fa-trash"></i>', ['news/delete', 'id' => $model->id], ['class' => 'btn btn-danger',-->
<!--                            'role'=>'modal-remote', 'title'=>'Удалить',-->
<!--                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api-->
<!--                            'data-request-method'=>'post',-->
<!--                            'data-confirm-title'=>'Вы уверены?',-->
<!--                            'data-confirm-message'=>'Вы действительно хотите удалить эту статью?'-->
<!--                            ]) ?>-->
<!--                        <hr>-->
<!--                    </div>-->
<!--                </div>-->
<!--            < ?php endif; ?>-->
<!--            < ?= $model->content ?>-->
<!--        </div>-->
<!--        <div class="col-md-6">-->
<!--            <h3>Последнее</h3>-->
<!--            <hr>-->
<!--            < ?= $this->render('_list', [-->
<!--                'searchModel' => $searchModel,-->
<!--                'dataProvider' => $dataProvider,-->
<!--            ]) ?>-->
<!--        </div>-->
<!--    </div>-->
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
