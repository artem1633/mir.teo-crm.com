<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */
/* @var $rubric app\modules\admin\models\Rubric */
/* @var $rubricItems array Rubric*/

$this->title = 'Добавить статью';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'rubric' => $rubric,
    	'rubricItems' => $rubricItems
    ]) ?>

</div>
