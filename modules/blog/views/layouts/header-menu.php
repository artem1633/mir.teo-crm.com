<?php

use app\admintheme\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\News;
use app\modules\blog\models\forms\NewsSearchForm;

$level1Count = 0;
$level2Count = 0;
$level3Count = 0;
$level4Count = 0;
$level5Count = 0;
$level6Count = 0;
$level7Count = 0;

if (Yii::$app->user->isGuest == false) {
    $levels = Yii::$app->user->identity->refer->getLevelsCount();

    $level1Count = $levels['1'];
    $level2Count = $levels['2'];
    $level3Count = $levels['3'];
    $level4Count = $levels['4'];
    $level5Count = $levels['5'];
    $level6Count = $levels['6'];
    $level7Count = $levels['7'];
}

$rubricsMenu = [];
$rubrics = \app\modules\admin\models\Rubric::find()->all();

foreach ($rubrics as $rubric){
    $rubricsMenu[] = [
        'label' => $rubric->name,
        'url' => ['/blog/news/index?NewsSearch[rubric]='.$rubric->id],
    ];
}

$notPublishedCount = News::find()->where(['or', ['status_published' => 0], ['status_published' => null]])->count();
$publishedCount = News::find()->where(['status_published' => 1])->count();
$search = new NewsSearchForm();

?>

<div id="sidebar" class="sidebar" style="padding-top: 0; z-index: 2000;">

    <div class="container">
        <div class="blog-header">
            <div class="blog-header__inner">
                <div class="blog-header__menu dropdown">
                    <button class="hamburger hamburger1" type="button" id="blogMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="bar bar1"></span>
                        <span class="bar bar2"></span>
                        <span class="bar bar3"></span>
                        <span class="bar bar4"></span>
                    </button>
                    <div class="blog-menu dropdown-menu" aria-labelledby="blogMenuButton">
                        <?php foreach($rubrics as $rubric): ?>
                            <?= Html::a(Html::encode($rubric->name), ['/blog/news/index', 'rubric' => $rubric->slug], ['class' => 'blog-menu__item']) ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="blog-header__logo">
                    <a href="/">
                        <img src="/img/mir-logo.png">
                    </a>
                </div>
                <div class="header-page__name">Блог МИРа</div>
                <div class="blog-header__search">
                    <?= Html::beginForm(Url::to(['/blog/news/index']), 'get') ?>
                        <div class="search-block">
                            <?= Html::activeTextInput($search, 'search', ['class' => 'header-search__input']) ?>
                        </div>
                    <?= Html::endForm() ?>
                </div>
                <a href=" https://mirumir24.ru/home#2" target="_blank" class="blog-header__text">
                    Стань автором и зарабатывай - участвуй в конкурсе!
                </a>
            </div>
        </div>

        
    </div>
</div>
