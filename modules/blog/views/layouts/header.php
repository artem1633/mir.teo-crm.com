<?php

use yii\helpers\Html;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top" style="min-height: unset; height: 41px;">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header">
                    <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                        <span class="navbar-logo"></span>
                        <?=Yii::$app->name?>
                    </a>
                    <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- end mobile sidebar expand / collapse button -->
                <ul class="header-menu">
                    <li><?= Html::a('Купоны', '#') ?></li>
                    <li><?= Html::a('Карта', '#') ?></li>
                    <li><?= Html::a('Тендеры', '#') ?></li>
                    <li><?= Html::a('Маркетплейс', '#') ?></li>
                    <li><?= Html::a('Доска', '#') ?></li>
                    <li><?= Html::a('Прокат', '#') ?></li>
                    <li><?= Html::a('Билеты', '#') ?></li>
                    <li><?= Html::a('Новости', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Погода', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('ТВ', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Радио', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Кино', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Игра', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Работа', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Знакоства', '#') ?></li>
                    <li class="mdsm-hidden"><?= Html::a('Офис', '#') ?></li>
                    <li><?= Html::a('Ещё', '#') ?></li>
                </ul>
                <?php if(Yii::$app->user->isGuest == false): ?>
                    <!-- begin header navigation right -->
                    <ul class="nav navbar-nav navbar-right" style="
    background: #f0da58;
    height: 41px;">
                        <li class="dropdown navbar-user">
                            <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" style="margin-top: -7px;">
                                <img src="/<?= Yii::$app->user->identity->getRealAvatarPath() ?>" data-role="avatar-view" alt="">
                                <?php
                                    $login = Yii::$app->user->identity->login;

                                    if(strlen($login) > 15){
                                        $login = iconv_substr ($login, 0, 13, "UTF-8").'...';
                                    }
                                ?>
                                <span class="hidden-xs"><?= $login?></span> <b class="caret"></b>
                            </a>
                            <div style="text-align: right;">
                                <ul class="dropdown-menu animated fadeInLeft">
									<?php if (Yii::$app->user->identity->login === 'admin'): ?>
                                    	<li> <?= Html::a('Раздел администратора', ['/admin/blog/index']) ?> </li>
									<?php endif; ?>
                                    <li> <?= Html::a('Счет', ['/accruals/index']) ?> </li>
                                    <li> <?= Html::a('Профиль', ['/user/profile']) ?> </li>
                                    	<li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
<!--                    <ul class="nav navbar-nav navbar-right">-->
<!--                        <li class="dropdown navbar-user" style="margin-top: 12px;">-->
<!--                            <p>--><?php //echo Yii::$app->user->identity->selfRefHref;?><!--</p>-->
<!--                        </li>-->
<!--                    </ul>-->
                    <!-- end header navigation right -->
                <?php endif; ?>
            </div>
            <!-- end container-fluid -->
        </div>