<div class="blue_menu">
    <div class="menu_und">  
        <a class="btn_menu_desc" href="#blue_menu_top">
            <div class="arrow_top"></div>
        </a>
        <a class="btn_menu_desc" href="#blue_menu_bottom">
            <div class="arrow_bottom"></div>
        </a>
        <div class="menu_carousel blue_menu_inner carousel owl-carousel owl-theme owl-loaded">
            <a href="/"> <div id="home_page" class="menu_img" ><img src="/img/blue_menu/1.png" alt="" title="Главная страница"></div></a>

            <a href="/info"> <div id="info_page" class="menu_img"><img src="/img/blue_menu/2.png" alt="" title="Информационный сайт"></div></a>
            <a href="/contests"> <div id="contests_page" class="menu_img"><img src="/img/blue_menu/cubok_blue.png" alt="" title="Конкурсы"></div></a>
             <!--<a href="https://zen.yandex.ru/id/5ea04f674f5630594cd75c14" target="_blank"><div id="digest_page" class="menu_img"><img src="/img/blue_menu/3.png" alt="" title="Дзен-канал МИРа"></div></a>-->
            <a href="/blog/news"> <div id="blog_page" class="menu_img"><img src="/img/blue_menu/4.png" alt="" title="Блог МИРа"></div></a>
            <a id="news_pages" href="/news"><div id="news_page" class="menu_img"><img src="/img/blue_menu/5.png" alt="" title="Новости">                
                </div></a>
            <a href="/weather"><div id="weather_page" class="menu_img"><img src="/img/blue_menu/6.png" alt="" title="Погода"></div></a>
            <a href="/tv"><div id="tv_page" class="menu_img"><img src="/img/blue_menu/7.png" alt="" title="ТВ"></div></a>
            <a href="/radio"><div id="radio_page" class="menu_img"><img src="/img/blue_menu/8.png" alt="" title="Радио"></div></a>
            <a href="/movies"><div id="movies_page" class="menu_img"><img src="/img/blue_menu/9.png" alt="" title="Кино и сериалы"></div></a>
            <a href="/games"><div id="games_page" class="menu_img"><img src="/img/blue_menu/10.png" alt="" title="Игры"></div></a>
            <a href="/construction"><div id="victorina_img" class="menu_img"><img style="margin-left: 1px;" src="/img/blue_menu/victorina_blue.png" alt="" title="Викторины"></div></a>
            <a href="/academy">  <div id="academy_page" class="menu_img"><img src="/img/blue_menu/12.png" alt="" title="Академия"></div></a>
            <a href="/construction"> <div id="dating_page" class="menu_img"><img src="/img/blue_menu/11.png" alt="" title="Знакомства"></div></a>

            <a href="/construction"> <div id="work_page" class="menu_img"><img src="/img/blue_menu/13.png" alt="" title="Работа"></div></a>
            <a href="/construction"> <div id="ads_page" class="menu_img"><img src="/img/blue_menu/14.png" alt="" title="Объявления"></div></a>
            <a href="/construction">  <div id="shop_page" class="menu_img"><img src="/img/blue_menu/19.png" alt="" title="Маркетплейс"></div></a>
            <a href="/construction">  <div id="servise_page" class="menu_img"><img src="/img/blue_menu/tools_blue.png" alt="" title="Услуги"></div></a>
            <a href="/construction">  <div id="auto_page" class="menu_img"><img src="/img/blue_menu/car_blue.png" alt="" title="Автомото"></div></a>
            <a href="/construction">  <div id="children_page" class="menu_img"><img src="/img/blue_menu/children_blue.png" alt="" title="Детский мир"></div></a>
            <a href="/construction"> <div id="eat_page" class="menu_img"><img src="/img/blue_menu/24.png" alt="" title="Питание"></div></a>

            <a href="/construction"> <div id="ticket_page" class="menu_img"><img src="/img/blue_menu/21.png" alt="" title="Путешествия"></div></a>
            <a href="/construction"> <div id="gift_page" class="menu_img"><img src="/img/blue_menu/gift_blue.png" alt="" title="Подарки"></div></a>
            <a href="/souvenirs"> <div id="souvenirs_page" class="menu_img"><img src="/img/blue_menu/cyveniru_blue.png" alt="" title="Сувениры"></div></a>
            <a href="/construction"> <div id="taxi_page" class="menu_img"><img src="/img/blue_menu/takci_blue.png" alt="" title="Такси"></div></a>



<!-- <a href="/construction"> <div class="menu_img"><img src="/img/blue_menu/15.png" alt="" title="ВИП-клуб"></div></a>
 <a href="/construction">  <div class="menu_img"><img src="/img/blue_menu/16.png" alt="" title="Профсоюз"></div></a>
 <a href="/construction"> <div class="menu_img"><img src="/img/blue_menu/18.png" alt="" title="Тендеры"></div></a>
<a href="/construction"> <div class="menu_img"><img src="/img/blue_menu/17.png" alt="" title="Купоны"></div></a>
<a href="/construction"> <div class="menu_img"><img src="/img/blue_menu/23.png" alt="" title="Прямые покупки"></div></a>
<a href="/construction"> <div class="menu_img"><img src="/img/blue_menu/20.png" alt="" title="Прокат"></div></a>
<a href="/construction"> <div class="menu_img"><img src="/img/blue_menu/25.png" alt="" title="Доставка еды"></div></a>-->





        </div>
    </div>
</div>


<div class="menu_news menu-news__sidebar">
    <div>Новости</div>
    <ul>
        <a href="/news/az"><li><img src="/img/country/1.png" alt="">Азербайджан</li></a>
        <a href="/news/am"><li><img src="/img/country/2.png" alt="">Армения</li></a>
        <a href="/news/by"><li><img src="/img/country/3.png" alt="">Беларусь</li></a>
        <a href="/news/ge"><li><img src="/img/country/4.png" alt="">Грузия</li></a>
        <a href="/news/kaz"><li><img src="/img/country/5.png" alt="">Казахстан</li></a>
        <a href="/news/kg"><li><img src="/img/country/6.png" alt="">Киргизия</li></a>
        <a href="/news/lv"><li><img src="/img/country/7.png" alt="">Латвия</li></a>
        <a href="/news/lt"><li><img src="/img/country/8.png" alt="">Литва</li></a>
        <a href="/news/md"><li><img src="/img/country/9.png" alt="">Молдова</li></a>
        <a href="/news/"><li><img src="/img/country/10.png" alt="">Россия</li></a>
        <a href="/news/tj"><li><img src="/img/country/11.png" alt="">Таджикистан</li></a>
        <a href="/news/tm"><li><img src="/img/country/12.png" alt="">Туркменистан</li></a>
        <a href="/news/uz"><li><img src="/img/country/13.png" alt="">Узбекистан</li></a>
        <a href="/news/ua"><li><img src="/img/country/14.png" alt="">Украина</li></a>
        <a href="/news/ee"><li><img src="/img/country/15.png" alt="">Эстония</li></a>
        <a href="/news/cn"><li><img src="/img/country/16.png" alt="">China</li></a>
        <a href="/news/fr"><li><img src="/img/country/17.png" alt="">France</li></a>
        <a href="/news/de"><li><img src="/img/country/18.png" alt="">Germany</li></a>
        <a href="/news/in"><li><img src="/img/country/19.png" alt="">India</li></a>
        <a href="/news/it"><li><img src="/img/country/20.png" alt="">Italy</li></a>
        <a href="/news/es"><li><img src="/img/country/21.png" alt="">Spain</li></a>
        <a href="/news/usa"><li><img src="/img/country/22.png" alt="">USA</li></a>
        <a href="/news/uk"><li><img src="/img/country/23.png" alt="">Inited Kingdom</li></a>
    </ul>
</div>

<script>
       $( document ).ready(function(){
     $(window).on('resize', function(){  
            if ($(window).height() <= 684  && $(window).width() <= 769){
                
                if($(window).width() <= 769){   
                    var h = $(window).height();    

                    h = h - 80;        
                 $( ".menu_news" ).css( "height", h );
                }
                if ($(window).width() <= 599) {
                    var h = $(window).height();    

                     h = h - 100; 

                   $( ".menu_news" ).css( "height", h ); 
                }                 
            }else{
                if($(window).height() <= 604 ){
                     $( ".menu_news" ).css( "height", "100%" );
                }else{
                $( ".menu_news" ).css( "height", "inherit" );}
            }                   
        }).trigger('resize');
    });

    $( document ).ready(function(){
    $(window).on('resize', function(){
        if($(window).width() >= 679){
      $( ".menu_news" ).hover(function(){  
        $(".menu_news").css( "display", "block" );
        }, function(){ 
        $(".menu_news").css( "display", "none" );
      });
      $( "#news_pages" ).hover(function(){  
        $( ".menu_news.menu-news__sidebar" ).css( "display", "block" );
        }, function(){ 
        $( ".menu_news.menu-news__sidebar" ).css( "display", "none" );
      });
  }else{
    
    $("a#news_pages").click(function(){    

        if($( ".menu_news.menu-news__sidebar" ).css( "display") == "block"){
            $( ".menu_news.menu-news__sidebar" ).css( "display", "none" );
        }else{            
        $( ".menu_news.menu-news__sidebar" ).css( "display", "block" );
        }
        return false;
    });   
        
  }
    }).trigger('resize');    
    });
    
    $(document).ready(function(){
         $('.menu_img').each(function() {
            $(this).removeClass("active_img");  
            $(this).removeClass("active_img_right");             
        });
        var page_loc=document.location.pathname;
        switch (page_loc) {
            case '/':
                $("#home_page img").attr('src', '/img/blue_menu/1_1.png');
                $("#home_page").addClass("active_img_new"); 
            break;
            case '/info':
                $("#info_page img").attr('src', '/img/blue_menu/2_1.png');
                $("#info_page").addClass("active_img_new"); 
            break;
            case '/digest':
                $("#digest_page img").attr('src', '/img/blue_menu/3_1.png');
                $("#digest_page").addClass("active_img_new");  
            break;
            case '/news':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;     
            case '/news/az':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;  
            case '/news/usa':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break; 
            case '/news/am':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;   
            case '/news/by':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;  
            case '/news/kaz':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;  
            case '/news/kg':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;  
            case '/news/lv':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;  
            case '/news/lt':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break; 
            case '/news/md':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;  
            case '/news/tj':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;   
            case '/news/tm':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;  
            case '/news/uz':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;  
            case '/news/ua':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;  
            case '/news/ee':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;  
            case '/news/cn':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;  
            case '/news/fr':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;
            case '/news/de':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;  
            case '/news/it':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;  
            case '/news/uk':
                $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;  
            case '/news/es':
                 $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");   
            break;  
            case '/news/in':
               $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;    
            case '/news/ge':
               $("#news_page img").attr('src', '/img/blue_menu/5_1.png');
                $("#news_page").addClass("active_img_new");  
            break;  
             case '/weather':
                 $("#weather_page img").attr('src', '/img/blue_menu/6_1.png');
                $("#weather_page").addClass("active_img_new");   
            break;   
             case '/site/profile':
                 $("#user-profile img").attr('src', '/img/orange_menu/4_1.png');
                $("#user-profile").addClass("active_img_new");   
            break;  
             case '/tv':
             $(window).on('resize', function(){
                
                if ($(window).width() < 355) {
                     $( ".blue_menu .owl-stage" ).css( "transform", "translate3d(-140px, 0px, 0px)")    
                }
                }).trigger('resize');
                $("#tv_page img").attr('src', '/img/blue_menu/7_1.png');
                $("#tv_page").addClass("active_img_new");
            break; 
             case '/radio':
              $(window).on('resize', function(){                
                
                if ($(window).width() < 355) {
                     $( ".blue_menu .owl-stage" ).css( "transform", "translate3d(-140px, 0px, 0px)")    
                }
                }).trigger('resize');
                $("#radio_page img").attr('src', '/img/blue_menu/8_1.png');
                $("#radio_page").addClass("active_img_new");
            break; 
             case '/movies':
             $(window).on('resize', function(){
                
                if ($(window).width() < 409) {
                     $( ".blue_menu .owl-stage" ).css( "transform", "translate3d(-94px, 0px, 0px)")    
                }
                if ($(window).width() < 355) {
                     $( ".blue_menu .owl-stage" ).css( "transform", "translate3d(-140px, 0px, 0px)")    
                }                
                }).trigger('resize');
                $("#movies_page img").attr('src', '/img/blue_menu/9_1.png');
                $("#movies_page").addClass("active_img_new");
            break; 
             case '/games':
             $(window).on('resize', function(){
                if ($(window).width() < 463) {
                     $( ".blue_menu .owl-stage" ).css( "transform", "translate3d(-47px, 0px, 0px)")    
                }
                if ($(window).width() < 409) {
                     $( ".blue_menu .owl-stage" ).css( "transform", "translate3d(-94px, 0px, 0px)")    
                }
                if ($(window).width() < 355) {
                     $( ".blue_menu .owl-stage" ).css( "transform", "translate3d(-140px, 0px, 0px)")    
                }                
                }).trigger('resize');
                $("#games_page img").attr('src', '/img/blue_menu/10_1.png');
                $("#games_page").addClass("active_img_new");
            break; 
            case '/academy':                
                $("#academy_page img").attr('src', '/img/blue_menu/12_1.png');
                $("#academy_page").addClass("active_img_new");
            break;
            case '/contests':                
                $("#contests_page img").attr('src', '/img/blue_menu/cubok_white.png');
                $("#contests_page").addClass("active_img_new");
            break;
            case '/souvenirs':                
                $("#souvenirs_page img").attr('src', '/img/blue_menu/cyveniru_white.png');
                $("#souvenirs_page").addClass("active_img_new");
            break;
             case '/rod':                
                $("#dashboard_page img").attr('src', '/img/orange_menu/3_1.png');
                $("#dashboard_page").addClass("active_img_new");
            break;  
            case '/dashboard/':                
                $("#dashboard_page img").attr('src', '/img/orange_menu/3_1.png');
                $("#dashboard_page").addClass("active_img_new");
            break;  
            case '/dashboard/index':                
                $("#dashboard_page img").attr('src', '/img/orange_menu/3_1.png');
                $("#dashboard_page").addClass("active_img_new");
            break;  
            case '/dashboard/parent':                
                $("#dashboard_page img").attr('src', '/img/orange_menu/3_1.png');
                $("#dashboard_page").addClass("active_img_new");
            break;  
             case '/dashboard/lider':                
                $("#dashboard_page img").attr('src', '/img/orange_menu/3_1.png');
                $("#dashboard_page").addClass("active_img_new");
            break; 
            case '/dashboard/loser':                
                $("#dashboard_page img").attr('src', '/img/orange_menu/3_1.png');
                $("#dashboard_page").addClass("active_img_new");
            break;
          default:
            
        }
    });
</script>