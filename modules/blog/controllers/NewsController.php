<?php

namespace app\modules\blog\controllers;

use app\models\Comment;
use app\models\NewsComment;
use app\models\NewsLike;
use app\modules\blog\models\forms\CommentForm;
use app\modules\blog\models\forms\NewsSearchForm;
use app\modules\blog\models\Rubric;
use app\modules\blog\models\RubricNews;
use app\modules\blog\models\RubricSearch;
use Yii;
use app\modules\blog\models\News;
use app\modules\blog\models\NewsSearch;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller{

    /**
     * @inheritdoc
     */
    public function behaviors(){

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     * @param $action
     * @return bool|Response
     */
    public function beforeAction($action){

        $model = new NewsSearchForm();
        if($model->load(Yii::$app->request->get()) && $model->validate()){
            $search = Html::encode($model->search);
            return $search ? $this->redirect(['/blog/news/index', 'search' => $search]) : $this->redirect(['/blog/news/index']);
        }
        return true;
    }

    /**
     * Lists all News models.
     *
     * @param string $rubric слаг рубрики
     * @param string $search поисковый запрос
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex($rubric = null, $search = null){

        $rubrics = Rubric::find()->all();
        $Rubric = null;
        if($rubric){
            if(!$Rubric = Rubric::findOne(['slug' => $rubric])){
                throw new NotFoundHttpException('Страница не найдена');
            }
        }
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $Rubric);
        $news_count = $searchModel->search(Yii::$app->request->queryParams, $Rubric, true);

        $modelsPks = ArrayHelper::getColumn($dataProvider->models, 'id');

        $rightDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $rightDataProvider->query->orderBy('date_published desc');
        $rightDataProvider->query->andWhere(['!=', 'main_news', 1]);
        $rightDataProvider->query->andWhere(['status_published' => 1]);
        $rightDataProvider->query->offset(4);
        $rightDataProvider->query->limit(9);
        $rightDataProvider->query->andWhere(['not in', 'id', $modelsPks]);

//        $expressDataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $expressDataProvider->query->orderBy('date_published desc');
//        $expressDataProvider->query->andWhere(['status_published' => 1]);
//        $expressDataProvider->query->andWhere(['is_express' => 1]);
//        $expressDataProvider->query->limit(3);

        $expressNews = News::find()->where(['status_published' => 1, 'is_express' => 1])->orderBy('date_published desc')->limit(3)->all();
        $expressDataProvider = new ArrayDataProvider(['models' => $expressNews]);

//        $popularDataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $popularDataProvider->query->orderBy('count_news desc');
//        $popularDataProvider->query->andWhere(['status_published' => 1]);
//        $popularDataProvider->pagination->limit = 3;

        $popularNews = News::find()->where(['status_published' => 1, 'is_express' => 1])->orderBy('date_published desc')->limit(3)->all();
        $popularDataProvider = new ArrayDataProvider(['models' => $popularNews]);

        $importantNews = News::find()->where(['status_published' => 1, 'is_important' => 1])->orderBy('date_published desc')->limit(3)->all();
        $importantDataProvider = new ArrayDataProvider(['models' => $popularNews]);

        $mainNew = News::findOneMain();
        $mainRubrics = ArrayHelper::index(Rubric::findAll(['id' => [2, 3, 4]]), 'id');

        return $this->render('index', compact('rubrics', 'search', 'searchModel', 'dataProvider', 'rightDataProvider', 'expressDataProvider', 'popularDataProvider', 'importantDataProvider', 'mainNew', 'Rubric', 'news_count', 'mainRubrics'));
    }

    public function actionModeration()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['or', ['status_published' => 0], ['status_published' => null]]);

        return $this->render('moderate', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param string $title
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($title){

        if(!$model = News::find()->where(['english_title' => $title])->one()){
            throw new NotFoundHttpException('Страница не найдена');
        }
        $searchModel = new NewsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->orderBy('date_published desc');
//        $dataProvider->query->andWhere(['!=', 'main_news', 1]);
//        $dataProvider->query->andWhere(['status_published' => 1]);
//        $dataProvider->query->andWhere(['!=', 'id', $model->id]);
//        $dataProvider->query->limit(9);
//        $dataProvider->pagination = false;

        $model->updateCountView();
        $rubrics = RubricNews::find()->where(['news_id' => $model->id])->all();
        $comments_query = NewsComment::find()->where(['new_id' => $model->id])->orderBy('created_at desc');
        if(!Yii::$app->request->post('comment_limit')){
            $comments_query->limit(3);
        }
        $comments = $comments_query->all();
        $comments_count = NewsComment::find()->where(['new_id' => $model->id])->count();
        $commentForm = new CommentForm();
        if($commentForm->load(Yii::$app->request->post()) && !Yii::$app->request->post('comment_limit')){
            if($commentForm->validate()){
                $commentForm->saveComment($model);
            }else{
                Yii::error(print_r($commentForm->getErrors(), true));
            }
            return $this->redirect(['view', 'title' => $title]);
        }
        return $this->render('view', compact('model', 'searchModel', 'rubrics', 'comments', 'comments_count', 'commentForm'));
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionComments($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $comments = NewsComment::find()->where(['new_id' => $id])->all();

        return $comments;
    }

    /**
     * @return array
     */
    public function actionAddComment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new NewsComment();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return ['success' => true];
        } else {
            return ['success' => false, 'errors' => $model->errors];
        }
    }

    /**
     * Добавление/удаление лайка
     *
     * @param integer $id
     * @return int|string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAddLike($id){

        if(Yii::$app->request->isAjax){
            if(!News::find()->where(['id' => $id])->exists()){
                throw new NotFoundHttpException('Страница не найдена');
            }
            if($Like = NewsLike::find()->where(['new_id' => $id, 'user_id' => Yii::$app->user->identity->getId()])->one()){
                $Like->delete();
                return NewsLike::find()->where(['new_id' => $id])->count();
            }else{
                $Like = new NewsLike();
                $Like->new_id = $id;
                $Like->user_id = Yii::$app->user->identity->getId();
                if($Like->save()){
                    return NewsLike::find()->where(['new_id' => $id])->count();
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdateComment($id){

        if(!$model = NewsComment::findOne(['id' => $id])){
            throw new NotFoundHttpException('Страница не найдена');
        }
        if($comment = Yii::$app->request->post('comment')){
            $model->content = $comment;
            if(!$model->save()){
                Yii::error(print_r($model->getErrors(), true));
                return false;
            }
            return $model->content;
        }
    }

    /**
     * @param int $id
     * @return false|int
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteComment($id){

        if(!$model = NewsComment::findOne(['id' => $id])){
            throw new NotFoundHttpException('Страница не найдена');
        }
        return $model->delete();
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $rubric = new Rubric();
        $rubricItems = RubricSearch::getAllRubric();
        $rubricNews = new RubricNews();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'title' => $model->english_title]);
        }

        return $this->render('create', [
            'model' => $model,
            'rubric' => $rubric,
            'rubricItems' => $rubricItems
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $rubric = new Rubric();
        $rubricItems = RubricSearch::getAllRubric();
        $rubricNews = new RubricNews();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'title' => $model->english_title]);
        }

        return $this->render('update', [
            'model' => $model,
            'rubric' => $rubric,
            'rubricItems' => $rubricItems
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPublish($id)
    {
        $model = $this->findModel($id);

        $model->status_published = 1;
        $model->date_published = time();

        $model->save(false);

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
