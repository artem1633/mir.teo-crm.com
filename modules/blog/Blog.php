<?php

namespace app\modules\blog;


use yii\base\Module;
use Yii;

class Blog extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\blog\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}