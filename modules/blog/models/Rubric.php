<?php

namespace app\modules\blog\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "rubric".
 *
 * @property int $id
 * @property string $name Наименование рубрики
 * @property string $slug Слаг
 * @property integer $type Тип
 *
 * @property RubricNews[] $rubricNews
 */
class Rubric extends \yii\db\ActiveRecord{

    const TYPE_FIXED = 1;
    const TYPE_ALPHABETICAL = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubric';
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'string', 'max' => 255],
            ['type', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование рубрики',
            'slug' => 'Слаг',
            'type' => 'Тип'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubricNews()
    {
        return $this->hasMany(RubricNews::class, ['rubric_id' => 'id']);
    }
}
