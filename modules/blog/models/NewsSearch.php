<?php

namespace app\modules\blog\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\blog\models\News;
use yii\helpers\ArrayHelper;

/**
 * NewsSearch represents the model behind the search form of `app\modules\blog\models\News`.
 *
 * @property string $rubric
 * @property string $_search
 * @property integer $_limit
 */
class NewsSearch extends News{

    public $rubric;

    private $_search;
    private $_limit;

    /**
     * @inheritDoc
     * @return void
     */
    public function init(){

        $this->_limit = Yii::$app->request->post('limit');
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules(){

        return [
            [['id', 'date_published', 'status_published', 'main_news', 'count_news', 'rubric'], 'integer'],
            [['title', 'english_title', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param Rubric $Rubric
     *
     * @return ActiveDataProvider|integer
     */
    public function search($params, $Rubric = null, $count = false){

        $query = News::find()
            ->innerJoinWith('rubricNews')
            ->orderBy(['date_published' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $this->_search = Yii::$app->request->get('search');

        if($Rubric){
            if($Rubric->id == 2){   //если рубрика "Самые срочные"
                $query->where(['is_express' => 1]);
            }elseif($Rubric->id == 4){  //если рубрика "Самые важные"
                $query->where(['is_important' => 1]);
            }elseif($Rubric->id != 1){
                $query->where(['rubric_news.rubric_id' => $Rubric->id]);
            }
        }
        $query->andWhere(['!=', 'main_news', 1])
            ->andWhere(['status_published' => 1]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'english_title', $this->english_title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'title', isset($_GET['search']) ? $_GET['search'] : null]);

        if($this->rubric != null){
            $rubricsPks = ArrayHelper::getColumn(RubricNews::find()->where(['rubric_id' => $this->rubric])->all(), 'news_id');

            $query->andFilterWhere(['id' => $rubricsPks]);
        }

        if(!$this->_search){
            $query->limit($this->_limit ? $this->_limit : 5);
        }

        if($this->_search){
            $query->orWhere(['like', 'title', $this->_search])
                ->orWhere(['like', 'content', $this->_search]);
        }

        if($count){
            return $query->count();
        }
        return $dataProvider;
    }
}
