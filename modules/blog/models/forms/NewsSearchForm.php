<?php

namespace app\modules\blog\models\forms;

use Yii;
use yii\base\Model;

class NewsSearchForm extends Model{

    public $search;

    /**
     * NewsSearchForm constructor.
     *
     * @inheritDoc
     * @param array $config
     */
    public function __construct($config = []){

        if(Yii::$app->request->get('search')){
            $this->search = Yii::$app->request->get('search');
        }
        parent::__construct($config);
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function rules(){

        return [
            ['search', 'string']
        ];
    }
}