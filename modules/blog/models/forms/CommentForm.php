<?php

namespace app\modules\blog\models\forms;

use Yii;
use app\models\NewsComment;
use yii\base\Model;

/**
 * Class CommentForm
 *
 * @package app\modules\blog\models
 *
 * @property $comment комментарий
 */

class CommentForm extends Model{

    public $comment;

    /**
     * @inheritDoc
     * @return array
     */
    public function rules(){

        return [
            ['comment', 'required'],
            ['comment', 'trim'],
            ['comment', 'string'],
        ];
    }

    /**
     * Сохранение комментария
     *
     * @param News $model
     * @return bool
     */
    public function saveComment($model){

        $Comment = new NewsComment();
        $Comment->new_id = $model->id;
        $Comment->content = $this->comment;
        $Comment->author_id = Yii::$app->user->getId();
        if(!$Comment->save()){
            Yii::error(print_r($Comment->getErrors(), true));
            return false;
        }
        return true;
    }
}