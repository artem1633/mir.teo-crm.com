<?php

namespace app\controllers;

use app\models\Currency;
use app\models\TransferSearch;
use app\models\User;
use Yii;
use app\models\forms\MoneyTransactionForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\widgets\ActiveForm;

class ProfileController extends Controller{

    /**
     * @inheritDoc
     * @return array
     */
    public function behaviors(){

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-profile-info' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Страница "Баланс"
     *
     * @return string|\yii\web\Response
     */
    public function actionBalance($type = null){

        $currencies = Currency::find()->all();
        $searchModel = new TransferSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());
        $count_balance_records = $searchModel->search(Yii::$app->request->post(), true);
        $balance = $searchModel->getBalance();
        $model = new MoneyTransactionForm();
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            return $this->asJson(ActiveForm::validate($model));
        }
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                $model->saveData();
            }else{
                Yii::error(print_r($model->getErrors(), true));
            }
            return $this->refresh();
        }
        return $this->render('balance', compact('type', 'model', 'currencies', 'searchModel', 'dataProvider', 'count_balance_records', 'balance'));
    }

    /**
     * Карточка пользователя при вводе урл профиля в форме
     *
     * @return bool|string|\yii\web\Response
     */
    public function actionGetProfileInfo(){

        if(Yii::$app->request->isAjax){
            $url = Yii::$app->request->post('url');
            if($url && preg_match('/id=\d+$/', $url)){
                $explode_url = explode('=', $url);
                if($User = User::findOne(['id' => end($explode_url)])){
                    return $this->renderPartial('_profile_info_card', compact('User'));
                }
            }
            return false;
        }
        return $this->redirect(['balance']);
    }
}