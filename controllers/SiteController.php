<?php

namespace app\controllers;

use app\helpers\TextHelper;
use app\models\forms\EditAdditionalSetting;
use app\models\forms\EditMainSetting;
use app\models\forms\ForgetPassword;
use app\models\forms\ResetPasswordForm;
use app\models\forms\SignupForm;
use app\models\User;
use app\models\UserLike;
use app\models\UserVisits;
use Google_Client;
use Google_Service_Sheets;
use SendGrid\Mail\Mail;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;
use yii\helpers\VarDumper;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @param string $s
     * @return string
     */
    public function actionSource($s)
    {
        return $this->render('source', [
            's' => $s
        ]);
    }

    public function actionTest()
    {
        $googleAccountKeyFilePath = __DIR__ . '/../web/credentials.json';
//        $googleAccountKeyFilePath = __DIR__ . '/assets/my-project-test1-fdd689d70f55.json';
        putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );

        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();

        $client->addScope( 'https://www.googleapis.com/auth/spreadsheets' );

        $service = new Google_Service_Sheets( $client );
        $spreadsheetId = '15ZxFxD3Eb_3HlR3DFhxPS08C4pgKM7J-zp6n8SJw_CQ';

        $response = $service->spreadsheets->get($spreadsheetId);

// Свойства таблицы
        $spreadsheetProperties = $response->getProperties();
        $spreadsheetProperties->title; // Название таблицы

        foreach ($response->getSheets() as $sheet) {

            // Свойства листа
            $sheetProperties = $sheet->getProperties();
            $sheetProperties->title; // Название листа

            $gridProperties = $sheetProperties->getGridProperties();
            echo $gridProperties->columnCount; // Количество колонок
            echo ' '.$gridProperties->rowCount; // Количество строк
            echo '<br>';
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * @param string $h
     * @return mixed
     */
    public function actionApproveEmail($h){
        $user = User::findOne(['password_hash' => $h]);

        $user->email_approved = 1;

        $user->save(false);

        Yii::$app->session->setFlash('success', 'Email подтвержден, пожалуйста пройдите авторизацию');

        $curl = curl_init();
        $data = [
            'name' => $user->name,
            'living' => $user->living_place,
            'email' => $user->login,
            'phone' => $user->phone,
            'messager_whats_app' => $user->messager_whats_app,
            'messager_viber' => $user->messager_viber,
            'messager_telegram' => $user->messager_telegram,
            'messager_none' => $user->messager_none,
            'interest_aristocratic' => $user->interest_aristocratic,
            'interest_services' => $user->interest_services,
            'interest_part' => $user->interest_part,
            'interest_intercom' => $user->interest_intercom,
            'interest_help' => $user->interest_help,
            'source' => 'Регистрация'
        ];
        curl_setopt($curl, CURLOPT_URL, 'http://crm.miruwir.info/api/client/create');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        $out = curl_exec($curl);
        \Yii::warning($out, 'Send new client to API');
        curl_close($curl);

        return $this->redirect(['site/login']);
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/site/profile');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionForgetPassword()
    {
        $this->layout = '@app/views/layouts/main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ForgetPassword();
        if ($model->load(Yii::$app->request->post()) && $model->sendNewPassword()) {
            Yii::$app->session->setFlash('success', 'На почту отправлен новый пароль');
            return $this->goBack();
        }
        return $this->render('forget-password', [
            'model' => $model,
        ]);
    }

    /**
     * Register action.
     *
     * @return string
     */
    public function actionRegister()
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'На ваш email отправлена ссылка для подтверждения регистрации');
            (new LoginForm(['username' => $model->email, 'password' => $model->password]))->login();
            return $this->goHome();
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if($action->id == 'register'){
            Yii::$app->request->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }


    public function actionUserlike()
    {

        if(\Yii::$app->request->isAjax) {

            $data = \Yii::$app->request->post();
            $data = $data['user_to'];

            $idthiuser = Yii::$app->user->getId();

            $userlike = new UserLike();
            $userlike->user_id = $idthiuser;
            $userlike->user_profile_id = $data;
            $userlike->save(false);

        }
    }
    
    public function actionAccount($id) {

        $id = Yii::$app->request->get('id');
        $profile =  User::find()->where(['id'=>$id])->one();
        $countlike = UserLike::find()->where(['user_profile_id'=>$id])->count();
        $idthiuser = Yii::$app->user->getId();
        $session = Yii::$app->session;
        $item = explode(' ',$session['visits_ses']);
        $yes = in_array($id,$item);

        $visi = UserVisits::find()->where(['user_id'=>$id])->one();

        $dig = $profile['aristocrat_dignity1'];
        $thisdate = null;
        if ($profile->birth_date != null) {
            $date = date_create_from_format('Y-m-d', $profile->birth_date);
            $thisdate = date_format($date, "d-m-Y");
        }

        if ($yes == false)
        {
            if ($visi!=null)
            {
                $visi->counter = $visi->counter++;
                $visi->update(false);
            }
            else
            {
                $visi = new UserVisits();
                $visi->user_id = $id;
                $visi->counter = 1;
                $visi->save(false);
            }

            if ($session['visits_ses'] !=null)
            {
                $session['visits_ses'] = $session['visits_ses']. ' '.$id;
            }
            else
            {
                Yii::$app->session['visits_ses'] = $id;

            }
        }
if(Yii::$app->user->isGuest){}else{

   $tmp_user = Yii::$app->user->id;
    if (User::findOne(['id' => $tmp_user])){
        $models_tmp = UserVisits::findOne(['user_id' => $tmp_user]);
        $models_tmp->counter++;
        $models_tmp->save();
    } 
}

        $islike1 = UserLike::find()->where(['user_profile_id'=>$id])->andWhere(['user_id' =>$idthiuser])->one();
        return $this->render('/site/account',compact('id','profile','countlike','islike1','visi','dig','thisdate'));
    }
    
    public function actionProfile() {


        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }

            $model = new EditMainSetting();
            $user = Yii::$app->user->identity;

            $countlike = UserLike::find()->where(['user_profile_id'=>$user->getId()])->count();

            if ($view = UserVisits::find()->where(['user_id'=>$user->getId()])->one())
                $view1 = $view->counter;
            else
                $view1 = 0;

            if ($model->load(Yii::$app->request->post()) && $model->editmain($user->id)) {
                Yii::$app->session->setFlash('success', 'Данные изменены');
                return $this->refresh();
            }

            $model2 = new EditAdditionalSetting();

            if ($model2->load(Yii::$app->request->post())){

                if($model2->totem_code == '' && $model2->totem == '' || $model2->totem_code == '10110' && $model2->totem == 'Маршал' || $model2->totem_code == '10228' && $model2->totem == 'Генерал' || $model2->totem_code == '10337' && $model2->totem == 'Подполковник' || $model2->totem_code == '10424' && $model2->totem == 'Полковник' || $model2->totem_code == '10578' && $model2->totem == 'Майор' || $model2->totem_code == '10687' && $model2->totem == 'Капитан' || $model2->totem_code == '10755' && $model2->totem == 'Лейтенант' || $model2->totem_code == '10827' && $model2->totem == 'Прапорщик' || $model2->totem_code == '10965' && $model2->totem == 'Сержант' || $model2->totem_code == '10122' && $model2->totem == 'Рядовой'){
                    if($model2->editadditional($user->id)){
                        Yii::$app->session->setFlash('success', 'Данные изменены');
                        return $this->refresh();
                    }
                    
                }else{
                    $model2->TotemCodeValidatorAlert( 'totem_code');
                }                
            }

            return $this->render('/site/profile',compact('model','user','model2','countlike','view1'));


    }
}
