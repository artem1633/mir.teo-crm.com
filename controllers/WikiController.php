<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use Yii;
use app\models\Faq;
use app\models\FaqSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use app\models\Wiki;
use app\models\User;
use yii\helpers\Html;
use yii\web\UploadedFile;

class WikiController extends Controller {
   

        
    public function actionIndex()
    {

        if (Yii::$app->user->isGuest) {
           return $this->redirect('/site/login');
        }

        $tmp_user_id = Yii::$app->user->id;

        $model = Wiki::findOne(['user_id' => $tmp_user_id]);
        if (!$model){
            $model = new Wiki();
            $model->user_id = $tmp_user_id;
            $model->save();
            $model = Wiki::findOne(['user_id' => $tmp_user_id]);
        }

        $modeltmp = Wiki::findOne(['user_id' => $tmp_user_id]);

        // если пришли post-данные...
        if ($model->load(Yii::$app->request->post())) {

           
            if($model->photo = UploadedFile::getInstance($model, 'photo')){ 
            $model->photo = UploadedFile::getInstance($model, 'photo');               
                if ($model->upload()) {
                $model->photo = $model->photo->baseName . '.' . $model->photo->extension;              
                    
                
                // file is uploaded successfully  \
                if ($model->save()) {
                // данные прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    true
                );
                // перезагружаем страницу, чтобы избежать повтороной отправки формы
                return $this->refresh();
            } else {
                // данные не прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    false
                );
                // не перезагружаем страницу, чтобы сохранить пользовательские данные
            }
                return $this->refresh();              
            }
        }        

$model->photo = $modeltmp->photo;
            if ($model->save()) {
                // данные прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    true
                );
                // перезагружаем страницу, чтобы избежать повтороной отправки формы
                return $this->refresh();
            } else {
                // данные не прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    false
                );
                // не перезагружаем страницу, чтобы сохранить пользовательские данные
            }
        }
        return $this->render('index', [
            'model' => $model,
            'user_money' => User::findOne(['id' => $tmp_user_id]),
        ]);
    }

    public function actionView($id)
    {

$request = Yii::$app->request;
if(Yii::$app->user->isGuest){}else{

   $tmp_user = Yii::$app->user->id;
    if (User::findOne(['id' => $tmp_user])){
        $models_tmp = Wiki::findOne(['user_id' => $tmp_user]);
        $models_tmp->views++;
        $models_tmp->save();
    } 
}

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "пользователь #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => Wiki::findOne(['user_id' => $id]),
                    ])
                ];
        }else{

            return $this->render('view', [
                'model' => Wiki::findOne(['user_id' => $id]),
                'user_money' => User::findOne(['id' => $id]),
            ]);
        }
        
    } 



}
