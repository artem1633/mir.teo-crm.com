<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\helpers\AvatarResolver;
use app\models\ChatHistory;
use app\models\User;
use app\models\UserSearch;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;


/**
 * Class ChatController
 * @package app\controllers
 */
class ChatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            ['class' => UpdateOnlineBehavior::className()],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $chat_partners = $dataProvider->getModels();

        $this->layout = 'chat-layout';

        return $this->render('index', [
            'chat_partners' => $chat_partners,
        ]);
    }

    /**
     * Получает историю для отправителя сообщений
     * @param int $sender_id Отправитель сообщения
     * @return ChatHistory[]
     */
    public function actionHistory($sender_id)
    {
        header('Access-Control-Allow-Origin: *');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $history = ChatHistory::find()->where(['sender_id' => $sender_id])->all();

        return $history;
    }

    public function actionSendMessage()
    {
        $request = Yii::$app->request;
        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        $chat_partners = (new User)->getChatPartners();

        $model = new ChatHistory();
        $model->sender_id = $identity->id;

        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'date' => Yii::$app->formatter->asTime($model->message_send_datetime),
                'text' => $model->text,
                'avatar' => $identity->avatar,
            ];
        }

        return $this->render('index', [
            'chat_partners' => $chat_partners,
        ]);
    }

    /**
     * @param int $id Идентификатор собеседника
     * @return string
     */
    public function actionGetMessages($id)
    {
        $history = (new ChatHistory)->getMessages($id);

        //Отмечаем как прочитнные

        /** @var ChatHistory $message */
        foreach ($history as $message) {
            if ($message->recipient_id == Yii::$app->user->identity->id){
                $message->read = 1;
                $message->save();
            }
        }


        return $this->renderAjax('messages', [
            'history' => $history,
        ]);
    }

    /**
     * Загрузка файлов в чат
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionUploadFiles()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        $request = Yii::$app->request;

        $recipient = $request->post('recipient');

        $chat_history = new ChatHistory();

        $uploaded_files = $chat_history->saveFiles($_FILES);


        //Добавляем ссылки на загруженные файлы в сообщение
        $file_icon = Html::img('/img/fi.png');
        foreach ($uploaded_files as $file_name => $file_path) {
            $chat_history->text .= Html::a($file_icon . '&nbsp;' . $file_name, $file_path, [
                    'data-pjax' => 0,
                    'target' => '_blank'
                ]) . '<br>';
        }

        $chat_history->sender_id = $identity->id;
        $chat_history->recipient_id = $recipient;

        if (!$chat_history->save()) {
            Yii::error($chat_history->errors, '_error');
            return ['success' => 0, 'data' => json_encode($chat_history->errors)];
        }

        return ['success' => 1];
    }

    /**
     * Получает и возвращает инфу о собеседнике в чате
     * @param $id
     * @return array
     */
    public function actionGetPartnerInfo($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $partner = User::findOne($id) ?? null;

        if (!$partner) {
            return ['success' => 0];
        }

        return [
            'success' => 1,
            'name' => $partner->name,
            'avatar' => AvatarResolver::getRealAvatarPath($partner->avatar),
            'work' => $partner->work_place,
        ];
    }

    /**
     * Запрос на поиск собеседников в окне чата
     * @return array|string
     */
    public function actionSearchPartners()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $search_text = Yii::$app->request->post('search_text');

        if (!$search_text) return ['success' => 0];

        $chat_partners = User::find()->andWhere(['LIKE', 'name', $search_text])->all();

        Yii::info('Найдено собеседников: ' . count($chat_partners), 'test');

       return $this->renderAjax('/chat/partner_list.php', [
           'chat_partners' => $chat_partners,
           'search_text' => $search_text
       ]);

    }

    /**
     * Форма отправки сообщений всем рефералам
     * @throws \yii\db\Exception
     */
    public function actionNotify()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        $chat = new ChatHistory();

        $request = Yii::$app->request;

        if ($request->isGet){
            return [
                'title' => 'Сообщение рефералам',
                'content' => $this->renderAjax('_notify_form', [
                    'model' => $chat,
                ]),
                'footer' => Html::submitButton('Отправить', [
                    'class' => 'btn btn-success'
                ])
            ];
        } else {
            if ($chat->load($request->post())){
                $chat->sendingMessageOfReferrals();
            }
        }

        return [
            'forceClose' => true,
        ];
    }
}