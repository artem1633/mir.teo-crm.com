<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use Yii;
use app\models\Faq;
use app\models\FaqSearch;
use app\models\Banner;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use app\models\Invite;
use app\models\User;
use yii\helpers\Html;
use yii\web\UploadedFile;




class InviteController extends Controller {
 
 public function beforeAction($action)
    {
        if( $action->id == 'test'){
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

public function actionTest()
    {
        if( Yii::$app->request->isAJax){
            
            $id_img = $_POST['id'];
             $model_img = Banner::findOne(['id' => $id_img]);
             $model_img->delete();
             
            return  $id_img;
        }

        return 'test';
    }
        
    public function actionIndex()
    {
           

       

        if (Yii::$app->user->isGuest) {
           return $this->redirect('/site/login');
        }

        $tmp_user_id = Yii::$app->user->id;

        $model = Invite::findOne(['user_id' => $tmp_user_id]);
        if (!$model){
            $model = new Invite();
            $model->user_id = $tmp_user_id;
            $model->save();
            $model = Invite::findOne(['user_id' => $tmp_user_id]);
        }

        $modeltmp = Invite::findOne(['user_id' => $tmp_user_id]);


        $models = Banner::find()->all();
       $models_new = new banner();
       if ($models_new->load(Yii::$app->request->post())) {
         if($models_new->banner = UploadedFile::getInstance($models_new, 'banner')){ 

                if ($models_new->upload()) {
                $models_new->banner = $models_new->banner->baseName . '.' . $models_new->banner->extension;    
            if ($models_new->save()) {
                // данные прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    true
                );
                // перезагружаем страницу, чтобы избежать повтороной отправки формы
                return $this->refresh();
            } else {
                // данные не прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    false
                );
                // не перезагружаем страницу, чтобы сохранить пользовательские данные
            }
            }
        }
       };


        // если пришли post-данные...
        if ($model->load(Yii::$app->request->post())) {

            if($model->user_id != '0'){
                $model->user_id = $tmp_user_id;


                

        if($model->img = UploadedFile::getInstance($model, 'img')){ 
            $model->img = UploadedFile::getInstance($model, 'img');               
                if ($model->uploadimg()) {
                $model->img = $model->img->baseName . '.' . $model->img->extension;              
                    $model->banner = '';
                
                // file is uploaded successfully  \
                if ($model->save()) {
                // данные прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    true
                );
                // перезагружаем страницу, чтобы избежать повтороной отправки формы
                return $this->refresh();
            } else {
                // данные не прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    false
                );
                // не перезагружаем страницу, чтобы сохранить пользовательские данные
            }
                return $this->refresh();              
            }
        }

        
                $model->img = $modeltmp->img;   
            }else{
               
                 $model->img = ''; 
                  $model->user_id = $tmp_user_id;
            }

             

   

    


            if ($model->save()) {
                // данные прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    true
                );
                // перезагружаем страницу, чтобы избежать повтороной отправки формы
                return $this->refresh();
            } else {
                // данные не прошли валидацию, отмечаем этот факт
                Yii::$app->session->setFlash(
                    'success',
                    false
                );
                // не перезагружаем страницу, чтобы сохранить пользовательские данные
            }
        }
        return $this->render('index', [
            'model' => $model,
            'user_money' => User::findOne(['id' => $tmp_user_id]),
            'models' => $models, 
               'models_new' => $models_new, 
            
        ]);
    }

  


}
