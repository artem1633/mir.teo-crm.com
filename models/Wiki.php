<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;
/**
 * Модель для формы обратной связи
 */
class Wiki extends ActiveRecord
{
 /**
     * @var UploadedFile
     */
	

    public function attributeLabels() {
        return [        	
            'name' => 'Фамилия Имя Отчество',
            'description' => 'Кратко о себе в данный момент',
            'purpose' => 'Жизненное кредо, миссия, цели, мечты',
            'childhood' => 'Детство, юность',
            'education' => 'Образование',
            'work' => 'Трудовая деятельность',
            'today' => 'Жизнень в настоящее время',
            'charity' => 'Общественная деятельность, благотворительность',
            'interests' => 'Интересы',
            'hobby' => 'Хобби, увлечения',
            'language' => 'Иностранные языки',
            'dignities' => 'Достоинства и недостатки',
            'family' => 'Семья',
            'pets' => 'Домашние питомцы',
            'idols' => 'Кумиры',
            'quote' => 'Любимая цитата',
            'joke' => 'Любимый анекдот',
            'cars' => 'Любимые автомобили',
            'brand' => 'Любимые марки одежды, обуви, аксессуаров',
            'food' => 'Любимая еда, напитки',
            'countries' => 'Любимые страны, города, места',
            'sport' => 'Любимые виды спорта',
            'book' => 'Любимые книги, авторы',
            'music' => 'Любимая музыка, фильмы, сериалы',
            'recommend' => 'Я рекомендую, советую',
            'birth' => 'Год рождения',
            'city_birth' => 'Город рождения',
            'country_birth' => 'Страна рождения',
            'father' => 'Отец',
            'mother' => 'Мать',
            'wife' => 'Супруга',
            'children' => 'Дети',
            'religion' => 'Вероисповедание',
            'photo' => 'фото',
            'views' => 'Просмотров',
    ];}
    public function rules(){
    	return [    	
    		['name', 'default'],
    		['description', 'default'],
    		['purpose', 'default'],
    		['childhood', 'default'],
    		['education', 'default'],
    		['work', 'default'],
    		['today', 'default'],
    		['charity', 'default'],
    		['interests', 'default'],
    		['hobby', 'default'],
    		['language', 'default'],
    		['dignities', 'default'],
    		['family', 'default'],
    		['pets', 'default'],
    		['idols', 'default'],
    		['quote', 'default'],
    		['joke', 'default'],
    		['cars', 'default'],
    		['brand', 'default'],
    		['food', 'default'],
    		['countries', 'default'],
    		['sport', 'default'],
    		['book', 'default'],
    		['music', 'default'],
    		['recommend', 'default'],
    		['birth', 'default'],
    		['city_birth', 'default'],
    		['country_birth', 'default'],
    		['father', 'default'],
    		['mother', 'default'],
    		['wife', 'default'],
    		['children', 'default'],
    		['religion', 'default'],
    		['views', 'default'],
    		[['photo'], 'file', 'extensions' => 'png, jpg'],
    	];
    }
    public function upload(){
 if($this->validate()){
   $this->photo->saveAs('uploads/' . $this->photo->baseName . '.' . $this->photo->extension);
            return true;
 }else{
 return false;
 }
}

}