<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Currency
 *
 * @package app\models
 *
 * @property integer $id
 * @property string $name название
 * @property string $slug слаг
 * @property double $course курс
 */

class Currency extends ActiveRecord{

    /**
     * @inheritDoc
     * @return array
     */
    public function rules(){

        return [
            [['name', 'slug'], 'string', 'max' => 255],
            ['course', 'number']
        ];
    }
}