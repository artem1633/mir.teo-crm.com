<?php

namespace app\models;

use Yii;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news_file".
 *
 * @property int $id
 * @property int $news_id Пост
 * @property string $dir Папка
 * @property string $file_name Наименование
 * @property string $original_file_name Файл
 * @property int $type Тип
 * @property string $url Путь
 *
 * @property News $news
 */
class NewsFile extends \yii\db\ActiveRecord
{
    const TYPE_IMAGE = 1;
    const TYPE_OTHER = 15;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'type'], 'integer'],
            [['dir', 'file_name', 'original_file_name', 'url'], 'string', 'max' => 255],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public function upload($file)
    {
        if (strpos($file->type, "image") !== false)
        {
            $this->type = self::TYPE_IMAGE;

            $out_dir = '/uploads/images/'.substr(md5(time()), 0, 2).'/'.substr(md5(time()+1), 0, 2).'/';
        }
        else
        {
            $this->type = self::TYPE_OTHER;

            $out_dir = '/uploads/files/'.substr(md5(time()), 0, 2).'/'.substr(md5(time()+1), 0, 2).'/';
        }

        $out_file_name = md5(time()+2) . '.' . $file->extension;
        $out_file = $out_dir . $out_file_name;

        BaseFileHelper::createDirectory(Yii::getAlias("@webroot") . $out_dir);

        if ($file->saveAs(Yii::getAlias("@webroot") . $out_file))
        {
            $this->dir = $out_dir;
            $this->file_name = $out_file_name;
            $this->original_file_name = $file->baseName . '.' . $file->extension;

            return true;
        }
        else
            return false;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->dir . '/' . $this->file_name;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $file_name = Yii::getAlias('@webroot') . $this->dir . $this->file_name;
        if (file_exists($file_name) && !is_dir($file_name))
            unlink($file_name);

        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'Пост',
            'dir' => 'Папка',
            'file_name' => 'Наименование',
            'original_file_name' => 'Файл',
            'type' => 'Тип',
            'url' => 'Путь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
