<?php

namespace app\models;

use app\components\user\Refer;
use app\helpers\AvatarResolver;
use SendGrid\Mail\Mail;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login Логин
 * @property string $name ФИО
 * @property string $birth_date дата рождения
 * @property string $country страна проживания
 * @property string $living_place место проживания
 * @property string $education образование
 * @property string $profession профессия
 * @property string $work_place место работы
 * @property string $work_position должность
 * @property string $work_status статус работы
 * @property string $phone телефон
 * @property string $email почта
 * @property string $interests интересы
 * @property string $hobby хобби
 * @property string $password_hash Зашифрованный пароль
 * @property int $role Роль
 * @property double $balance_usd Баланс (Доллар США)
 * @property int $ref_id Кто пригласил
 * @property int $is_deletable Можно удалить или нельзя
 * @property string $last_active_datetime Дата и время последней активности
 * @property int $points баллы
 * @property string $created_at
 * @property string $avatar Аватар
 * @property string $play_name
 * @property int $email_approved Email Подтвержден
 * @property int $messager_whats_app WhatsApp
 * @property int $messager_viber Viber
 * @property int $messager_telegram Telegram
 * @property int $messager_none Отсутствуют
 * @property int $interest_aristocratic Аристократические титулы
 * @property int $interest_services Поставки товаров и услуг
 * @property int $interest_part Членство в профсоюзе
 * @property int $interest_intercom Работа в интеркомах
 * @property int $interest_help Помогите определиться
 *
 * @property Refer $refer
 *
 * @property Accruals[] $accruals
 * @property RefAgent[] $refAgents
 * @property Ticket[] $tickets
 * @property Transaction[] $transactions
 * @property Transaction[] $transactions0
 * @property TransactionPurpose[] $transactionPurposes
 * @property User $ref
 * @property User[] $users
 * @property ChatHistory $last_message
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_USER = 3;
    const ROLE_EDITOR = 4;

    public $password;

    private $oldPasswordHash;


    /**
     * @var
     */
    private $_refer;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'name',
                'login',
                'living_country',
                'is_deletable',
                'password',
                'password_hash',
                'work_position1',
                'email_approved',
                'play_name',
                'role'
            ],
            self::SCENARIO_EDIT => [
                'name',
                'play_name',
                'login',
                'is_deletable',
                'password',
                'password_hash',
                'phone',
                'role',
                'birth_date',
                'country',
                'living_place',
                'education',
                'profession',
                'work_place',
                'work_position',
                'email_approved',
                'interests',
                'living_country',
                'work_position1',
                'hobby',
                'work_status'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
//            [
//                'password',
//                'match',
//                'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/',
//                'message' => '{attribute} не соответствует всем параметрам безопасности'
//            ],
            [['login'], 'unique'],
            ['login', 'email'],
            [['created_at', 'last_active_datetime', 'email_approved'], 'safe'],
            [['is_deletable', 'rate_id', 'messager_whats_app', 'messager_viber', 'messager_telegram', 'messager_none', 'interest_aristocratic', 'interest_services', 'interest_part', 'interest_intercom', 'interest_help',], 'integer'],
            [['balance_usd'], 'number'],
            [
                [
                    'login',
                    'name',
                    'country',
                    'living_place',
                    'education',
                    'profession',
                    'work_place',
                    'work_position',
                    'work_status',
                    'phone',
                    'email',
                    'interests',
                    'hobby',
                    'password_hash',
                    'living_country',
                    'work_position1',
                    'play_name'
                ],
                'string',
                'max' => 255
            ],
            [
                ['ref_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['ref_id' => 'id']
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if ($this->ref_id != null) {
            $users = User::find()->where(['ref_id' => $this->id])->all();

            foreach ($users as $user) {
                $user->ref_id = $this->ref_id;
                $user->save(false);
            }
        }

        if ($uid == $this->id) {
            Yii::$app->session->setFlash('error',
                "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if ($this->is_deletable == false) {
            Yii::$app->session->setFlash('error',
                "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->password != null) {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            $href = Url::toRoute(['site/approve-email', 'h' => $this->password_hash], true);
            $text = 'Подтвердите ваш email пройдя по ссылке: '.Html::a($href, $href);

            try {
//                Yii::$app->mailer->compose()
//                    ->setFrom('mir.notify.service@mail.ru')
//                    ->setTo($this->login)
//                    ->setSubject('Подтверждение почты')
//                    ->setHtmlBody($text)
//                    ->send();

                $email = new Mail();
                $sendgrid = new \SendGrid('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');
                $email->setFrom('no-reply@mir.com', 'Mir notification service');
                $email->setSubject('Подтверждение почты');
                $email->addTo($this->login);
                $email->addContent(
                    'text/html',
                    $text
                );
                $sendgrid->send($email);

            } catch (\Exception $e){
                Yii::warning('Email sending failed with error: '.$e->getMessage().' Text of message: '.$text);
            }
        }

    }

    /**
     * @return boolean
     */
    public function isSuperAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    /**
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->role == self::ROLE_MANAGER || $this->role == self::ROLE_ADMIN;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'ФИО',
            'play_name' => 'Игровое имя',
            'avatar' => 'Avatar',
            'email_approved' => 'Подтвержденный email',
            'birth_date' => 'дата рождения',
            'country' => 'Гражданство',
            'living_country' => 'Страна',
            'living_place' => 'Город',
            'education' => 'образование',
            'profession' => 'профессия',
            'work_place' => 'место работы',
            'work_position' => 'должность 1',
            'work_position1' => 'должность 2',
            'work_status' => 'статус работы',
            'phone' => 'телефон',
            'email' => 'почта',
            'interests' => 'интересы',
            'hobby' => 'хобби',
            'password_hash' => 'Зашифрованный пароль',
            'role' => 'Роль',
            'balance_usd' => 'Баланс (Доллар США)',
            'ref_id' => 'Кто пригласил',
            'is_deletable' => 'Можно удалить или нельзя',
            'last_active_datetime' => 'Дата и время последней активности',
            'points' => 'баллы',
            'created_at' => 'Created At',
            'messager_whats_app' => 'WhatsApp',
            'messager_viber' => 'Viber',
            'messager_telegram' => 'Telegram',
            'messager_none' => 'Отсутствуют',
            'interest_aristocratic' => 'Аристократические титулы',
            'interest_services' => 'Поставки товаров и услуг',
            'interest_part' => 'Членство в профсоюзе',
            'interest_intercom' => 'Работа в интеркомах',
            'interest_help' => 'Помогите определиться',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @return Refer
     */
    public function getRefer()
    {
        if ($this->_refer == null) {
            $this->_refer = new Refer(['user' => $this]);
        }

        return $this->_refer;
    }

    /**
     * @return string
     */
    public function getSelfRefHref()
    {
//        return Url::toRoute(['site/register', 'ref_id' => $this->id], true);

        return 'http://miruwir.info?ref_id='.$this->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRef()
    {
        return $this->hasOne(User::className(), ['id' => 'ref_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccrualsFor()
    {
        return $this->hasMany(Accruals::className(), ['for_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccrualsFrom()
    {
        return $this->hasMany(Accruals::className(), ['from_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['ref_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getRealAvatarPath()
    {
        return AvatarResolver::getRealAvatarPath($this->avatar);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username, 'email_approved' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return boolean
     */
    public function isOnline()
    {
        if ($this->last_active_datetime == null) {
            return false;
        }

        $nowDt = new \DateTime("now", new \DateTimeZone(\Yii::$app->timeZone));
        $seen = new \DateTime($this->last_active_datetime, new \DateTimeZone(\Yii::$app->timeZone));

        return $nowDt->getTimestamp() - $seen->getTimestamp() < 2 * 60;
    }

    /**
     * @return User[]
     */
    public function getChatPartners()
    {
        return self::find()->andWhere(['<>', 'id', Yii::$app->user->identity->id])->all();
    }

    /**
     * @param int $id Идентификатор пользователя
     * @return mixed
     */
    public function getLastMessage($id)
    {
        /** @var ChatHistory $last_message */
        $last_message = ChatHistory::find()->andWhere(['sender_id' => $id])->orderBy(['id' => SORT_DESC])->one() ?? null;
        if (!$last_message) {
            return [
                'message' => '',
                'date' => '',
            ];
        }
        $last_message->text = strip_tags($last_message->text);
        return [
            'message' => mb_substr($last_message->text, 0, 20, 'UTF-8') . '...',
            'date' => $last_message->message_send_datetime,
        ];
    }

    /**
     * @param int $id Идентификатор пользователя
     * @return mixed
     */
    public function getLastMessageChat($id)
    {
        $pks = [Yii::$app->user->getId(), $id];
        /** @var ChatHistory $last_message */
        $last_message = ChatHistory::find()->andWhere(['and', ['recipient_id' => $pks], ['sender_id' => $pks]])->orderBy(['id' => SORT_DESC])->one() ?? null;
        if (!$last_message) {
            return [
                'message' => '',
                'date' => '1970-01-01 00:00:00',
            ];
        }
        $last_message->text = strip_tags($last_message->text);
        return [
            'message' => mb_substr($last_message->text, 0, 20, 'UTF-8') . '...',
            'date' => $last_message->message_send_datetime,
        ];
    }

    /**
     * Получает кол-во непрочитанных сообщений
     * @return int|string
     */
    public function getUnreadMessagesCount()
    {
        return ChatHistory::find()->andWhere([
            'sender_id' => $this->id,
            'recipient_id' => Yii::$app->user->identity->id ,
            'read' => 0
        ])->count();
    }

    /**
     * Получает статус пользователя
     * @return string
     */
    public function getStatus()
    {
        $last_active = time() - strtotime($this->last_active_datetime);

        if ($last_active < (5 * 60)){
            $class = 'partner-status green-status';
        } elseif ($last_active > (5 * 60) && $last_active < (30 * 60)){
            $class = 'partner-status yellow-status';
        } else {
            $class = 'partner-status red-status';
        }

        return $class;
    }
}
