<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "accruals".
 *
 * @property int $id
 * @property string $date дата и время начисления
 * @property integer $payment_number номер платежа
 * @property int $from_user_id от кого
 * @property int $for_user_id кому
 * @property double $amount сумма начисления
 * @property int $user_level уровень тому кому начислено
 *
 * @property User $forUser
 * @property User $fromUser
 */
class Accruals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accruals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['from_user_id', 'for_user_id', 'user_level', 'payment_number'], 'integer'],
            [['amount'], 'number'],
            [['for_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['for_user_id' => 'id']],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'дата и время начисления',
            'payment_number' => 'Номер платежа',
            'from_user_id' => 'от кого',
            'for_user_id' => 'кому',
            'amount' => 'сумма начисления',
            'user_level' => 'уровень тому кому начислено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForUser()
    {
        return $this->hasOne(User::className(), ['id' => 'for_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return void
     */
    public function setPaymentNumber(){

        $this->payment_number = $this->generatePaymentNumber();
    }

    /**
     * Генерация номера платежа
     *
     * @return int
     */
    private function generatePaymentNumber(){

        $number = rand(10000, 99999);
        while(self::find()->where(['payment_number' => $number])->exists() == true){
            $number = rand(10000, 99999);
        }
        while(Transfer::find()->where(['payment_number' => $number])->exists() == true){
            $number = rand(10000, 99999);
        }
        return $number;
    }
}
