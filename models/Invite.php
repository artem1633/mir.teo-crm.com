<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;
/**
 * Модель для формы обратной связи
 */
class Invite extends ActiveRecord
{
 /**
     * @var UploadedFile
     */
	

    public function attributeLabels() {
        return [     
        'user_id' => 'Написать сообщение',   	
            'title' => 'Написать сообщение',
            'banner' => 'Баннер',
            'img' => 'Изображение',            
    ];}
    public function rules(){
    	return [    	
    		['title', 'default'],  
            ['user_id', 'default'],    	
            ['banner', 'default'],	
    		            [['img'], 'file', 'extensions' => 'png, jpg'],
    	];
    }
    
public function uploadimg(){
 if($this->validate()){
   $this->img->saveAs('uploads/' . $this->img->baseName . '.' . $this->img->extension);
            return true;
 }else{
 return false;
 }
}

}