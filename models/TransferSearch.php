<?php

namespace app\models;

use Yii;
use app\models\Transfer;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\components\UserBalanceFormatter;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class TransferSearch extends Transfer{

    public $search;

    public $user_ids = [];

    private $_user_id;
    private $_models;
    private $_limit;
    private $_type;

    /**
     * @inheritDoc
     * @return void
     */
    public function init(){

        $this->_user_id = Yii::$app->user->getId();
        $this->_limit = Yii::$app->request->post('limit');
        $this->_type = Yii::$app->request->get('type');
        parent::init();
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function rules(){

        return [
            ['search', 'trim'],
            ['search', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return int|string
     */
    public function search($params, $count = false){

        $transfer_query = Transfer::find()
            ->joinWith([
                'fromUser' => function($q){
                    $q->from('user fromUser');
                },
                'forUser' => function($q){
                    $q->from('user forUser');
                }
            ])
            ->orderBy(['date' => SORT_DESC]);
        $accrual_query = Accruals::find()
            ->joinWith([
                'fromUser' => function($q){
                    $q->from('user fromUser');
                },
                'forUser' => function($q){
                    $q->from('user forUser');
                }
            ])
            ->orderBy(['date' => SORT_DESC]);

        $this->load($params);

        if($this->_type){
            switch($this->_type){
                case 'coming':
                    $transfer_query->where(['transfer.for_user_id' => Yii::$app->user->getId()]);
                    $accrual_query->where(['accruals.for_user_id' => Yii::$app->user->getId()]);
                    break;
                case 'expenses':
                    $transfer_query->where(['transfer.from_user_id' => Yii::$app->user->getId()]);
                    $accrual_query->where(['accruals.from_user_id' => Yii::$app->user->getId()]);
                    break;
                case 'ref':
                    $accrual_query->where(['accruals.for_user_id' => Yii::$app->user->getId()]);
                    break;
            }
        }else{
            $transfer_query->where(['from_user_id' => $this->_user_id])
                ->orWhere(['for_user_id' => $this->_user_id]);
            $accrual_query->where(['from_user_id' => $this->_user_id])
                ->orWhere(['for_user_id' => $this->_user_id]);
        }
        if($this->search){
            $transfer_query->andWhere(['or',
                ['like', 'transfer.payment_number', $this->search],
                ['like', 'date_format(`date`,\'%d.%m.%Y %H:%i\')', $this->search],
                ['like', 'fromUser.name', $this->search],
                ['like', 'forUser.name', $this->search],
                ['like', 'transfer.purpose_of_payment', $this->search],
                (float)$this->search != '0' ? ['like', 'transfer.amount', (float)$this->search] : ''
            ]);
            $accrual_query->andWhere(['or',
                ['like', 'accruals.payment_number', $this->search],
                ['like', 'date_format(`date`,\'%d.%m.%Y %H:%i\')', $this->search],
                ['like', 'forUser.name', $this->search],
                (float)$this->search != '0' ? ['like', 'accruals.amount', (float)$this->search] : ''
            ]);
        }
        $model1 = ArrayHelper::index($transfer_query->all(), function($model){
            return 'transfer-'.$model->id;
        });
        $model2 = ArrayHelper::index($accrual_query->all(), function($model){
            return 'accrual-'.$model->id;
        });
        if($this->_type != 'ref'){
            $models = array_merge($model1, $model2);
            uasort($models, function($a, $b){
                return strtotime($a['date']) < strtotime($b['date']);
            });
        }else{
            $models = $model2;
        }

        $this->_models = $models;

        if($count){
            return count($models);
        }
        $this->getRefUserIds();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'pageSize' => $this->_limit ? $this->_limit : 25,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return bool
     */
    public function getBalance(){

        $accruals = Accruals::find()->where(['from_user_id' => Yii::$app->user->getId()])->orWhere(['for_user_id' => Yii::$app->user->getId()])->all();
        $transfer = Transfer::find()->where(['from_user_id' => Yii::$app->user->getId()])->orWhere(['for_user_id' => Yii::$app->user->getId()])->all();
        $models = array_merge($transfer, $accruals);
        uasort($models, function($a, $b){
            return strtotime($a['date']) < strtotime($b['date']);
        });
        if($models){
            $balance_arr = [];
            $balance = 0;
            foreach(array_reverse($models) as $model){
                if($model->from_user_id == Yii::$app->user->getId()){
                    $balance -= $model->amount;
                    $balance_arr[$model instanceof Transfer ? 'transfer-'.$model->id : 'accrual-'.$model->id] = $balance;
                }elseif($model->for_user_id == Yii::$app->user->getId()){
                    $balance += $model->amount;
                    $balance_arr[$model instanceof Transfer ? 'transfer-'.$model->id : 'accrual-'.$model->id] = $balance;
                }
            }
            return array_reverse($balance_arr);
        }
        return false;
    }

    /**
     * @return void
     */
    private function getRefUserIds(){

        if($this->_models){
            uasort($this->_models, function($a, $b){
                return $a['user_level'] < $b['user_level'];
            });
            foreach($this->_models as $model){
                if($model instanceof Accruals){
                    if($model->from_user_id){
                        if($model->user_level == 1){
                            if($model->fromUser){
                                $this->user_ids[$model->id] = $model->from_user_id;
                            }
                        }else{
                            $user_query = User::find()->where(['ref_id' => $model->from_user_id]);
                            if(empty($this->user_ids)){
                                $user = $user_query->one();
                            }else{
                                $user = $user_query->andWhere(['not', ['id' => $this->user_ids]])->one();
                            }
                            if($user){
                                $this->levelRecursive($model->id, $user, 2, $model->user_level);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param integer $accrual_id
     * @param User $parent_user
     * @param integer $current_level
     * @param integer $level
     */
    private function levelRecursive($accrual_id, $parent_user, $current_level, $level){

        $parent_id = null;
        for($i = $current_level; $i <= $level; $i++){
            if($i == $level && $level == 2){
                $this->user_ids[$accrual_id] = $parent_user->id;
            }
            $user_query = User::find()->where(['ref_id' => !$parent_id ? $parent_user->id : $parent_id]);
            if(empty($this->user_ids)){
                $user = $user_query->one();
            }else{
                $user = $user_query->andWhere(['not', ['id' => $this->user_ids]])->one();
            }
            if($user){
                if($i == $level){
                    $parent_id = $user->id;
                    $this->user_ids[$accrual_id] = $user->id;
                }
            }
        }
    }
}