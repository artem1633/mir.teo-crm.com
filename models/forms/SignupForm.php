<?php
namespace app\models\forms;

use Yii;
use app\models\Accruals;
use app\models\RefAgent;
use app\models\Settings;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $email;
    public $password;
    public $ref_id;
    public $living_country;
    public $name;
    public $birth_date;
    public $country;
    public $living_place;
    public $education;
    public $profession;
    public $work_place;
    public $work_position;
    public $work_status;
    public $phone;
    public $interests;
    public $hobby;



    public $messager_whats_app;
    public $messager_viber;
    public $messager_telegram;
    public $messager_none;
    public $interest_aristocratic;
    public $interest_services;
    public $interest_part;
    public $interest_intercom;
    public $interest_help;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','email', 'password', 'living_place'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            ['birth_date', function(){
                if($this->birth_date != null){
                    $date = strtotime("{$this->birth_date} 23:59:59");
                    $now = strtotime(date("Y-m-d 23:59:59", time()));

                    if($date > $now){
                        $this->addError('birth_date', 'Некорректная дата');
                        return false;
                    }
                }
            }],
            [['ref_id'], 'integer'],
            [['ref_id'], 'required'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'targetAttribute' => 'login', 'message' => 'Этот email уже зарегистрирован'],
            ['password', 'string', 'min' => 6],
            [['name', 'birth_date', 'country', 'living_place', 'education', 'profession', 'living_country', 'work_place', 'work_position', 'work_status', 'phone', 'interests', 'hobby'], 'string', 'max' => 255],
            [['messager_whats_app', 'messager_viber', 'messager_telegram', 'messager_none', 'interest_aristocratic', 'interest_services', 'interest_part', 'interest_intercom', 'interest_help'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль',
            'ref_id' => 'Код реферера',
            'name' => 'Ф.И.О.',
            'birth_date' => 'Дата рождения',
            'country' => 'Ваше гражданство',
            'living_country' => 'Страна',
            'living_place' => 'Город',
            'education' => 'Ваше образование',
            'profession' => 'Ваша профессия',
            'work_place' => 'Ваше место работы',
            'work_position' => 'Ваша должность',
            'work_status' => 'Интересует ли Вас трудоустройство',
            'phone' => 'Номер телефона',
            'interests' => 'Круг Ваших интересов',
            'hobby' => 'Ваше (Ваши) хобби',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }



        $user = new User();
        $user->name = $this->name;
        $user->login = $this->email;
        $user->email = $this->email;
        $user->password = $this->password;
        $user->role = User::ROLE_USER;
        $user->birth_date = $this->birth_date;
        $user->living_country = $this->living_country;
        $user->country =  $this->country;
        $user->living_place = $this->living_place;
        $user->education = $this->education;
        $user->profession = $this->profession;
        $user->work_place = $this->work_place;
        $user->work_position = $this->work_position;
        $user->work_status = $this->work_status;
        $user->phone = $this->phone;
        $user->interests = $this->interests;
        $user->hobby = $this->hobby;
        $user->ref_id = $this->ref_id;
        $user->messager_whats_app = ($this->messager_whats_app == 'on' ? 1 : 0);
        $user->messager_telegram = ($this->messager_telegram == 'on' ? 1 : 0);
        $user->messager_viber = ($this->messager_viber == 'on' ? 1 : 0);
        $user->messager_none = ($this->messager_none == 'on' ? 1 : 0);
        $user->interest_services = ($this->interest_services == 'on' ? 1 : 0);
        $user->interest_aristocratic = ($this->interest_aristocratic == 'on' ? 1 : 0);
        $user->interest_part = ($this->interest_part == 'on' ? 1 : 0);
        $user->interest_intercom = ($this->interest_intercom == 'on' ? 1 : 0);
        $user->interest_help = ($this->interest_help == 'on' ? 1 : 0);
        $user->save(false);
        $this->levelRecursive($user);

        return $user;
    }

    public function levelRecursive($user, $counter = 1)
    {
        if($counter > 7){
            return false;
        }

        $level = Settings::find()->where(['key' => "level_$counter"])->one()->value;

        $parentUser = User::find()->where(['id' => $user->ref_id])->one();

        if($parentUser){
            $parentUser->balance_usd = $parentUser->balance_usd + $level;
            $parentUser->save(false);

            $accruals = new Accruals();
            $accruals->payment_number = $accruals->setPaymentNumber();
            $accruals->amount = $level;
            $accruals->from_user_id = $user->id;
            $accruals->for_user_id = $parentUser->id;
            $accruals->user_level = $counter;
            $accruals->date = date('Y-m-d H:i:s');
            $accruals->save(false);

            $parentUser2 = User::find()->where(['id' => $parentUser->ref_id])->one();

            if($parentUser2 == null){
                return true;
            } else {
                $this->levelRecursive($parentUser, $counter+1);
            }
        }

        return true;
    }

//    /**
//     * update user.
//     *
//     * @param User $user
//     * @return User|null the saved model or null if saving fails
//     */
//    public function update($user)
//    {
//        if (!$this->validate()) {
//            return null;
//        }
//
//        $user->name = $this->name;
//        $user->surname = $this->surname;
//        $user->patronymic = $this->patronymic;
//        $user->phone = $this->phone;
//        $user->category = $this->category;
//        $user->department = $this->department;
//        $user->email = $this->email;
//        $user->setPassword($this->password);
//        $user->password = $this->password;
//        // $user->generateAuthKey();
//
//        return $user->update();
//    }
}
