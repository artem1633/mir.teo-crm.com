<?php

namespace app\models\forms;

use app\models\User;
use SendGrid\Mail\Mail;
use Yii;
use yii\base\Model;

/**
 * Class ForgetPassword
 * @package app\models\forms
 */
class ForgetPassword extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * @return bool
     */
    public function sendNewPassword()
    {
        if($this->validate()){

            $newPassword = Yii::$app->security->generateRandomString(10);

            /** @var User $user */
            $user = User::find()->where(['login' => $this->email])->one();

            if($user){
                $user->setPassword($newPassword);
                $user->save(false);
            }

            $email = new Mail();
            $sendgrid = new \SendGrid('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');
            $email->setFrom('no-reply@mir.com', 'Mir notification service');
            $email->setSubject('Востановление пароля');
            $email->addTo($user->login);
            $email->addContent(
                'text/html',
                'Ваш новый пароль: '.$newPassword
            );
            $sendgrid->send($email);


//            Yii::$app->mailer->compose()
//                ->setFrom('money.notification.service@mail.ru')
//                ->setTo($user->login)
//                ->setSubject('Востанавление пароля')
//                ->setHtmlBody('Ваш новый пароль: '.$newPassword)
//                ->send();

            return true;
        }

        return false;
    }
}