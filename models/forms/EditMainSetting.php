<?php
/**
 * Created by PhpStorm.
 * User: ssset
 * Date: 06.07.2020
 * Time: 19:51
 */

namespace app\models\forms;
use app\models\Accruals;
use app\models\RefAgent;
use app\models\Settings;
use yii\base\Model;
use app\models\User;

class EditMainSetting extends Model
{
    public $email;
    public $password;
    public $phone;
    public $messager_whats_app;
    public $messager_viber;
    public $messager_telegram;
    public $messager_none;

    public function rules()
    {
        return [

            ['email', 'trim'],
            ['email', 'email'],
            [['messager_whats_app', 'messager_viber', 'messager_telegram', 'messager_none' , 'password','phone'], 'safe']];
    }


    public function editmain($id){

        if (!$this->validate()) {
            var_dump($this->getErrors());
            return null;
        }

        $user = User::find()->where(['id' => $id])->one();

        if ($user->email != $this->email)
            $user->login=$this->email;

        if ($this->password!=null) {
            $user->setPassword($this->password);
            $user->password = $this->password;
        }
        $user->phone=$this->phone;

        if ($this->messager_whats_app)
            $user->messager_whats_app=1;
        else
            $user->messager_whats_app=0;

        if ($this->messager_viber)
            $user->messager_viber=1;
        else
            $user->messager_viber=0;

        if ($this->messager_telegram)
            $user->messager_telegram=1;
        else
            $user->messager_telegram=0;

        if ($this->messager_none)
            $user->messager_none=1;
        else
            $user->messager_none=0;


        $user->save(false);


        return $user;
    }
}