<?php

namespace app\models\forms;

use app\models\Transfer;
use Yii;
use app\models\User;
use yii\base\Model;
use app\components\UserBalanceFormatter;

/**
 * Class MoneyTransactionForm
 *
 * @package app\models\forms
 *
 * @property double $sum сумма
 * @property string $profile_url ссылка на профиль
 * @property string $purpose_of_payment назначение платежа
 */

class MoneyTransactionForm extends Model{

    public $sum;
    public $profile_url;
    public $purpose_of_payment;

    private $_for_user_id;

    /**
     * @inheritDoc
     * @return array
     */
    public function rules(){

        return [
            [['sum', 'profile_url'], 'required'],
            ['sum', 'number'],
            [['profile_url', 'purpose_of_payment'], 'string'],
            ['profile_url', 'url', 'message' => 'Неверная ссылка'],
            ['sum', 'validateSum'],
            ['profile_url', 'validateUrl'],
        ];
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function attributeLabels(){

        return [
            'sum' => 'Сумма',
            'profile_url' => 'Ссылка на профиль',
            'purpose_of_payment' => 'Назначение платежа'
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateSum($attribute){

        if($this->$attribute > Yii::$app->user->identity->balance_usd){
            $this->addError($attribute, 'Недостаточно средств');
        }
    }

    /**
     * @param string $attribute
     */
    public function validateUrl($attribute){

        if(preg_match('/id=\d+$/', $this->profile_url)){
            $explode_url = explode('=', $this->profile_url);
            if(!($User = User::findOne(['id' => end($explode_url)])) || $User->id == Yii::$app->user->getId()){
                $this->addError($attribute, 'Неверная ссылка');
            }
            $this->_for_user_id = $User->id;
        }else{
            $this->addError($attribute, 'Неверная ссылка');
        }
    }

    /**
     * @return bool
     */
    public function saveData(){

        if(!$this->_for_user_id) return false;
        $transfer = new Transfer();
        $transfer->currency_id = 1; //роллар, в дальнейшем при выборе других валют, сюда нужно будет записывать id валюты
        $transfer->from_user_id = Yii::$app->user->getId();
        $transfer->for_user_id = $this->_for_user_id;
        $transfer->setPaymentNumber();
        $transfer->purpose_of_payment = $this->purpose_of_payment;
        $transfer->amount = UserBalanceFormatter::asBalance($this->sum);
        $transfer->type = Transfer::TYPE_TRANSFER;
        if(!$transfer->save()){
            Yii::error(print_r($transfer->getErrors(), true));
            return false;
        }
        Yii::$app->user->identity->balance_usd -= $this->sum;
        Yii::$app->user->identity->save(false);
        if($for_user = User::findOne(['id' => $this->_for_user_id])){
            $for_user->balance_usd += $this->sum;
            $for_user->save(false);
        }
        return true;
    }
}