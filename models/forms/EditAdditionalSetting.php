<?php
/**
 * Created by PhpStorm.
 * User: ssset
 * Date: 06.07.2020
 * Time: 19:51
 */

namespace app\models\forms;
use app\models\Accruals;
use app\models\RefAgent;
use app\models\Settings;
use yii\base\Model;
use app\models\User;
use yii\web\UploadedFile;

class EditAdditionalSetting extends Model
{
    public $play_name;
    public $name;
    public $country;
    public $living_place;
    public $birth_date;
    public $education;
    public $profession;
    public $marital_status;
    public $interests;
    public $hobby;
    public $link_vk;
    public $link_odnok;
    public $link_facebook;
    public $link_instagram;
    public $link_twitter;
    public $link_youtube;
    public $code_state;
    public $position_state;
    public $code_service;
    public $aristocrat_dignity1;
    public $code_vip;
    public $totalistic_dignity;
    public $franchise_city;
    public $code_tpm;
    public $code_dsm;
    public $code_epm;
    public $code_ppm;
    public $avatar;
    public $gerb;
    public $is_simple;
    public $code_sity;
    public $code_matrix;
    public $totem_code;
    public $totem;
    const STATUS_STATE = 11585;
    const STATUS_STATE2 = 11222;
    const STATUS_STATE3 = 11747;
    const STATUS_STATE4 = 11615;
    const STATUS_STATE5 = 11254;
    const STATUS_STATE6 = 11365;
    const STATUS_SERVICE = 22847;
    const STATUS_VIP = 33655;
    const STATUS_TPM = 44250;
    const STATUS_DSM = 55200;
    const STATUS_DSM2 = 55321;
    const STATUS_DSM3 = 55890;
    const STATUS_EPM = 66100;
    const STATUS_EPM2 = 66909;
    const STATUS_PPM = 77091;
    const STATUS_SIMPLE2 = 88974;
    const STATUS_SIMPLE = 88120;
    const SityValidator = 95120;
    const MatrixValidator = 10353;
    const MatrixValidator2 = 10223;
    const TotemCode = 10110;
    const TotemCode2 = 10228;
    const TotemCode3 = 10337;
    const TotemCode4 = 10424;
    const TotemCode5 = 10578;
    const TotemCode6 = 10687;
    const TotemCode7 = 10755;
    const TotemCode8 = 10827;
    const TotemCode9 = 10965;
    const TotemCode10 = 10122;
    public function rules()
    {
        return [
            ['code_state','CodeStateValidator'],
            ['code_service','CodeServiceValidator'],
            ['code_vip','CodeVipValidator'],
            ['code_tpm','CodeTPMValidator'],
            ['code_dsm','CodeDSMValidator'],
            ['code_epm','CodeEPMValidator'],
            ['code_ppm','CodePPMValidator'],
            ['is_simple','SimpleValidator'],
            ['code_sity','SityValidator'],
            ['code_matrix','MatrixValidator'],
            ['totem_code','TotemCodeValidator'],


            [['totem','franchise_city','totalistic_dignity','aristocrat_dignity1','code_service','position_state','play_name','name','country','living_place','birth_date','education','profession','marital_status','interests','hobby','link_vk','link_odnok','link_facebook','link_instagram','link_twitter','link_youtube','interest_state'],'safe'],



//            [['avatar'], 'file', 'extensions'=>['jpg','png'],'skipOnEmpty'=>false, 'on'=>'insert'],
        ];


    }

    public function MatrixValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::MatrixValidator,self::MatrixValidator2]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }
    public function SityValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::SityValidator]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }
    public function SimpleValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::STATUS_SIMPLE,self::STATUS_SIMPLE2]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }
    public function CodePPMValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::STATUS_PPM]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }
    public function CodeEPMValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::STATUS_EPM,self::STATUS_EPM2]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }
    public function CodeDSMValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::STATUS_DSM,self::STATUS_DSM2,self::STATUS_DSM3]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }
    public function CodeTPMValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::STATUS_TPM]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }
    public function CodeVipValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::STATUS_VIP]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }

    public function CodeStateValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::STATUS_STATE,self::STATUS_STATE2,self::STATUS_STATE3,self::STATUS_STATE4,self::STATUS_STATE5,self::STATUS_STATE6]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }

    public function CodeServiceValidator($attribute,$params)
    {
        if (!in_array($this->$attribute,[null,self::STATUS_SERVICE]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }

    public function TotemCodeValidator($attribute,$params)
    {   
        if (!in_array($this->$attribute,[null,self::TotemCode,self::TotemCode2,self::TotemCode3,self::TotemCode4,self::TotemCode5,self::TotemCode6,self::TotemCode7,self::TotemCode8,self::TotemCode9,self::TotemCode10]))
        {
            $this->addError($attribute,"Ошибка ввода");
        }

    }

    public function TotemCodeValidatorAlert($attribute)
    {   
        
        $this->addError($attribute,"Ошибка ввода");    

            \Yii::$app->session->setFlash('errorval', 'Данные указаны неверно');
            return null;
        
    }


    public function editadditional($id){

        if (!$this->validate()) {
            \Yii::$app->session->setFlash('errorval', 'Данные указаны неверно');
            return null;
        }


        $user = User::find()->where(['id' => $id])->one();

        $user->play_name=$this->play_name;
        $user->name=$this->name;

        $country = explode(", ",$this->country);

        $user->country = $country[0];
        $user->living_place = $country[1];
        $user->birth_date = $this->birth_date;
        $user->education = $this->education;
        $user->profession = $this->profession;
        $user->marital_status = $this->marital_status;
        $user->interests = $this->interests;
        $user->hobby = $this->hobby;
        $user->link_vk = $this->link_vk;
        $user->link_odnok = $this->link_odnok;
        $user->link_facebook = $this->link_facebook;
        $user->link_instagram = $this->link_instagram;
        $user->link_twitter = $this->link_twitter;
        $user->link_youtube = $this->link_youtube;
        $user->code_state = $this->code_state;
        $user->position_state = $this->position_state;
        $user->code_service = $this->code_service;
        $user->aristocrat_dignity1 = $this->aristocrat_dignity1;
        $user->code_vip = $this->code_vip;
        $user->totalistic_dignity = $this->totalistic_dignity;
        $user->franchise_city = $this->franchise_city;
        $user->code_tpm = $this->code_tpm;
        $user->code_dsm = $this->code_dsm;
        $user->code_epm = $this->code_epm;
        $user->code_ppm = $this->code_ppm;
        $user->is_simple = $this->is_simple;
        $user->code_sity = $this->code_sity;
        $user->code_matrix = $this->code_matrix;
        $user->totem = $this->totem;
        $user->totem_code = $this->totem_code;


        $save = UploadedFile::getInstance($this,'avatar');

        if ($save != null) {
            $string1file = \Yii::$app->security->generateRandomString(4);
            $uploadDir = \Yii::getAlias('@webroot/img/');
            $fulldir = $uploadDir . $string1file . $save;
            $save->saveAs($fulldir);
            $fulldir = '/img/' . $string1file . $save;
            $user->avatar = $fulldir;
        }
        $save = UploadedFile::getInstance($this,'gerb');
        if ($save != null) {
            $string1file = \Yii::$app->security->generateRandomString(4);
            $uploadDir = \Yii::getAlias('@webroot/img/');
            $fulldir = $uploadDir . $string1file . $save;
            $save->saveAs($fulldir);
            $fulldir = '/img/' . $string1file . $save;
            $user->gerb = $fulldir;
        }


        $user->save(false);


        return $user;
    }
}