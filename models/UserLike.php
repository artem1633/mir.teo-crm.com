<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_like".
 *
 * @property int $id
 * @property int $new_id Новости
 * @property int $user_id Пользователь
 *
 * @property News $new
 * @property User $user
 */
class UserLike extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_like';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'user_profile_id'], 'integer'],
//            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
//            [['user_profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь кто лайкнул',
            'user_profile_id' => 'Пользователь кого лайкнули',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getNew()
//    {
//        return $this->hasOne(News::className(), ['id' => 'new_id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getUser()
//    {
//        return $this->hasOne(User::className(), ['id' => 'user_id']);
//    }
}
