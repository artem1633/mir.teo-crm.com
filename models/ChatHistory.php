<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "chat_history".
 *
 * @property int $id
 * @property int $sender_id Отправитель
 * @property int $recipient_id Получатель
 * @property resource $text Текст сообщения
 * @property string $message_send_datetime Дата и время отправки сообщения
 * @property string $read Прочитано получателем или нет
 *
 * @property User $recipient
 * @property User $sender
 */
class ChatHistory extends ActiveRecord
{
    public $files;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_history';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['message_send_datetime'],
                ],
                'value' => date('Y-m-d H:i:s', time()),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_id', 'recipient_id', 'read'], 'integer'],
            [['text'], 'string'],
            [['message_send_datetime'], 'safe'],
            [
                ['recipient_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['recipient_id' => 'id']
            ],
            [
                ['sender_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['sender_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender_id' => 'Отправитель',
            'recipient_id' => 'Получатель',
            'text' => 'Текст сообщения',
            'message_send_datetime' => 'Дата и время отправки сообщения',
            'read' => 'Прочитано получателем',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(User::className(), ['id' => 'recipient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }

    /**
     * Получает сообщения для чата
     * @param int $id Идентификатор собеседника
     * @return array|null|ActiveRecord[]
     */
    public function getMessages($id)
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        $my_id = $identity->id;

        return ChatHistory::find()
                ->andWhere([
                    'recipient_id' => $id,
                    'sender_id' => $my_id
                ])->orWhere([
                    'recipient_id' => $my_id,
                    'sender_id' => $id
                ])
                ->orderBy(['message_send_datetime' => SORT_ASC])
                ->all() ?? null;
    }

    /**
     * Загрузка файла
     * @param array $files Инфа о файлах
     * @return array
     * @throws \yii\base\Exception
     */
    public function saveFiles($files)
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        $upload_dir = 'uploads/chat/' . $identity->id;
        $max_file_size = 1024 * 1024 * 2;

        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }

        $white_list = [
            'image/jpeg',
            'image/gif',
            'image/png',
            'application/pdf',
            'application/zip',
            'application/msword',
            'application/vnd.oasis.opendocument.text',
            'application/vnd.oasis.opendocument.spreadsheet',
            'audio/mpeg',
            'text/plain'
        ];

        /** @var array $uploaded_files Загруженные файлы в формате [ИмяФайла => ПутьКФайлу] */
        $uploaded_files = [];
        foreach ($files as $file) {
            $upload_file_path = $upload_dir . '/' . time() . Yii::$app->security->generateRandomString(5) . '.' . pathinfo($file['name'])['extension'];
            if (in_array(mime_content_type($file['tmp_name']), $white_list) || pathinfo($file['name'])['extension'] == 'm4a') {
                if ($max_file_size < $file['size']) {
                    Yii::error('Превышен допустимый размер файла', '_error');
                    continue;
                }
                move_uploaded_file($file['tmp_name'], $upload_file_path);
                $uploaded_files[$file['name']] = $upload_file_path;
            } else {
                Yii::error('Не разрешенный тип файла', '_error');
                continue;
            }
        }

        return $uploaded_files;
    }

    public function getFormattedDate()
    {
        $date = date('d.m.Y', strtotime($this->message_send_datetime));
        $time = date('H:i', strtotime($this->message_send_datetime));

        if ($date == date('d.m.Y', time())) {
            return $time;
        } else {
            return $date . '<br>' . $time;
        }

    }

    /**
     * Отправляет сообщение в чат всем рефералам от текущего пользователя
     * @return bool
     * @throws \yii\db\Exception
     */
    public function sendingMessageOfReferrals()
    {
        if ($this->text == '') {
            return false;
        }

        $sender = Yii::$app->user->identity->id;

        $rows = [];

        /** @var User $referral */
        foreach (User::find()->andWhere(['ref_id' => $sender])->each() as $referral) {
            $rows[] = [$sender, $referral->id, $this->text, date('Y-m-d H:i:s', time())];
        }

        Yii::$app->db->createCommand()->batchInsert(ChatHistory::tableName(),
            ['sender_id', 'recipient_id', 'text', 'message_send_datetime'], $rows)->execute();

        return true;
    }
}
