<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;
/**
 * Модель для формы обратной связи
 */
class Banner extends ActiveRecord
{
 /**
     * @var UploadedFile
     */
	

    public function attributeLabels() {
        return [     
            'id' => "id",
            'banner' => 'Баннер',
            'category' => 'Изображение',            
    ];}
    public function rules(){
    	return [  
        ['id', 'default'],   	
    		['banner', 'default'],  
            ['category', 'default'],    	
            
    	];
    }
    
public function upload(){
 if($this->validate()){
   $this->banner->saveAs('uploads/banner/' . $this->banner->baseName . '.' . $this->banner->extension);
            return true;
 }else{
 return false;
 }
}

}