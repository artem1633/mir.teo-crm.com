<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_like".
 *
 * @property int $id
 * @property int $new_id Новости
 * @property int $user_id Пользователь
 *
 * @property News $new
 * @property User $user
 */
class UserVisits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_visits';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'counter'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Просмтотры пользователя',
            'counter' => 'Количество просмотров',
        ];
    }

}
