<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class Transfer
 *
 * @package app\models
 *
 * @property integer $id
 * @property integer $currency_id id валюты
 * @property integer $from_user_id отправитель
 * @property integer $for_user_id получатель
 * @property integer $payment_number номер платежа
 * @property string $purpose_of_payment назначение платежа
 * @property double $amount сумма
 * @property integer $user_level уровень тому, кому начислено
 * @property integer $type тип платежа
 * @property string $date дата создания
 */

class Transfer extends ActiveRecord{

    const TYPE_TRANSFER = 1;    //перевод
    const TYPE_EXCHANGE = 2;    //обмен

    /**
     * @inheritDoc
     * @return array
     */
    public function behaviors(){

        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => new Expression('NOW()')
            ],
        ];
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function rules(){

        return [
            [['currency_id', 'from_user_id', 'for_user_id', 'payment_number', 'type', 'user_level'], 'integer'],
            ['amount', 'number'],
            [['purpose_of_payment', 'date'], 'string']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency(){

        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser(){

        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForUser(){

        return $this->hasOne(User::className(), ['id' => 'for_user_id']);
    }

    /**
     * @return void
     */
    public function setPaymentNumber(){

        $this->payment_number = $this->generatePaymentNumber();
    }

    /**
     * @param $balance
     * @return string
     */
    public static function getBalanceBillsLineClass($from_user_id, $for_user_id, $balance = false){

        if(Yii::$app->user->getId() == $from_user_id){
            return 'expenses';
        }elseif(Yii::$app->user->getId() == $for_user_id){
            return $balance ? 'comming' : 'coming';
        }
    }

    /**
     * Генерация номера платежа
     *
     * @return int
     */
    private function generatePaymentNumber(){

        $number = rand(10000, 99999);
        while(self::find()->where(['payment_number' => $number])->exists() == true){
            $number = rand(10000, 99999);
        }
        return $number;
    }
}