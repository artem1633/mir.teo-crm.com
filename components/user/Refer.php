<?php

namespace app\components\user;

use Yii;
use app\models\User;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class Refer
 * @package app\components\user
 */
class Refer extends Component
{
    /**
     * @var User
     */
    public $user;

    /**
     * @return array
     */
    public function getLevelsCount()
    {
        $levels = [
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
            '7' => 0,
            '8' => 0,
            'lider' => 0,
            'loser' => 0,
        ];

        $userLevel1 = User::find()->where(['ref_id' => $this->user->id])->all();

        $pks = [];
        $pks1 = [];
        $pks2 = [];
        $pks3 = [];
        $pks4 = [];
        $pks5 = [];
        $pks6 = [];
        $pks7 = [];
        $pks8 = [];
        $pkslider = [];
        $pks2_1 = 0;

        if ($userLevel1) {

            $pks = ArrayHelper::getColumn($userLevel1, 'id');
            $pks1 = ArrayHelper::getColumn($userLevel1, 'id');


            foreach ($userLevel1 as $itemUserLevel1) {
//                $userLevel2Count += User::find()->where(['ref_id' => $itemUserLevel1->id])->count();
                $userLevel2 = User::find()->where(['ref_id' => $itemUserLevel1->id])->all();


                if ($userLevel2) {

                    $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel2, 'id'));
                    $pks2 = ArrayHelper::merge($pks2, ArrayHelper::getColumn($userLevel2, 'id'));

                    foreach ($userLevel2 as $itemUserLevel2) {
//                        $userLevel3Count += User::find()->where(['ref_id' => $itemUserLevel2->id])->count();
                        $userLevel3 = User::find()->where(['ref_id' => $itemUserLevel2->id])->all();
                        if ($userLevel3) {

                            $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel3, 'id'));
                            $pks3 = ArrayHelper::merge($pks3, ArrayHelper::getColumn($userLevel3, 'id'));

                            foreach ($userLevel3 as $itemUserLevel3) {
//                                $userLevel4Count += User::find()->where(['ref_id' => $itemUserLevel3->id])->count();
                                $userLevel4 = User::find()->where(['ref_id' => $itemUserLevel3->id])->all();
                                if (count($userLevel4) > 0) {

                                    $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel4, 'id'));
                                     $pks4 = ArrayHelper::merge($pks4, ArrayHelper::getColumn($userLevel4, 'id'));

                                    foreach ($userLevel4 as $itemUserLevel4) {
//                                        $userLevel5Count += User::find()->where(['ref_id' => $itemUserLevel4->id])->count();
                                        $userLevel5 = User::find()->where(['ref_id' => $itemUserLevel4->id])->all();

                                        if($userLevel5){

                                            $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel5, 'id'));
                                            $pks5 = ArrayHelper::merge($pks5, ArrayHelper::getColumn($userLevel5, 'id'));

                                            foreach ($userLevel5 as $itemUserLevel5){

//                                                $userLevel6Count += User::find()->where(['ref_id' => $itemUserLevel5->id])->count();
                                                $userLevel6 = User::find()->where(['ref_id' => $itemUserLevel5->id])->all();

                                                if($userLevel6) {

                                                    $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel6, 'id'));
                                                    $pks6 = ArrayHelper::merge($pks6, ArrayHelper::getColumn($userLevel6, 'id'));

                                                    foreach ($userLevel6 as $itemUserLevel6){

//                                                        $userLevel7Count += User::find()->where(['ref_id' => $itemUserLevel6->id])->count();
                                                        $userLevel7 = User::find()->where(['ref_id' => $itemUserLevel6->id])->all();

                                                        if($userLevel7) {

                                                            $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel7, 'id'));
                                                             $pks7 = ArrayHelper::merge($pks7, ArrayHelper::getColumn($userLevel7, 'id'));
                                                             foreach ($userLevel7 as $itemUserLevel7){

//                                                        $userLevel7Count += User::find()->where(['ref_id' => $itemUserLevel6->id])->count();
                                                        $userLevel8 = User::find()->where(['ref_id' => $itemUserLevel7->id])->all();
                                                                if($userLevel8) {
                                                                     $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel8, 'id'));
                                                                     $pks8 = ArrayHelper::merge($pks8, ArrayHelper::getColumn($userLevel8, 'id'));

                                                                }


                                                            }

                                                        }

                                                    }

                                                }

                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        $query = User::find();

        $query->andWhere(['id' => $pks]);

        $query->asArray(true);

        $models = $query->all();



        for ($i = 0; $i < count($models); $i++)
        {
            $model = $models[$i];

            if($model['balance_usd'] >= 1000){
                $levels['lider'] = $levels['lider'] + 1;
            }

            for ($ij = 0; $ij < count($models); $ij++)
        {
            $model2 = $models[$ij];

            if($model['id'] == $model2['ref_id']){
                $levels['loser'] = $levels['loser'] + 1; 
                 $ij = count($models);         
            }
        }

            if(in_array($model['id'], $pks1)){
                $levels['1'] = $levels['1'] + 1;
            }

            if(in_array($model['id'], $pks2)){
                $levels['2'] = $levels['2'] + 1;
            }

            if(in_array($model['id'], $pks3)){
                $levels['3'] = $levels['3'] + 1;
            }

            if(in_array($model['id'], $pks4)){
                $levels['4'] = $levels['4'] + 1;
            }

            if(in_array($model['id'], $pks5)){
                $levels['5'] = $levels['5'] + 1;
            }

            if(in_array($model['id'], $pks6)){
                $levels['6'] = $levels['6'] + 1;
            }

            if(in_array($model['id'], $pks7)){
                $levels['7'] = $levels['7'] + 1;
            }
            if(in_array($model['id'], $pks8)){
                $levels['8'] = $levels['8'] + 1;
            }
        }

        $levels['loser'] =  count($models) - $levels['loser']; 
       

        return $levels;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return array_sum(array_values($this->getLevelsCount()));
    }
    public function getMenu()
    {

        $tmp_user_id = Yii::$app->user->id;
       $menu_tmp = User::findOne(['id' => $tmp_user_id]);
        $menu = $menu_tmp;


        return $menu;
    }
}