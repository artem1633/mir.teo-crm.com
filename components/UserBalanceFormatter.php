<?php

namespace app\components;

use yii\i18n\Formatter;

class UserBalanceFormatter extends Formatter{

    /**
     * @param float $balance
     * @return string
     */
    public static function asBalance($balance){

        return number_format($balance, 2, '.', '');
    }
}